# Title:            test_function.py
# Project:          Transformation Tree Library
# Description:      Testing Class for storing all expected comparisons.

# Team:             The Hackson Five

# Last Modified by: Kuan-Yun Chao
# Date Modified:    10 Feb 2021

import node
import copy
import decimal
import random

"""
*   Function: treenodes_compatability_add_remove_tester
*   Description: This function is  accessed by Tree_Test_Section_1
*                under the class 'test_modules_component_1' within
*                the test_modules_component_1.py file. It accepts
*                empty tree, then it will do a series of function 
*                calls to add(), insert(), replace(), remove(), and
*                "at_index(), in_tree(), copy_path(), and copy()" 
*                (in which are all helper functions) in tree.py
*                for testing tree functionalities. These functionalities
*                are separated into add, remove, and helper blocks.
*                If all three blocks passes the test, it will
*                return tuple - True, list of blocks it success.
*                Otherwise, it will return tuple - False, 
*                list of a specific test it failed.
*
*   Date: 9 Feb 2021
*   Edit History: V1.1.1.1 (2/1/2021):
*                 Initial skeleton was built due to the removal of
*                 add() and remove() within node.py
*                 V1.2.2.2 (2/2/2012):
*                 Since add() and remove() is moved to tree.py
*                 two hard-coded testing blocks were added for
*                 add() and remove()
*                 Initial V1.2.3.3 (2/6/2012):
*                 Third testing block skeleton for helper function 
*                 test is completed.
*                 V1.2.3.3 (2/6/2012):
*                 Third testing block for helper function is
*                 completed.
"""


def treenodes_compatability_add_remove_tester(tree):
    '''
    Tree Depth:
    1    -> 2  -> 3  -> 4
    Tree Structure Tested:
    Root -> p1 -> p2
               -> m2 -> v3
               -> v2
         -> v1 -> v4

    Hard-coded Tests for add(), insert(), replace(), remove(), and
    "at_index(), in_tree(), copy_path(), and copy()" (in which are all helper functions) in tree.py
    Note: corresponding functions to
            add() in node.py is can_parent()
            copy() and copy_path() in node.py is copy()
    '''
    # ------------------------------------/ Testing add() on node (Start) /------------------------------------
    ret_li = []
    i_node_succ = []
    p_node_succ = []
    m_node_succ = []
    v_node_succ = []

    p1 = node.PrepNode()  # only this one can add successfully
    i1 = node.InputNode()
    m1 = node.ModNode()
    v1 = node.VisNode()  # only this one can add successfully

    i_node_succ.append(tree.add(tree.root, tree.root))  # __in_tree detects a redundant node
    i_node_succ.append(tree.add(tree.root, p1))  # the add operation fails due to incompatibility
    i_node_succ.append(tree.add(tree.root, i1))
    i_node_succ.append(tree.add(tree.root, m1))
    i_node_succ.append(tree.add(tree.root, v1))

    if tree.root.children[0] != p1 or tree.root.children[1] != v1:
        return False, ["add() Children not p1 or v1"]
    if len(tree.nodes) != 3:
        return False, ["add() Total Nodes are not Three"]

    p2 = node.PrepNode()  # only this one can add successfully
    i2 = node.InputNode()
    m2 = node.ModNode()  # only this one can add successfully
    v2 = node.VisNode()  # only this one can add successfully

    p_node_succ.append(tree.add(p1, p1))  # __in_tree detects a redundant node
    p_node_succ.append(tree.add(p1, p2))  # the add operation fails due to incompatibility
    p_node_succ.append(tree.add(p1, i2))
    p_node_succ.append(tree.add(p1, m2))
    p_node_succ.append(tree.add(p1, v2))

    if p1.children[0] != p2 or p1.children[1] != m2 or p1.children[2] != v2:
        return False, ["add() Children not p2 or m2 or v2"]
    if len(tree.nodes) != 6:
        return False, ["add() Total Nodes are not Six"]

    p3 = node.PrepNode()
    i3 = node.InputNode()
    m3 = node.ModNode()
    v3 = node.VisNode()  # only this one can add successfully

    m_node_succ.append(tree.add(m2, m2))  # __in_tree detects a redundant node
    m_node_succ.append(tree.add(m2, p3))  # the add operation fails due to incompatibility
    m_node_succ.append(tree.add(m2, i3))
    m_node_succ.append(tree.add(m2, m3))
    m_node_succ.append(tree.add(m2, v3))

    if m2.children[0] != v3:
        return False, ["add() Children not v3"]
    if len(tree.nodes) != 7:
        return False, ["add() Total Nodes are not Seven"]

    p4 = node.PrepNode()
    i4 = node.InputNode()
    m4 = node.ModNode()
    v4 = node.VisNode()  # only this one can add successfully

    v_node_succ.append(tree.add(v1, v1))  # __in_tree detects a redundant node
    v_node_succ.append(tree.add(v1, p4))  # the add operation fails due to incompatibility
    v_node_succ.append(tree.add(v1, i4))
    v_node_succ.append(tree.add(v1, m4))
    v_node_succ.append(tree.add(v1, v4))

    if v1.children[0] != v4:
        return False, ["add() Children not v4"]
    if len(tree.nodes) != 8:
        return False, ["add() Total Nodes are not Eight"]

    if (i_node_succ != [0, 1, 0, 0, 1]
            or p_node_succ != [0, 1, 0, 1, 1]
            or m_node_succ != [0, 0, 0, 0, 1]
            or v_node_succ != [0, 0, 0, 0, 1]):
        return False, ["add() Integer Signal for successful addition is incorrect"]

    ret_li.append("add() test for node success!")

    # ------------------------------------/ Testing add() on node (End) /------------------------------------
    # ------------------------------------/ Testing Helper Methods (Start) /------------------------------------

    # Testing in_tree() by checking every single node in or not in the tree
    if (tree.in_tree(p1)
        + tree.in_tree(p1)
        + tree.in_tree(v1)
        + tree.in_tree(v2)
        + tree.in_tree(v3)
        + tree.in_tree(v4)
        + tree.in_tree(m2)
            + tree.in_tree(i2)) != 7:
        return False, ["in_tree() Not catching something in the tree or catched something that's not in the tree"]

    # Testing at_index()
    actual_nodes = [tree.root, p1, v1, p2, m2, v2, v3, v4]
    for i in range(len(tree.nodes)):
        if tree.at_index(i) != actual_nodes[i]:
            return False, ["actual_nodes() Returned incorrect child"]
    # Testing negative index accessing
    if (tree.at_index(-1) == 0) or (tree.at_index(-8) == 0):
        return False, ["actual_nodes() Checking Node using Negative Index Failed"]

    # Testing copy_path() by nodes from depth two to depth four
    c1 = node.PrepNode()
    c2 = node.InputNode()
    c3 = node.ModNode()
    c4 = node.VisNode()

    # Copying path of nodes not in the tree
    if (tree.copy_path(c1)
        + tree.copy_path(c2)
        + tree.copy_path(c3)
            + tree.copy_path(c4)) != 0:
        return False, ["copy_path() copied nodes not in the tree"]

    # Copying path of nodes in the tree and check whether they have correct types
    v3_path = tree.copy_path(v3)
    if ((type(v3_path.children[0]) != type(p1))
            or (type(v3_path.children[0].children[0]) != type(m2))
            or (type(v3_path.children[0].children[0].children[0]) != type(v3))):
        return False, ["copy_path() does not copy the path correctly"]

    # Copying path of nodes in the tree and check whether they deep copy
    if ((v3_path.children[0] == type(p1))
            or (v3_path.children[0].children[0] == m2)
            or (v3_path.children[0].children[0].children[0] == v3)):
        return False, ["copy_path() does not deep copy"]

    # Copying path of nodes in the tree and check whether they have correct child length
    if (len(v3_path.children)
        + len(v3_path.children[0].children)
            + len(v3_path.children[0].children[0].children)) != 3:
        return False, ["copy_path() does not copy the path (it copied a tree)"]

    # Testing copy() by copying and comparing it to original tree
    shallow_copy = tree.copy()
    tru_false = isinstance(type(shallow_copy.root), type(tree.root))
    if (isinstance(shallow_copy.root, type(tree.root))
            or shallow_copy.root.children[0] == tree.root.children[0]
            or shallow_copy.root.children[1] == tree.root.children[1]
            or shallow_copy.root.children[0].children[0] == tree.root.children[0].children[0]
            or shallow_copy.root.children[0].children[1] == tree.root.children[0].children[1]
            or shallow_copy.root.children[0].children[2] == tree.root.children[0].children[2]
            or shallow_copy.root.children[0].children[1].children[0] == tree.root.children[0].children[1].children[0]
            or shallow_copy.root.children[1].children[0] == tree.root.children[1].children[0]):
        return False, ["copy() does not deep copy"]

    ret_li.append("helper function at_index(), in_tree(), copy_path(), and copy() test for node success!")
    # ------------------------------------/ Testing Helper Methods (End) /------------------------------------
    # ------------------------------------/ Testing remove() on node (Start)/------------------------------------
    removal_test_for_node = copy.copy(tree)

    p5 = node.PrepNode()
    i5 = node.InputNode()
    m5 = node.ModNode()
    v5 = node.VisNode()

    # Node Removal Test
    # Removal node isn't found in the tree list
    # Removal node is the root
    # If tree flag is invalid:
    #   No action is taken
    if (removal_test_for_node.remove(p5)
        + removal_test_for_node.remove(i5)
        + removal_test_for_node.remove(m5)
        + removal_test_for_node.remove(v5)
        + removal_test_for_node.remove(p2, "not flag")
            + removal_test_for_node.remove(removal_test_for_node.root)) != 0:
        return False, ["remove() removed something not in the tree / returned wrong flag / doesn't check 'tree' flag"]

    # Tree flag is not set and removal node's children are incompatible with its parent
    if removal_test_for_node.remove(p1) == 1:
        return False, ["remove() removed a node with none compatible children"]

    # If tree flag is not set:
    #   Removal node is spliced out of the tree and its children are appended to its parent if possible
    removal_test_for_node.add(p2, p5)
    removal_test_for_node.add(p2, m5)
    removal_test_for_node.add(p2, v5)
    if removal_test_for_node.remove(p2) == 0:
        return False, ["remove() cannot remove a node with compatible children"]

    # Remove from leaf to root
    if (removal_test_for_node.remove(p5)
        + removal_test_for_node.remove(m5)
        + removal_test_for_node.remove(v5)
        + removal_test_for_node.remove(v3)
        + removal_test_for_node.remove(v2)
        + removal_test_for_node.remove(m2)
        + removal_test_for_node.remove(p1)
        + removal_test_for_node.remove(v4)
            + removal_test_for_node.remove(v1)) != 9:
        return False, ["remove() Integer Signal for successful removal is incorrect"]

    if (len(p5.children)
        + len(p5.children)
        + len(p5.children)
        + len(p5.children)
        + len(p5.children)
        + len(p5.children)
        + len(p5.children)
        + len(p5.children)
        + len(p5.children)
            + len(p2.children)) != 0:
        return False, ["remove() Integer Signal does not remove node's children"]

    ret_li.append("remove() test for node success!")

    # ------------------------------------/ Testing remove() on node (End)/------------------------------------

    return True, ret_li


"""
*   Function (Private): __flat_merge
*   Description: This is accessed by generate_random_time_series under test_function.py.
*                It takes a list of a single constant, list it will be added to,
*                , and where the single constant started for the addition.
*                Then, it will randomly choose where to put it and append 
*                the list of a single constant to the list it will be added to
*                and return the modified list.
*
*   Date: 9 Feb  2021
*   Edit History: v1.3.1.1 (2/5/2021):
*                 Initial skeleton was built as a universal list merger
*                 that maintains the list order. Removed for code inspection.
*                 v1.3.2.1 (2/9/2021):
*                 Having too much issues with the idea, changed it to
*                 merger that only merges a type of error in time series
*                 - a long period of a constant magnitude
"""


def __flat_merge(li1, li2, start_index=None):
    # choose where to start adding the magnitude constant
    addition_index = random.randrange(0, len(li2) - 1)
    flat_num = li2[start_index - 1]
    for i in range(len(li2)):
        if li2[i] != 0:
            # adding the constant to the list until the length of magnitude constant is met
            li1.insert(addition_index, flat_num)
    return li1


"""
*   Function (Private): __base_magnitude_generator
*   Description: This is accessed by __random_time_series under test_function.py.
*                It take an empty list, number of points in time series, and
*                a tuple - [minimum magnitude, maximum magnitude], generate a basic
*                time series with no trends and return the list.
*
*   Date: 9 Feb  2021
*   Edit History: v1.3.1.1 (2/5/2021):
*                 Initial skeleton was built. Removed for code inspection.
*                 v1.3.2.1 (2/9/2021):
*                 Finished on the first go, generator succeed 
"""


def __base_magnitude_generator(base_series, data_points, mag_range):
    for i in range(data_points):
        # append random number within magnitude range to an empty list
        base_series.append(float(decimal.Decimal(random.randrange(mag_range[0], mag_range[1])) / 100))
    return base_series


"""
*   Function (Private): __outlier_magnitude_generator
*   Description: This is accessed by __random_time_series under test_function.py.
*                It gets numbers of outliers, numbers of datapoint in a time series
*                , outlier range from a tuple - [[minimum magnitude range it can do], 
*                                                [maximum magnitude range it can do]]
*                , and an list. It then randomly assign random outliers to the list
*                and then return modified list. Raise exception when magnitude range
*                is incorrect.
*
*   Date: 9 Feb  2021
*   Edit History: v1.3.1.1 (2/5/2021):
*                 Initial skeleton was built. Removed for code inspection.
*                 v1.3.2.1 (2/9/2021):
*                 Finished on the second go after fixing index error, generator succeed.
*                 Raise exception added later as a check point. 
"""


def __outlier_magnitude_generator(num_of_out, data_points, outlier_range, outlier_series):
    for i in range(num_of_out):
        prev_flag = True
        # choosing random index it will be adding to, if add not successful, continue looping the same process
        while (prev_flag):
            # choose what outliers to add maximum or minimum outlier
            up = random.choice([True, False])
            hit_miss = random.randrange(0, data_points - 1)
            if outlier_series[hit_miss] != 0:
                continue
            # check if the input is correct - minimum to maximum in respect to default magnitude
            if outlier_range[1][0] >= outlier_range[1][1] or outlier_range[0][0] >= outlier_range[0][1]:
                raise ValueError("Incorrect outlier magnitude range")
            # adding maximum outliers
            if up:
                outlier_series[hit_miss] = float(decimal.Decimal(
                    random.randrange(outlier_range[1][0], outlier_range[1][1])) / 100)
            # adding minimum outliers
            if not up:
                outlier_series[hit_miss] = float(decimal.Decimal(
                    random.randrange(outlier_range[0][0], outlier_range[0][1])) / 100)
            prev_flag = False
    return outlier_series


"""
*   Function (Private): __none_magnitude_generator
*   Description: This is accessed by __random_time_series under test_function.py.
*                Similar to __outlier_magnitude_generator, but it now take
*                Number of data points in a time series, numbers of None needed to add,
*                , and a list for generating a list of None data points then return
*                the modified list.
*
*   Date: 9 Feb  2021
*   Edit History: v1.3.1.1 (2/5/2021):
*                 Initial skeleton was built. Removed for code inspection.
*                 v1.3.2.1 (2/9/2021):
*                 Finished on the first try.
"""


def __none_magnitude_generator(data_points, nones, none_series):
    for i in range(nones):
        prev_flag = True
        # choosing random index it will be adding to, if add not successful, continue looping the same process
        while (prev_flag):
            random_index = random.randrange(0, data_points - 1)
            if none_series[random_index] is not None:
                none_series[random_index] = None
                prev_flag = False
    return none_series


"""
*   Function (Private): __flat_magnitude_generator
*   Description: This is accessed by __random_time_series under test_function.py.
*                It take numbers of data points a time series has, [length of how
*                far the constant should so, magnitude constant], and a list. 
*                Then, it randomly assign where the magnitude constant should go,
*                keep adding it to the list, then return the modified list.
*
*   Date: 9 Feb  2021
*   Edit History: v1.3.1.1 (2/5/2021):
*                 Initial skeleton was built. Removed for code inspection. 
*                 With multiple magnitude  constant addition in mind.
*                 v1.3.2.1 (2/9/2021):
*                 Finished on the first go, ditched the multiple magnitude
*                 constant addition due to expected implementation complexity.
"""


def __flat_magnitude_generator(data_points, flats, flat_series):
    max_range = data_points - flats[0]
    # choosing random index it will be adding to, then keep adding magnitude constant until the length is met
    start_index = random.randrange(0, max_range)
    for i in range(flats[0]):
        flat_series[start_index] = flats[1]
        start_index += 1
    return start_index


"""
*   Function (Private): __random_time_series
*   Description: This is accessed by generate_random_time_series under test_function.py.
*                Validate the user input via control list, return None if not provided.
*                Check input validity for each verified generator access, raise exception if found.
*                Otherwise, return list of what each accessed generator outputted 
*                andor list fill with zeros for none accessed generator.
*
*   Date: 9 Feb  2021
*   Edit History: v1.3.1.1 (2/5/2021):
*                 Initial skeleton was built with universal list merger in mind
*                 (i.e) controlling the merge process by control list alone
*                 without a dedicated merge function. Removed for code inspection.
*                 v1.3.3.3 (2/9/2021):
*                 Finished on the third go, ditched the idea of not using merge helper
*                 function initially due to errors, ditched the idea of control list
*                 controlling the merge process on the second go due to implementation
*                 redundancy.
"""


def __random_time_series(mag_range, data_points, control_li=None, outliers=None, nones=None, flats=None):
    # control list: base magnitude, outliers, zero,and flat in an boolean array - [Bool,Bool,Bool,Bool]
    # initialize all the list for each generator
    base_list = []
    outlier_series = [0 for i in range(data_points)]
    none_series = [0 for i in range(data_points)]
    flat_series = [0 for i in range(data_points)]
    start_index = None

    # control what generator will be used to modify the initialized list
    if control_li is None:
        return False
    if control_li[0]:
        # generate a list of default time series data points with no trend
        __base_magnitude_generator(base_list, data_points, mag_range)
    if control_li[1]:
        # generate a list of outliers that will be appended to default time series
        if type(outliers) is not list:
            raise ValueError("incorrect outlier list inputs")
        if data_points <= outliers[0]:
            raise IndexError("number of magnitudes is longer than the length of datasets allowed")
        # numbers of outliers, as well as inputting user specified outlier magnitude range
        num_of_out = outliers[0]
        upper_out_magnitude = outliers[1][1]
        lower_out_magnitude = outliers[1][0]
        __outlier_magnitude_generator(num_of_out, data_points,
                                      [[lower_out_magnitude, mag_range[0]],
                                       [mag_range[1], upper_out_magnitude]], outlier_series)
    if control_li[2]:
        # generate a list of None that will be appended to default time series
        if type(nones) is not int or nones <= 0:
            raise ValueError("incorrect number of nones input")

        if data_points <= nones:
            raise IndexError("number of nones is longer than the length of datasets allowed")
        __none_magnitude_generator(data_points, nones, none_series)

    if control_li[3]:
        # generate a list of magnitude constant (with specified length) that will be appended to default time series
        if type(flats) is not list:
            raise ValueError("incorrect flat list inputs")

        if data_points <= flats[0]:
            raise IndexError("length of flat sequence is longer than the length of datasets allowed")
        start_index = __flat_magnitude_generator(data_points, flats, flat_series)

    return base_list, outlier_series, none_series, flat_series, start_index


"""
*   Function: generate_random_time_series
*   Description: This function will check is control list present. Return empty list
*                if it is not present. Start merging if the control list is present.
*                Join all generators' output list and shuffle it. Remove any zeros
*                present in the merged list then return it.
*
*   Date: 9 Feb 2021
*   Edit History: v1.3.3.3 (2/9/2021):
*                 Finished on the second go, first run failed due to unexpected exponential
*                 list joining. A top level merger after ditching the idea of control list
*                 controlling the merge process.
"""


def generate_random_time_series(mag_range, data_points, control_li=None, outliers=None, nones=None, flats=None):
    # check whether the control list is present
    if control_li is None:
        return []
    else:
        # getting outputs from generators
        BL, OS, NS, FS, Si = __random_time_series(mag_range, data_points, control_li, outliers, nones, flats)
        # remove all the zeros
        BL = [i for i in BL if i != 0]
        OS = [i for i in OS if i != 0]
        NS = [i for i in NS if i != 0]
        # merge and shuffle
        ret_1 = BL + OS + NS
        random.shuffle(ret_1)
        # cannot merge and shuffle magnitude constant list since it will be needed for testing
        if Si is not None:
            # use helper function for merging
            ret_3 = __flat_merge(ret_1, FS, Si)
            ret_4 = [i for i in ret_3 if i != 0]
            return ret_4
        return ret_1
