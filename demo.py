"""
* Title:            demo.py
* Project:          Transformation Tree Library
* Description:      A demo program for light testing of the TFormTree and Pipeline classes.
*
* Team:             The Hackson Five
*
* Last Modified by: Aaron Van Cleave
* Date Modified:    09 Feb 2021
"""

import node
import tree
import pipeline as pipe
import preprocess as prep
import visualization as vis


def main():

    demo_tree = tree.TFormTree.load("test_files/all_paths_tree.txt")

    # If the file was not found, build the tree from scratch
    # This builds a tree with 100 nodes and 77 possible paths
    if not demo_tree:

        # Tree starts in an automatic mode configured for the format of demo_data_0.csv
        demo_tree = tree.TFormTree("auto split", ["input_files", 1, 0, 1])

        preprocess_nodes = []
        visual_nodes = []

        # Get all preprocess modes
        for function in prep.modes:
            preprocess_nodes.append(node.PrepNode(function))

        # Get all visualization modes
        for function in vis.modes:
            visual_nodes.append(node.VisNode(function))

        # Test all preprocessing modes in the tree
        for p_node in preprocess_nodes:
            demo_tree.add(0, p_node.copy())

        # Add an active modeling node to each preprocess node
        for cp_node in demo_tree.root.children:
            demo_tree.add(cp_node, node.ModNode("active", [10, 2, [10, 10], 20]))

        # Test every visualization mode with every preprocessing mode
        for cp_node in demo_tree.root.children:
            cpm_node = cp_node.children[0]
            for v_node in visual_nodes:
                demo_tree.add(cpm_node, v_node.copy())

        # Save the demo tree
        demo_tree.save("test_files/all_paths_tree.txt")

    # Test all 77 paths on one of the shorter files
    all_results = demo_tree.execute("demo_data_0.csv")

    # Print all results
    for result in all_results:
        print(result)

    # Now execute a single path with larger, non-homogeneously-formatted files
    demo_tree.root.set_mode("interactive split")

    demo_tree.execute_path(94,
                           "demo_data_0.csv",
                           "demo_data_1.csv",
                           "demo_data_2.csv",
                           "demo_data_3.csv",
                           "demo_data_4.csv",
                           "demo_data_5.csv",
                           "demo_data_6.csv")

    # Executing the same processes with a pipeline derived from the tree
    demo_pipe = pipe.Pipeline(demo_tree, 94)

    print(demo_pipe)

    demo_pipe.execute("demo_data_0.csv",
                      "demo_data_1.csv",
                      "demo_data_2.csv",
                      "demo_data_3.csv",
                      "demo_data_4.csv",
                      "demo_data_5.csv",
                      "demo_data_6.csv")

    return 0


if __name__ == "__main__":
    main()
