# Title:            design.py
# Project:          Transformation Tree Library
# Description:      Implementation for the design matrix functions.

# Team:             The Hackson Five

# Last Modified by: Sarah Hartley
# Date Modified:    8 Feb 2021

import pandas as pd
from utils import split_data
from input import input_func


def check_output(output_path: str):
    """
    Input:
        A string that is a path to a CSV file
    Expected behavior:
        Checks a string to see if string has a "csv" extension
    Output:
        True on success, False on failure
    """
    # checks the output string for a csv extension. if found, returns True
    if output_path.endswith('.csv'):
        return True
    # if there is no csv extension, returns False
    else:
        print('Error:Output path must be a csv ')
        return False


def check_matrix_input(input_index, output_index):
    """
    Input:
        A list of input indices and a list of output indices
    Expected behavior:
        Checks the input of the function to make sure it is a valid input for
        design matrix
    Output:
        True on success, False on failure
    """
    # checks to see is the input indices and output indices are in a list
    if not isinstance(input_index, list) or not isinstance(output_index, list):
        print('Error: Design Matrix Input Index and Output Index must be a list ')
        return False
    # checks to see if all input indices are less than or equal to 0
    for index in input_index:
        if index > 0:
            print('Error: input index cannot contain positive values')
            return False
    # checks to see if all output indices are are greater than 0
    for index in output_index:
        # if the index is less than or equal to 0
        if index <= 0:
            print('Error: output_index cannot contain negative values')
            return False
    # returns True if invalid arguments were not found
    return True


def design_matrix(ts, input_index, output_index, a=None, b=None):
    """
    Input:
        A time series data frame, an input list, an output list
        OR
        A time series data frame, and an mi, ti, mo, and to integer
    Expected behavior:
        Checks for optional parameters a and b. If found, checks to ensure input fits mi, ti, mo, to format.
        Calls the helper function to change the format to input index/output index format.
        Then takes input index/output index format and creates a design matrix
    Output:
        Returns a dataframe in the form of a design matrix
    """
    # checks for optional parameters a and b, if found passes to helper function
    if a and b:
        df = design_matrix_helper(ts, input_index, output_index, a, b)
        return df
    # checks for presence of exclusively a or b
    elif (a and not b) or (b and not a):
        print('Error: Invalid number of arguments to design matrix')
        return 0
    # else, has valid parameters for input index and output index
    else:
        if not check_matrix_input(input_index, output_index):
            return 0
        # makes a reference to the time series magnitude list
        mag_list = ts['Magnitude']
        matrix = []
        # calculates the lowest value in the input index
        lowest = (min(input_index))
        # calculates the highest value in the output index
        highest = (max(output_index))
        # sets the t value to the absolute value of the lowest value
        t = abs(lowest)
        # concatenates the two index lists together
        index_list = input_index + output_index
        # starts the current t value at t and goes through the list, stopping when the end is reached
        for curr_t in range(t, (len(mag_list) - highest)):
            curr_mag_list = []
            # goes through index list, finds appropriate index
            for index in index_list:
                curr_mag_list.append(mag_list[curr_t + index])
            matrix.append(curr_mag_list)
        # sorts the index list for better readability
        index_list.sort()
        # creates a data frame from the list of calues and index list
        df = pd.DataFrame(matrix, columns=index_list)
        return df


def design_matrix_helper(ts, mi, ti, mo, to):
    """
    Input:
        A time series data frame, and integers mi, ti, mo, and to that specify the spacing and occurances
        of data
    Expected behavior:
        Turns the input into a format that can then be passed back to the design matrix function
    Output:
        Passes the newly formatted data back to design matrix, returns a design matrix
    """
    # checks to make sure that the given arguments are integers. If so, returns an error message
    if not (isinstance(mi, int) and isinstance(ti, int) and isinstance(mo, int) and isinstance(to, int)):
        print('Error: mi, ti, mo, and to must be integers')
        return 0
    # initializes empty lists for the input and output indices
    input_index = []
    output_index = []
    # turns mi values into input index values and appends them to the empty input list
    for i in range(1, mi + 1):
        input_index.append((i * ti) * -1)
    # turns mo values into output index values and appends them to the empty output list
    for i in range(1, mo + 1):
        output_index.append(i * to)
    # takes the new input and output list and calls design matrix
    return design_matrix(ts, input_index, output_index)


def ts2db(input_filename, target_folder, perc_training, perc_valid, perc_test, input_index, output_index,
          output_file_name):
    """
   Input:
       A string that is a path to file name, a target folder that the file is in, a percentage of valid data,
       a percentage of training data, a percentage of test day, an list of input indices, a list of output indices,
       and an output file name that is the path to a csv file
   Expected behavior:
       Takes these parameters and calls the input, split data, and design matrix formulas creating a database.
       Saves this database to a csv file.
   Output:
       Saves a database to the file specified by 'output_file_name'
   """
    # checks to see if the output file name has a csv extension
    if not check_output(output_file_name):
        return 0
    # take in input_filename, return data_frame_dict
    df = input_func([input_filename], "interactive", target_folder, 0, 0, 1)
    # takes data_frame_dict, call split_data
    x = split_data(df, perc_training, perc_valid, perc_test)
    # passes the split data frame to the design matrix function
    new_df = design_matrix(x['split_dfs']['0']['training'], input_index, output_index)
    # writes the data frame to a csv file
    new_df.to_csv(output_file_name)
