"""
* Title:            input.py
* Project:          Transformation Tree Library
* Description:      Input module for reading files and converting to standard DFD format.
*
* Team:             The Hackson Five
*
* Last Modified by: Aaron Van Cleave
* Date Modified:    07 Feb 2021
"""

# Utils is used for DFD standards and to access the default empty DFD
import utils
# pathlib is used for file routing
from pathlib import Path
# pandas.read_csv is used to generate dataframes from csv files
import pandas as pd

# Modes for the input class to specify what filetype it can read
#       Whether it splits or not
#       Whether the input is user-validated or automatically read based on the given format
modes = ["interactive", "interactive split", "auto", "auto split"]
# The number of lines of the input file to preview
default_preview_lines = 5


# Input will take:
def input_func(filenames: list,
               mode: str or None,
               target_folder: str,
               auto_skip_rows: int,
               auto_time_col: int,
               auto_mag_col: int):
    """
    Input:
        A nonempty list of filenames
        None or string mode
        String path relative to the project directory to a folder files are read from
        Non-negative int for the number of rows to skip in each file
        Non-negative int for the column to read timestamps from
        Non-negative int for the column to read magnitudes from
    Expected behavior:
        If mode is None, return an empty DFD
        Otherwise, validate the parameters and execute the appropriate mode
        In interactive, prompt users for information to read files
        In automatic, read all files the same way
        If a file is successfully read, save its DF representation into a DFD to return
    Should fail when:
        Parameters are invalid
        No files are successfully read
    Output:
        A DFD representing the data in the successfully read filenames on success, 0 otherwise
    """

    # Return the default DFD
    if mode is None:
        return utils.default_empty_DFD

    # GENERAL VALIDATION
    # Other methods ensure that filenames should be a list, but we check again for thoroughness
    if not isinstance(filenames, list):
        print("Error: Argument 0 must be a list")
        return 0
    if not filenames:
        print("Error: No filename(s) given")
        return 0
    for item in filenames:
        if not isinstance(item, str):
            print("Error: Argument 0 list must contain strings only")
            return 0
    if not isinstance(mode, str):
        print("Error: Argument 1 must be a string or None")
        return 0
    if not isinstance(target_folder, str):
        print("Error: Argument 2 must be a string")
        return 0
    if not isinstance(auto_skip_rows, int) or auto_skip_rows < 0:
        print("Error: Argument 3 must be a non-negative int")
        return 0
    if not isinstance(auto_time_col, int) or auto_time_col < 0:
        print("Error: Argument 4 must be a non-negative int")
        return 0
    if not isinstance(auto_mag_col, int) or auto_mag_col < 0:
        print("Error: Argument 5 must be a non-negative int")
        return 0
    if auto_time_col == auto_mag_col:
        print("Error: Argument 4 and 5 must be distinct")
        return 0

    # Information needed for every loop iteration:

    # To count how many file reads are successful
    success_counter = 0
    # The file folder in the cwd
    proper_path = Path(target_folder).resolve()
    # The destination for all successfully extracted data is this simple TS object
    ts_object = {utils.data_frames_dict: {}, utils.complete: False}

    # Iterate across every filename
    for filename in filenames:

        # Construct filenames and paths
        complete_path = proper_path / filename

        # These are the values that must be obtained from the user if mode is interactive
        skip_row_num = auto_skip_rows
        timestamp_index = auto_time_col
        magnitude_index = auto_mag_col

        # Enter interactive mode
        if mode == "interactive split" or mode == "interactive":

            # This starts out at default, but the user will be given an option to modify it if necessary
            preview_amount = default_preview_lines

            # Attempt to open the file, if failed, skip this filename
            try:
                preview_file = open(complete_path, "r")
            except FileNotFoundError:
                print("Error: Could not preview file " + filename + " in " + target_folder)
                continue

            # Get the file lines and close the file
            preview_lines = preview_file.read().splitlines()
            preview_file.close()

            # Get these values to help with input validation later
            # An upper bound on the maximum number of rows to skip
            num_rows = len(preview_lines)
            # An upper bound on column indices
            num_cols = len(preview_lines[0].split(","))

            # A flag to indicate when user input is satisfactory
            can_continue = False

            # This first loop traps on condition that the user has previewed sufficient lines of the file
            while not can_continue:

                # Show a preview of the file so they can see how it's formatted
                print("Previewing the first " + str(preview_amount) +
                      "/" + str(num_rows) + " lines of " + filename + ":\n")

                for x in range(preview_amount):
                    print(preview_lines[x])

                # Don't bother with the getting-more-lines procedure if there aren't any more to get
                if preview_amount == num_rows:
                    break

                print("\nDo you need to preview more lines to decide how many to skip?")

                # This loop traps on condition that the user properly answers a yes / no prompt
                preview_confirmed = False
                while not preview_confirmed:

                    # Make the prompt
                    yesno = input("(Y/N): ")

                    # Now we need to find out how many lines they want to see
                    if yesno == "y" or yesno == "Y":

                        # This loop traps on condition that the user provides a valid integer for a new preview
                        new_preview_set = False
                        while not new_preview_set:

                            # Prompt to get a new number of lines to preview
                            print("Enter a number between " + str(preview_amount) +
                                  " and " + str(num_rows) + " to preview more.")

                            new_preview_amount = input("Lines to preview: ")

                            # Attempt converting to an integer
                            try:
                                new_preview_amount = int(new_preview_amount)

                                # Is it in a range greater than the current preview and within the upper bound?
                                if preview_amount < new_preview_amount <= num_rows:
                                    preview_amount = new_preview_amount
                                    new_preview_set = True
                                else:
                                    print("Error: " + str(new_preview_amount) +
                                          " is not a valid number between " + str(preview_amount) +
                                          " and " + str(num_rows))
                            # Not a valid integer
                            except ValueError:
                                print("Error: " + str(new_preview_amount) + " is not a valid integer")

                        # After the valid integer is obtained, we can break out of the preview confirmation loop
                        preview_confirmed = True

                    # No additional preview necessary - only under this condition may we proceed
                    elif yesno == "n" or yesno == "N":
                        preview_confirmed = True
                        can_continue = True

            print("\nPlease answer the following prompts to ensure that this data is formatted properly.",
                  "\n - Rows to skip are counted from the top.",
                  "\n - Columns are 0-indexed from left to right.\n")

            # This loop traps on condition that the user provides a valid number of top rows to ignore
            can_continue = False
            while not can_continue:

                # Prompt user: get how many top rows to ignore
                skip_row_num = input("Number of rows to skip: ")
                try:
                    skip_row_num = int(skip_row_num)

                    if skip_row_num > num_rows or skip_row_num < 0:
                        print("Error: " + str(skip_row_num) +
                              " is not a valid number between 0 and " + str(num_rows))
                    else:
                        can_continue = True
                except ValueError:
                    print("Error: " + str(skip_row_num) + " is not a valid integer")

            # This loop traps on condition that the user provides a valid timestamp index
            can_continue = False
            while not can_continue:

                # Prompt user: get which column is the timestamp column
                timestamp_index = input("Timestamp column index: ")
                try:
                    timestamp_index = int(timestamp_index)

                    if timestamp_index >= num_cols or timestamp_index < 0:
                        print("Error: " + str(timestamp_index) +
                              " is not a valid index between 0 and " + str(num_cols - 1))
                    else:
                        can_continue = True
                except ValueError:
                    print("Error: " + str(timestamp_index) + " is not a valid integer")

            # This loop traps on condition that the user provides a valid magnitude index
            can_continue = False
            while not can_continue:
                # Prompt user: get which column is the magnitude column
                magnitude_index = input("Magnitude column index: ")
                try:
                    magnitude_index = int(magnitude_index)

                    if magnitude_index >= num_cols or magnitude_index < 0:
                        print("Error: " + str(magnitude_index) +
                              " is not a valid index between 0 and " + str(num_cols - 1))
                    # Note that these must not equal each other; must be distinct columns
                    elif magnitude_index == timestamp_index:
                        print("Error: Magnitude index " + str(magnitude_index) +
                              " cannot equal timestamp index " + str(timestamp_index))
                    else:
                        can_continue = True
                except ValueError:
                    print("Error: " + str(magnitude_index) + " is not a valid integer")

        # Get the timestamp and magnitude format lists in the right order
        if timestamp_index < magnitude_index:
            use_list = [timestamp_index, magnitude_index]
            col_names = [utils.timestamp, utils.magnitude]
        else:
            use_list = [magnitude_index, timestamp_index]
            col_names = [utils.magnitude, utils.timestamp]

        # The list of columns to parse for dates
        parse_date_list = [timestamp_index]

        # Attempt to read the file with the given format specifiers
        try:
            temp_df = pd.read_csv(complete_path,
                                  names=col_names,
                                  parse_dates=parse_date_list,
                                  infer_datetime_format=True,
                                  usecols=use_list,
                                  skiprows=skip_row_num)

            # On success, add the DF to the DFD and increment the number of successful reads
            ts_object[utils.data_frames_dict][str(success_counter)] = temp_df
            success_counter = success_counter + 1

            print("Read " + filename)

        # Indicate which files fail
        except ValueError:
            print("Error: Value error. Failed on " + filename)
        except pd.errors.ParserError:
            print("Error: Parser error. Failed on " + filename)
        except FileNotFoundError:
            print("Error: File " + filename + " not found.")
        except IndexError:
            print("Error: Index error. Failed on " + filename)

    # As long as we read at least one file successfully, process the DFs further
    if success_counter:
        print("Read " + str(success_counter) + " / " + str(len(filenames)) + " files")

        # Attempt validating the DFD for completeness
        # This is also an implicit check for correct DF structure, hence the try-except
        try:
            # Whether the DFD is "complete" or not won't affect the output besides the DFD completeness flag
            # It only matters if it can be safely processed
            if utils.is_complete_dictionary(ts_object):
                print("DFD is verified complete")
            else:
                print("DFD contains NaN values")
        except TypeError:
            print("Error: Data was improperly formatted")
            return 0

        # If the DFD was safely processed, and the function is in split mode, do that now
        if mode == "interactive split" or mode == "auto split":
            utils.split_data(ts_object, 80, 10, 10)
            print("DFD is split")

        return ts_object

    # This is the case where every file read failed
    print("Error: No files read")
    return 0
