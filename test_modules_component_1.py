# Title:            test_modules_component_1.py
# Project:          Transformation Tree Library
# Description:      Testing Class for storing all expected comparisons.

# Team:             The Hackson Five

# Last Modified by: Kuan-Yun Chao
# Date Modified:    10 Feb 2021

import preprocess as PP
import test_function as TF

"""
*   Class: test_modules_component_1
*   Description: A class that contain an interface function for future testing purposes
*                on preprocesses and an test function for testing an input tree against
*                hard-coded output. Only test function return error if no argument
*                specified what test to use. It normally return tested result list.
*                Interface return specified time series column list.
*
*   Date: 24 Jan 2021
*   Edit History: v1.0 (1/19/2021):
*                 Initial Frame was built with each function labeled as a dedicated
*                 tester for each module  
*                 v1.1 (1/23/2021):
*                 Initial Frame was completed with each function labeled as a dedicated
*                 tester for each module plus function calls to its module it will be tested
*                 on.
*                 v1.1.1.1 (1/29/2021):
*                 Initial tests for testing node.py was partially completed, it was a test
*                 of a list of nodes each with different type adding andor against each other.
*                 Verified the node functions can be operated just like a tree. Notified
*                 member for moving its tree functionalities into tree.py. Local objects
*                 created for future testing.
*                 v1.2.1.1 (2/1/2021):
*                 Some node functionalities moved to the tree, test functions broke, commenting
*                 out andor removed affected test functions. Initial skeleton for tree 
*                 testing is built in wish to replace old tests for node.
*                 v1.2.2.2 (2/2/2021):
*                 Moved all hard-coded tests to test_function.py
*                 Now it only calls the test and the test will finish addition and removal of tree
*                 node. 
*                 v1.2.3.3 (2/6/2021):
*                 All tests previously for testing node functionalities were removed. Local objects
*                 were removed since it will no longer be used.
*                 v1.2.3.3 (2/9/2021):
*                 All test functions for modules that does not have enough time to be tested are
*                 removed. Initial test interface was made for testing preprocessing module. Caught
*                 preprocessing bugs if the outlier was at the beginning of the time series.
*                 Bug fixed.
*                 Found the preprocessing is not sensitive enough for small outliers.
*                 Not fixed and verified it is an expected behaviour of the outlier preprocessing
*                 function.
"""


class TestModulesComponent_1:
    """
    test_modules_component_1:
        a test class contains either hard-coded tests such as Tree_Test_Section_1
        or
        an interface for capturing outputs of a module with given arguments
        such as
    """
    #  Initialization section
    def __init__(self):
        #  Initializing a default input list for storing a consistent data set for testing
        self.default_input1 = []

    """
        This Function is for testing tree.py with test cases
    """

    def Tree_Test_Section_1(self, tree, gate):
        """
        Tree: Tree Test Section 1, testing whether tree functions' output matches input
        It takes a tree for input testing against hard-code output, a gate for controlling what
        part of the entire test set will be tested
        """
        if gate == "treenodes_compatability_add_remove_tester":
            ret_tuple = TF.treenodes_compatability_add_remove_tester(tree)
            return ret_tuple
        return False, "incorrect argument"

    """
        This Function is for testing preprocess.py with test cases
    """

    def Preprocess_Function_Interface_1(self, input, column, index,
                                        process_function: str, a=None, b=None):
        """
        Preprocess: Function Interface
        An interface designed to take what preprocess() take and return the result
        after preprocess() is executed
        """
        PP.preprocess(input, process_function, a, b)
        inner_dict = input['data_frames_dict']
        specified_data_frame = inner_dict[index]
        specified_data_column_list = specified_data_frame[column].to_list()
        return specified_data_column_list
