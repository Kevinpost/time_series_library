"""
* Title:            pipeline.py
* Project:          Transformation Tree Library
* Description:      Pipeline class for executing time series operations.
*
* Team:             The Hackson Five
*
* Last Modified by: Aaron Van Cleave
* Date Modified:    07 Feb 2021
"""

# Node and Tree are used to generate and validate paths when instantiating and loading
import node
import tree

# However, the pipeline class interfaces directly with the functional modules when executing
import input as inp
import preprocess as prep
import model_forecast as mod
import visualization as vis


class Pipeline:
    """
    A Pipeline is a lightweight representation of a path in a Transformation Tree.
    """

    def __init__(self, input_tree, path_node: node.OperationNode or int):
        """
        Input:
            An input tree and a node in that tree
        Expected behavior:
            Always instantiates a pipeline object, even when parameters are invalid
            Invalid parameters yield a null pipeline which cannot be saved or executed
            If parameters are valid, tree nodes are converted into pipeline sections in proper order
        """

        print("Creating Pipeline")

        # The Pipeline is a list of tuples
        # Each tuple represents a node and its settings
        self.sections = []

        # Validate the tree, if invalid make a null pipeline
        if not isinstance(input_tree, tree.TFormTree):
            print("Warning: Invalid tree argument. Pipeline is null\n")
            return

        # Pipelines are only generated from a path derived from a tree and one of the tree's nodes
        # This is meant to ensure that their structure and members are valid
        path = input_tree.copy_path(path_node)

        # Validate the generated path
        if not isinstance(path, node.InputNode):
            print("Warning: Invalid path. Pipeline is null\n")
            return

        # Traverse the path from here
        op_node = path

        # Save each node's attributes as a tuple in the pipeline's sections list
        while True:
            # Convert each node into a tuple of its components and add it to the list
            section = (op_node.type, op_node.mode, op_node.parameters)
            self.sections.append(section)

            # After getting all the path nodes, break out
            if op_node.is_leaf():
                break
            else:
                op_node = op_node.children[0]

        print("Created Pipeline of length " + str(len(self.sections)) + "\n")

    def execute(self, *args):
        """
        Input:
            An arbitrary number of filenames
        Expected behavior:
            Executes sections in sequence, feeding each output to the next section
        Should fail when:
            No filenames are given
            The executed path fails
            Un unknown section type is encountered (likely to be removed)
        Output:
            The output of the final section after its predecessors have executed, or 0 on failure
        """

        print("Executing pipeline")

        # This function takes a variable number of arguments; convert those to a list
        section_input = []

        # Extract the arguments into a list
        for filename in args:
            section_input.append(filename)

        # Make sure there's at least one
        # Further validation isn't necessary; it will be handled in the called functions
        if not section_input:
            print("Error: No filename(s) given")
            print("Pipeline execution failed\n")
            return 0

        # Check whether the pipeline is null
        if not self.sections:
            print("Error: Pipeline is null")
            print("Pipeline execution failed\n")
            return 0

        # Loop through the list of sections
        # This loop is very similar to the execution functions in the node class; it combines them all
        for section in self.sections:

            # Extract the section attributes
            sect_type = section[0]
            mode = section[1]
            params = section[2]

            print("Executing " + sect_type + " section in " + str(mode) + " mode")

            # For each section, identify its type, break out the parameters, and call the appropriate function
            if sect_type == node.input_type:

                p0 = params[0]
                p1 = params[1]
                p2 = params[2]
                p3 = params[3]

                section_input = inp.input_func(section_input, mode, p0, p1, p2, p3)

            elif sect_type == node.prep_type:

                p0 = params[0]
                p1 = params[1]
                p2 = params[2]
                p3 = params[3]
                p4 = params[4]
                p5 = params[5]
                p6 = params[6]

                if mode is not None:
                    section_input = prep.preprocess(section_input, mode, p0, p1, p2, p3, p4, p5, p6)

            elif sect_type == node.mod_type:

                int_p0 = params[0]
                int_p1 = params[1]
                list_p = params[2]
                int_p2 = params[3]

                if mode is not None:
                    section_input = mod.mod_fore(section_input, int_p0, int_p1, list_p, int_p2)

            elif sect_type == node.vis_type:

                p0 = params[0]
                p1 = params[1]
                p2 = params[2]

                if mode is not None:
                    section_input = vis.visualize(section_input, mode, p0, p1, p2)

            # Stop execution if an invalid section is encountered
            else:
                print("Error: Execution interrupted by invalid section " + str(self.sections.index(section)))
                print("Pipeline execution failed\n")
                return 0

            # Check if the section executed successfully
            if not section_input:
                print("Execution interrupted in section " + str(self.sections.index(section)))
                print("Pipeline execution failed\n")
                return 0

        # Return the final output
        print("Pipeline execution succeeded\n")
        return section_input

    def save(self, filename):
        """
        Input:
            A filename for the destination file
        Expected behavior:
            Writes a simple string representation of each section to a line in a file
        Should fail when:
            The filename provided is not a string
        Output:
            1 for success, 0 for failure
        """

        print("Saving Pipeline to " + str(filename))

        # Validate the filename
        if not isinstance(filename, str):
            print("Error: Filename must be a string")
            print("Pipeline save failed\n")
            return 0

        # Make sure the pipeline isn't null
        # Saving it would just yield an empty file; pointless
        if not self.sections:
            print("Error: Pipeline is null")
            print("Pipeline save failed\n")
            return 0

        # Store all the pipeline information here
        file_text = ""

        # Store each section
        for section in self.sections[0:-1:]:
            file_text = file_text + str(section) + "\n"

        # Store the final section with no line ending
        file_text = file_text + str(self.sections[-1])

        # Open the file, write, and close
        o_file = open(filename, "w")
        o_file.write(file_text)
        o_file.close()

        print("Pipeline save succeeded\n")
        return 1

    @staticmethod
    def load(filename):
        """
        Input:
            A string filename
        Expected behavior:
            Reads a file describing a valid Pipeline object
            Instantiates the described object
        Should fail when:
            The filename provided is not a string
            The file read operation fails
            The contents of the file are invalid
        Output:
            A valid pipeline on success, 0 otherwise
        """

        print("Loading Pipeline from " + str(filename))

        # Validate filename
        if not isinstance(filename, str):
            print("Error: Filename must be a string")
            print("Pipeline load failed\n")
            return 0

        # Attempt to open the file
        try:
            i_file = open(filename, "r")
        except FileNotFoundError:
            print("Error: Failed to open file")
            print("Pipeline load failed\n")
            return 0

        # Get the contents as separate lines and close the file
        file_lines = i_file.read().splitlines()
        i_file.close()

        # Check that the file was nonempty
        if not file_lines:
            print("Error: File is empty")
            print("Pipeline load failed\n")
            return 0

        # Declare temporary storage for pre-validated pipeline sections
        temp_sections = []
        # print("Number of lines read: " + str(num_sections))

        # Each line should correspond to one pipeline section, so for each line:
        for line in file_lines:

            # A persistent validity flag to only be set at the end of successful validations
            valid = False
            # These store validated sections of a pre-validated pipeline
            # Whether they are None or not is also used as a flag
            temp_tuple = None
            temp_node = None

            # Try converting the raw string to a python tuple
            try:
                # Evaluate the file line string as a python expression, expecting a tuple
                temp_tuple = eval(line)

                # Validate tuple expression and tuple structure
                if isinstance(temp_tuple, tuple) and len(temp_tuple) == 3:

                    # Extract what we expect to be the node type
                    t_type = temp_tuple[0]

                    # Validate tuple contents
                    # If the section type is valid, instantiate that type of node
                    if t_type == node.input_type:
                        temp_node = node.InputNode()
                    elif t_type == node.prep_type:
                        temp_node = node.PrepNode()
                    elif t_type == node.mod_type:
                        temp_node = node.ModNode()
                    elif t_type == node.vis_type:
                        temp_node = node.VisNode()
                    else:
                        print("Error: Invalid node type: " + str(t_type))
                else:
                    print("Error: Invalid tuple: " + line)

            # The file was formatted improperly somehow
            except SyntaxError:
                print("Error: File contains invalid Python syntax:")
                print(line)
            except TypeError:
                print("Error: File contains invalid Python type usage:")
                print(line)

            # If we were able to extract a valid node, it's time to validate the mode and parameters
            # This will only not be None if all previous validations passed
            if temp_node is not None:

                # Grab what we expect to be the mode and parameters
                t_mode = temp_tuple[1]
                params = temp_tuple[2]

                # Pass responsibility for validating those to the node classes
                if temp_node.set_mode(t_mode):
                    if temp_node.set_params(params):
                        # True if and only if we have a valid type, mode, and parameters
                        valid = True
                    else:
                        print("Error: Invalid parameters: " + str(params))
                else:
                    print("Error: Invalid mode: " + str(t_mode))

            # If we have a valid node, add it
            if valid:
                temp_sections.append(temp_node)
            # Just one invalid section is enough to fail the entire load process
            else:
                print("Pipeline load failed\n")
                return 0

        # After loading all the sections successfully, the entire pipeline structure must be validated

        # The pipeline must start with an input section
        if not isinstance(temp_sections[0], node.InputNode):
            print("Error: First file line is not an encoding of an InputNode: " + str(temp_sections[0]))
            print("Pipeline load failed\n")
            return 0

        # This is the tree representing the pipeline that will be returned if all goes well
        t_tree = tree.TFormTree()

        # Overwrite the reference to the original tree root with the read input node
        t_tree.root = temp_sections[0]
        t_tree.nodes[0] = t_tree.root

        # Now we'll try adding the sections to the try, starting from the input root
        current_parent = t_tree.root

        # Nodes will be added before nodes with indices that depend on them
        for item in temp_sections[1:]:

            # Try adding each node to the tree
            # A single failure indicates an invalid section arrangement in the file, prompting a load failure
            if not t_tree.add(current_parent, item):
                print("Error: Invalid section configuration encountered")
                print("Pipeline load failed\n")
                return 0

            # Since the tree is a simple chain, the previous child is the new parent
            current_parent = item

        # Use the tree and its final node to create the pipeline that will be returned
        t_pipe = Pipeline(t_tree, temp_sections[-1])

        print("Pipeline load succeeded\n")
        return t_pipe

    def __str__(self):
        """
        Output:
            A string representing the sections of the pipeline
        """

        result = ""

        # Generate a string of the section tuples
        for section in self.sections:
            result = result + str(section) + "\n"

        return result


# Light testing with creating, saving, loading, and executing pipelines
def main():

    # Loading a tree to build pipelines
    test_tree = tree.TFormTree.load("test_files/tree.txt")

    pipe1 = Pipeline(test_tree, test_tree.at_index(16))

    pipe2 = test_tree.to_pipeline(test_tree.at_index(16))

    print(pipe1)
    print(pipe2)

    pipe2.save("test_files/pipe.txt")

    pipe3 = Pipeline.load("test_files/pipe.txt")
    print(pipe3)

    pipe4 = Pipeline.load("test_files/badpipe2.txt")
    print(pipe4)

    pipe5 = Pipeline.load("test_files/badpipe3.txt")
    print(pipe5)

    pipe6 = Pipeline.load("test_files/pipe2.txt")
    print(pipe6)

    null_pipe = Pipeline(test_tree, test_tree.at_index(8))

    null_pipe.execute("input_files/testdata2.csv")

    null_pipe.save("test_files/nullpipe.txt")

    # Try making and executing a Pipeline with a non-tree
    bad_pipe = Pipeline(1, 5)

    bad_pipe.execute("hello", "hi", "how are you")

    bad_pipe.save("really_bad_pipe.txt")

    # Try making and executing a Pipeline with a good tree and a bad path
    bad_pipe2 = Pipeline(test_tree, 12000)

    bad_pipe2.execute("hello", "hi", "how are you")

    bad_pipe2.save("really_bad_pipe2.txt")


if __name__ == "__main__":
    main()
