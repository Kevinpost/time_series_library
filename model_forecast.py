"""
*   Title:            model_forecast.py
*   Project:          Transformation Tree Library
*   Description:      Module for building and training models and generating
*                     forecasts.
*
*   Team:             The Hackson Five
*
*   Last Created by: Perat Damrongsiri
*   Date Created:    24 Jan 2021
"""

import pandas as pd
import numpy as np
from sklearn.neural_network import MLPRegressor
import utils


# Helper function: convert pandas timestamp to unix datetime
def timestamp_to_unixtime(timestamps):
    return timestamps.values.astype(int) / 10**9


"""
*   Function: mod_fore
*   Description: This is accessed in node.py. This function will check and
*                validate the user input. On success, it returns timeseries
*                dictionary that is included the forecast data. On failure, it
*                returns 0 and prints error message.
*
*   Date: 24 Jan 2021
*   Edit History: v1.0: Created this function. No inputs validation added.
*                 29 Jan 2021
*                 v1.1: Added input validation. Edited the part that use
*                       Modeling_Forecasting class to match with the updated
*                       functions in the class.
*                 31 Jan 2021
*                 v1.2: Bug fixing
*                 4 Feb 2021
*                 v1.4: Fixed: multiple prediction per timestamp Error
*                       Fixed: error when output_dimension == 1
*                 5 Feb 2021
*                 v1.5: Fixed: timestamp error
*                 7 Feb 2021
*                 v1.5.1: PEP 8 format
*                 v1.6: Change forecast timestamp.
"""


def mod_fore(ts, input_dimension, output_dimension, internal_layers, num_forecast):
    # Validate parameters' data type
    # input_dimension - int
    if not isinstance(input_dimension, int):
        print("Error: Input dimension is not an integer.")
        return 0

    # output_dimension - int
    if not isinstance(output_dimension, int):
        print("Error: Output dimension is not an integer.")
        return 0

    # internal_layers - list
    if not isinstance(internal_layers, list):
        print("Error: Internal layers is not a list.")
        return 0

    # elements in internal_layers list - int
    for elem in internal_layers:
        if not isinstance(elem, int):
            print("Error: Layer ", elem, " is not an int.")
            return 0

    # num_forecast - int
    if not isinstance(num_forecast, int):
        print("Error: Number of forecast elements is not an integer.")
        return 0

    # Validate parameters' value
    # input_dimension - positive
    if input_dimension <= 0:
        print("Error: Invalid input dimension (has to be greater than 0).")
        return 0

    # output_dimension - positive
    if output_dimension <= 0:
        print("Error: Invalid output dimension (has to be greater than 0).")
        return 0

    # num_forecast - positive
    if num_forecast <= 0:
        print("Error: Invalid number of forecast elements (has to be greater than 0).")
        return 0

    # elements in internal_layers list - positive
    for elem in internal_layers:
        if elem < 1:
            print("Error: Invalid internal layers.")
            return 0

    # Checking that the dataframes are all complete
    if not ts[utils.complete]:
        print("Error: Data is not complete.")
        return 0

    # Checking that split_dfs dictionary is in ts
    if utils.split_dfs not in ts:
        print("Error: Modeling and Forecasting requires to run split_data first.")
        return 0

    # Checking that split_dfs dictionary is not empty
    if not ts[utils.split_dfs]:
        print("Error: split_dfs dictionary is empty.")
        return 0

    # iterate through split_dfs dictionary
    for elem in ts[utils.split_dfs]:
        # assign splited time series data to data variable
        data = ts[utils.split_dfs][elem]

        # convert timestamp on training datasets to unix datetime
        unixtime = timestamp_to_unixtime(data[utils.training][utils.timestamp])
        # temporary variable for recreating training data set
        temp = {utils.timestamp: unixtime, utils.magnitude: data[utils.training][utils.magnitude]}
        # train variable store training pandas data frame with unix datetime
        train = pd.DataFrame.from_dict(temp)

        # convert timestamp on validation datasets to unix datetime
        unixtime = timestamp_to_unixtime(data[utils.valid][utils.timestamp])
        # temporary variable for recreating validation data set
        temp = {utils.timestamp: unixtime, utils.magnitude: data[utils.valid][utils.magnitude]}
        # val variable store validation pandas data framewith unix datetime
        val = pd.DataFrame.from_dict(temp)

        # test variable store validation pandas data framewith
        test = data[utils.test]

        # initialize the modeling class object
        model = ModelingForecasting()
        # set the mlp layers
        model.mlp_model(input_dimension, output_dimension, internal_layers)
        # train the model. If success it will forecast the result following the
        # num_forecast parameter
        if model.train(train, val):

            # performing prediction
            # pred variable will store prediction result
            pred = []
            # dic_leng is the length of the pandas data frame i
            dic_leng = len(ts[utils.data_frames_dict][elem][utils.magnitude])
            # test_len is the length of test data set
            test_len = test.shape[0]
            # prev_mag store n previous magnitudes where n is equal to input dimension
            prev_mag = ts[utils.data_frames_dict][elem][utils.magnitude][dic_leng - input_dimension - test_len:
                                                                         dic_leng - test_len]
            # convert prev_mag to numpy array
            prev_mag = np.array([prev_mag])

            # loop num_forecast times to predict the magnitudes
            for j in range(num_forecast):
                # store prediction result of each iteration
                pred_each = model.forecast(prev_mag)
                # append the result from the prediction to pred list
                if isinstance(pred_each[0], float):
                    pred.append(pred_each[0])
                else:
                    pred.append(pred_each[0][0])

                # convert prev_mag to python list
                prev_mag = prev_mag.tolist()
                # append the latest prediction result to the input list for the next iteration
                if isinstance(pred_each[0], float):
                    prev_mag[0].append(pred_each[0])
                else:
                    prev_mag[0].append(pred_each[0][0])
                # pop the oldest data out
                prev_mag[0].pop(0)
                # convert prev_mag to numpy array
                prev_mag = np.array(prev_mag)

            # create timestamp list that will match the forecast result.
            tim_sta = test[utils.timestamp][:num_forecast]

            # combine the timestamp and prediction
            tim_ser = {utils.timestamp: tim_sta, utils.magnitude: pred}

            # checking that forecasts dictionary is existed or not. If not
            # , create it.
            if utils.forecasts not in ts:
                ts[utils.forecasts] = {}

            # convert python dictionary to pandas dataframe
            df = pd.DataFrame.from_dict(tim_ser)
            ts[utils.forecasts][elem] = df
        else:
            print("Error: Fail to train ", elem, " dataset")
    return ts


"""
*   Class: Modeling_Forecasting
*   Description: A class that contain a multi-layers perceptron performing
*                prediction from the given input.
*   Date: 24 Jan 2021
*   Edit History: v1.0: Creating all the function. *Not testing anything yet
*                 29 Jan 2021
*                 v1.1: Update on input output dimensions. Training can
*                       calculating the accuracy of the model. Add accuracy
*                       function.
*                 31 Jan 2021
*                 v1.2: Change from multi-layers perceptron classifier to
*                       regressor
*                 3 Feb 2021
*                 v1.3: Fixed: unable to train the model when y has n * m
*                       dimension
*                 4 Feb 2021
*                 v1.4: Fixed: data conversion warning in train()
"""


class ModelingForecasting:
    def __init__(self):
        """
        Internal class variable:
            model -> Multi-layers perceptron models
            in_di -> input dimension
            out_di -> output dimension
        """
        self.model = None
        self.in_di = 0
        self.out_di = 0

    def mlp_model(self, input_dimension, output_dimension, internal_layers):
        """
        initialize the model according to the user's model internal layers
        """
        self.in_di = input_dimension
        self.out_di = output_dimension
        self.model = MLPRegressor(hidden_layer_sizes=internal_layers, validation_fraction=0,
                                  solver='adam', max_iter=10000)

    def train(self, train, val):
        """
        Train the model with the time series data set and validating the
        accuracy of the model with validation data set. Return 1 on success and
        0 on failure.
        """
        # convert data in dataframe to integer and convert pandas dataframe to
        # python list
        train = train.astype('int')
        train_list = train[utils.magnitude].values.tolist()
        val = val.astype('int')
        val_list = val[utils.magnitude].values.tolist()

        # check training and validation dataset dimension with the input and
        # output dimension
        if self.in_di + self.out_di > len(train_list):
            print("Error: input + output dimension is larger than length of training dataset.")
            return 0
        if self.in_di + self.out_di > len(val_list):
            print("Error: input + output dimension is larger than length of validation dataset.")
            return 0

        # initialize variable for training and validating
        # x_train store input values of the training data set
        x_train = []
        # y_train store output values of the training data set
        y_train = []
        # x_val store input values of the validation data set
        x_val = []
        # y_val store output values of the validation data set
        y_val = []

        # arrange data into 2D list for training and validating
        for i in range(len(train_list) - self.in_di - self.out_di + 1):
            x_train.append(train_list[i:i + self.in_di])
            y_train.append(train_list[i + self.in_di:i + self.in_di + self.out_di])
        for i in range(len(val_list) - self.in_di - self.out_di + 1):
            x_val.append(val_list[i:i + self.in_di])
            y_val.append(val_list[i + self.in_di:i + self.in_di + self.out_di])

        # convert python list to numpy array
        x_train = np.array(x_train)
        y_train = np.array(y_train)
        x_val = np.array(x_val)
        y_val = np.array(y_val)

        # train the model
        if self.out_di > 1:
            self.model.fit(x_train, y_train)
        else:
            self.model.fit(x_train, y_train.ravel())

        #   calculating the score of the model
        print('Accuracy testing : {:.3f}'.format(self.model.score(x_val, y_val)))

        return 1

    def forecast(self, x_pred):
        """
        Predict the magnitude according to the input time stamps.
        """
        # y_pred store the result from prediction
        y_pred = self.model.predict(x_pred)
        return y_pred
