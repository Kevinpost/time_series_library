# Title:            main_test.py
# Project:          Transformation Tree Library
# Description:      Main testing file for testing modules.

# Team:             The Hackson Five

# Last Modified by: Kuan-Yun Chao
# Date Modified:    9 Feb 2021

import test_modules_component_1
import test_function as TF
import pandas as pd
import tree
import os

"""
*   Function: clear
*   Description: A function that cleans the console output
*
*   Date: 24 Jan 2021
*   Edit History: v1.1 (1/29/2021):
*                 Since node and tree will produce printings, it was meant to clean its console output
*                 v1.1 and v1.2 (2/2/2021):
*                 After failures trying to suppress printings from other module with different methods,
*                 this function is now abandoned and unused.
"""


def clear():
    os.system('cls')


"""
*   Function: result
*   Description: A function that creates test objects, generating appropriate data set for testing,
*                finishing up the test, and return tuple - result from testing functionalities 
*                on an empty tree, resulted list from calling the test interface for preprocessing.
*                It take empty list for appending test results and arguments for the preprocessing
*                if user want to see before and after effect of to a time series list
*                using preprocess function.
*
*   Date: 24 Jan 2021
*   Edit History: v1.0 (1/19/2021):
*                 Initial Frame was built for calling tests as an interaction layer to the
*                 other test modules.  
*                 v1.1.1.1 (1/29/2021):
*                 Initial test calls for testing node was completed, it generate 
*                 appropriate data by itself (no argument required) and calls other test 
*                 modules for testing. Verified the node functions can be operated just like a tree. 
*                 Notified member for moving its tree functionalities into tree.py.
*                 v1.2.1.1 (2/1/2021):
*                 Since relative test functions broke, 
*                 commenting out andor removed affected test function calls. Initial skeleton for tree 
*                 testing is built in wish to replace old tests for node.
*                 v1.2.2.2 (2/2/2021):
*                 Now it only calls tree test function.
*                 v1.2.3.3 (2/6/2021):
*                 All test calls previously for testing node functionalities were removed.
*                 v1.2.3.3 (2/9/2021):
*                 Initial test interface was made for testing preprocessing module. 
*                 Argument were coded to accommodate user access to the preprocessing
*                 test. 
*                 Caught preprocessing bugs if the outlier was at the beginning of the time series.
*                 Bug fixed.
*                 Found the preprocessing is not sensitive enough for small outliers.
*                 Not fixed and verified it is an expected behaviour of the outlier preprocessing
*                 function.
"""


def result(ret, column: str = "", index: str = "", pandas_data_frame_dict=None, process_function: str = "", a=None,
           b=None):
    """
    Test Section for tree.py
    """
    # call function that compare add, remove, and helper function result by adding PIMV to a depth four tree
    # then remove PIMV from the tree
    # P is PrepNode, I is InputNode, M is ModNode, and V is VisNode
    test_1 = test_modules_component_1.TestModulesComponent_1()
    result1 = tree.TFormTree()

    result2 = test_1.Tree_Test_Section_1(result1,
                                         "treenodes_compatability_add_remove_tester")
    if result2[0]:
        ret.append("treenodes_compatibility_add_remove_tester for tree.py : success!")
        for i in range(len(result2[1])):
            ret.append(result2[1][i])
    else:
        ret.append("treenodes_compatability_add_remove_tester for tree.py : failed!\n")
        ret.append(result2[1])

    """
    Test Section for preprocess.py
    """

    test_2 = test_modules_component_1.TestModulesComponent_1()
    result3 = test_2.Preprocess_Function_Interface_1(pandas_data_frame_dict, column, index,
                                                     process_function, a, b)
    return ret, result3


"""
*   Function: example()
*   Description: An example function tells how generate_random_time_series() is used in conjunction
*                to the preprocessing interface and the result of it using a specific preprocess
*                function - impute_outliers. Returns test result appended to the empty list, a 
*                generated list before calling impute_outliers() , and a list after calling
*                impute_outliers().        
*
*   Date: 24 Jan 2021
*   Edit History: v1.0 (1/19/2021):
*                 Initial Frame was built for calling result and interact with user inputs. 
*                 A simple call was created. No argument needed.
*                 v1.2.3.3 (2/9/2021):
*                 Initial test interface was made for testing preprocessing module.
*                 Caught preprocessing bugs if the outlier was at the beginning of the time series.
*                 Bug fixed.
*                 Found the preprocessing is not sensitive enough for small outliers.
*                 Not fixed and verified it is an expected behaviour of the outlier preprocessing
*                 function.
"""


def example():
    # list for test result
    li = []

    # using generate_random_time_series() to generate a time series with constant range
    default_magnitude = [-110, 150]
    number_of_datapoints = 90
    # control list: base magnitude, outliers, zero,and flat in an boolean array - [Bool,Bool,Bool,Bool]
    control_list = [True, True, True, True]
    outliers = [10, [-1000010, 1000050]]  # [number of outliers, magnitude of outliers from low to high]
    nones = 10  # number of nones for the none list
    flats = [10, -2]  # [length of a flat sequence, magnitude of the flat sequence]

    input_time_series = TF.generate_random_time_series(default_magnitude, number_of_datapoints,
                                                       control_list, outliers, nones, flats)

    magnitude_series = pd.Series(input_time_series)  # create series of magnitude values from list

    temp_list_2 = []
    for item in input_time_series:
        if item is None:
            temp_list_2.append(None)
        else:
            temp_list_2.append(item + 1)
    magnitude_series_2 = pd.Series(temp_list_2)
    time_series = pd.date_range("2021-01-01 00:00:00", periods=len(input_time_series), freq="10MIN")
    data = {'TimeStamp': time_series, 'Magnitude': magnitude_series}  # dictionary of series
    pandas_data_frame = pd.DataFrame(data)  # Create DataFrame object
    time_series_2 = pd.date_range("2021-03-13 00:00:00.000", periods=len(input_time_series), freq="25MIN")
    data_2 = {'TimeStamp': time_series_2, 'Magnitude': magnitude_series_2}
    pandas_data_frame_2 = pd.DataFrame(data_2)
    pandas_data_frame_dict = \
        {'data_frames_dict': {'0': pandas_data_frame, '1': pandas_data_frame_2}, 'complete_flag': False}

    ret, comparison = result(li, 'Magnitude', '0', pandas_data_frame_dict, "impute_outliers", 20)
    for item in ret:
        print(item)
    print("This is just a running example! \n")
    print("using generate_random_time_series() to generate a time series with constant range with following inputs: \n \
          default_magnitude (expected time series without errors) =  [-110,150] \n \
          number_of_datapoints (how many data points a time series should have) = 90 \n \
          control_list (A control list telling what error to be added to the list) = [True, True, True, True] \n \
          [generate default time series, add outliers to the time series, add number of None to the time series ,add a "
          "block of constant magnitude to the time series] \n \
          outliers (number of outliers with maximum and minimum outlier) = [10, [-1000010, 1000050]] \n \
          [number of outliers [min outlier magnitude, max outlier magnitude]] \n \
          nones (number of nones) = 10 \n \
          flats (length of the constant magnitude, constant) = [10, -2] \n"
          )
    print("processing 'impute_outliers' with 20 neighbors \n")
    print(
        "hard-coded test_result from tree.py, input time series, and comparison result after preprocessing.py "
        "are returned for further inspection, especially preprocess.py \n")
    print(("numbers of input data points: {}, after removing outliers: {} \n").format(len(input_time_series),
                                                                                      len(comparison)))
    total_removed = len(input_time_series) - len(comparison)
    percent_removed = float(total_removed / (nones + outliers[0]))
    print("Note: the outlier detection is insensitive, so multiple runs will be needed \n")
    print("Percent removed compared to number of errors we input: {}% \n".format(percent_removed))

    return ret, input_time_series, comparison


def main():
    example()


if __name__ == "__main__":
    main()
