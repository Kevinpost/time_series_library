"""
* Title:            visualization.py
* Project:          Transformation Tree Library
* Description:      Prototype implementations of the visualization function and its associated functions
*
* Team:             The Hackson Five
*
* Last Modified by: Sarah Hartley
* Date Modified:    07 Feb 2021
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from utils import split_data
from matplotlib.backends.backend_pdf import PdfPages
from scipy import stats
import scipy
import utils

# This is accessed in node.py to bind the user options to the different modes here
modes = ["histogram", "plot", "boxplot", "normalitytest", "mse", "mape", "smape"]


def check_output_pdf(outputpath: str):
    """
            Input:
                A string that is a path to a pdf
            Expected behavior:
                Checks a string to see if string has a "pdf" extension
            Output:
                True on success, False on failure
            """
    # checks the output string
    if outputpath.endswith('.pdf'):
        return True
    else:
        print('Error:Output path must be a pdf ')
        return False


def visualize(pandas_data_frame_dict, visualize_function: str, plot_type=None, path=None, orientation=None):
    """
    Input:
        A Pandas Data Frame Object
    Expected behavior:
        Takes the Pandas Data Frame dictionary and applies the function
            specified by 'visualize data' to the data frame dictionary.
    Output:
        Returns the modified Pandas Data Frame Object on Success, returns 0 on failure
    """

    # Checks to see if the data frame dicitonary key is present
    if 'data_frames_dict' not in pandas_data_frame_dict:
        print("'data_frames_dict' is required element of dictionary passed to visualization()")
        return 0

    # Calls the check_forecast function to check if there are forecast values
    check_forecast(pandas_data_frame_dict)

    # makes a reference to the data frames dictionary and forecast dictionary
    data_frames_dict = pandas_data_frame_dict['data_frames_dict']
    forecast_dict = pandas_data_frame_dict['forecast']

    # goes through the data frame dictionary to check that Magnitude values are present
    for element in data_frames_dict:
        # if the magnitude key is not in the dictionary
        if 'Magnitude' not in data_frames_dict[element]:
            print("'Magnitude' is a required element of DataFrame dictionaries for visualize() function")
            return 0
        else:
            # sets any none values to nan values so they can be plotted
            data_frames_dict[element]['Magnitude'] = data_frames_dict[element]['Magnitude'].fillna(value=np.nan)

    # calls the correct helper function based on the value of 'visualize function'
    if visualize_function == 'histogram':
        histogram_helper(pandas_data_frame_dict, plot_type, path, orientation)
    elif visualize_function == 'plot':
        plot_helper(pandas_data_frame_dict, plot_type, path)
    elif visualize_function == 'boxplot':
        box_plot_helper(pandas_data_frame_dict, plot_type, path)
    elif visualize_function == 'normalitytest':
        normality_helper(pandas_data_frame_dict, plot_type, path)
    elif visualize_function == 'mse':
        # goes through element in the data frame dictionary calculates the MSE. adds value to dictionary
        for element in data_frames_dict:
            if bool(forecast_dict):
                if check_errors(pandas_data_frame_dict):
                    # tries to add the MSE value to the dictionary. Otherwise prints an error message
                    try:
                        value = error_helper('mse', pandas_data_frame_dict['split_dfs'][element]['test'],
                                             forecast_dict[element], element)
                        new_value = round(value, 4)
                        add_stat_results(pandas_data_frame_dict, 'mse', new_value, element)
                    except KeyError:
                        print(f'Did not calculate MSE for dataframe {element} due to invalid input')

            else:
                print('Forecast needed in order to calculate MSE')

    elif visualize_function == 'mape':
        # goes through element in the data frame dictionary calculates the MAPE. adds value to dictionary
        for element in data_frames_dict:
            # Checks to make sure there is a forecast dictionary with values
            if bool(forecast_dict):
                # tries to add the MAPE value to the dictionary. Otherwise prints an error message
                if check_errors(pandas_data_frame_dict):
                    try:
                        value = error_helper('mape', pandas_data_frame_dict['split_dfs'][element]['test'],
                                             forecast_dict[element], element)
                        value = round(value, 4)
                        add_stat_results(pandas_data_frame_dict, 'mape', value, element)
                    except KeyError:
                        print(f'Did not calculate MAPE for dataframe {element} due to invalid input')
            else:
                print('Forecast needed in order to calculate MAPE')

    elif visualize_function == 'smape':
        # goes through element in the data frame dictionary calculates the MAPE. adds value to dictionary
        for element in data_frames_dict:
            # tries to add the MAPE value to the dictionary. Otherwise prints an error message
            if bool(forecast_dict):
                if check_errors(pandas_data_frame_dict):
                    try:
                        value = error_helper('smape', pandas_data_frame_dict['split_dfs'][element]['test'],
                                             forecast_dict[element], element)
                        value = round(value, 4)
                        add_stat_results(pandas_data_frame_dict, 'smape', value, element)
                    except KeyError:
                        print(f'Did not calculate SMAPE for dataframe {element} due to invalid input')
            else:
                print('Forecast needed in order to calculate SMAPE')
    # if no visualize functions were valid, prints an error message
    else:
        print("Error: invalid function")
        return 0
    # returns the pandas data frame dictionary
    return pandas_data_frame_dict


def plot_helper(pandas_data_frame_dict, plot_type: str = '', path=None, title: str = ''):
    output = None
    # checks to see if a path was given
    if path:
        # checks to see if the path is a valid path
        if check_output_pdf(path):
            output = PdfPages(path)
    # creates a reference to the data frames dictionary and forecast dictionary
    data_frames_dict = pandas_data_frame_dict['data_frames_dict']
    forecast_dict = pandas_data_frame_dict['forecast']
    # if the plot type is forecast, does appropriate error checks
    if plot_type == 'forecast':
        if bool(forecast_dict):
            for element in forecast_dict:
                plot(forecast_dict[element], plot_type=plot_type, path=output, title=element)
        else:
            print('Cannot plot forecast data, as there is no forecast data available')
    # checks if the plot type is actual and forecast data, or if it has not been set
    elif plot_type == 'actualandforecast' or not plot_type:
        # if you only have a forecast
        if not data_frames_dict:
            # if there is a forecast dictionary with data in it
            if not forecast_dict:
                # goes through the elements in the dictionary, drops Nan values, and passes them to
                # box_plot function
                print('Error: Cannot plot, no data given')
            else:
                for element in forecast_dict:
                    plot(forecast_dict[element], plot_type='Forecast', path=output)
                print('Did not graph any actual data, as only forecast data was given')

        for element in data_frames_dict:
            try:
                plot(data_frames_dict[element], forecast_dict[element], path=output, plot_type=plot_type, title=element)
            except KeyError:
                plot(data_frames_dict[element], path=output, plot_type='Actual Data', title=element)

    elif plot_type == 'actual':
        if not data_frames_dict:
            print('Cannot graph the data as no data has been given')
        else:
            for element in data_frames_dict:
                plot(data_frames_dict[element], path=output, plot_type='Actual Data', title=element)
    else:
        print(f'Unrecognized plot type: {plot_type}')
    if output:
        output.close()
        print(f'Saved plot to {path}')


def histogram_helper(pandas_data_frame_dict, plot_type: str = '', path=None, orientation=None):
    """
    Input:
        A pandas data frame dictionary, an optional plot type, an optional path, an optional title
    Expected behavior:
        Helper function for the histogram function.
    Output:
        None
    """
    output = None
    # checks to see if a path was given
    if path:
        # checks to see if the path is a valid path
        if check_output_pdf(path):
            output = PdfPages(path)
    # creates a reference to the data frames dictionary and forecast dictionary
    data_frames_dict = pandas_data_frame_dict['data_frames_dict']
    forecast_dict = pandas_data_frame_dict['forecast']
    # if the plot type is forecast, does appropriate error checks
    if plot_type == 'forecast':
        # if there is a forecast dictionary with data in it
        if bool(forecast_dict):
            # goes through the elements in the dictionary, drops Nan values, and passes them
            # to histogram function
            for element in forecast_dict:
                histogram(forecast_dict[element], plot_type=plot_type, path=output, orientation=orientation,
                          title=element)
        else:
            print('Error: Unable to plot forecast data as no forecast data was given')
    # checks if the plot type is actual and forecast data, or if it has not been set
    elif plot_type == 'actualandforecast' or not plot_type:
        # if you only have a forecast
        if not data_frames_dict:
            if not forecast_dict:
                print('Error: Cannot plot, no data given')
            else:
                for element in forecast_dict:
                    histogram(forecast_dict[element], plot_type='Forecast', path=output, orientation=orientation,
                              title=element)
                print('Did not graph any actual data, as only forecast data was given')
        for element in data_frames_dict:
            try:
                histogram(data_frames_dict[element], forecast_dict[element], path=output, plot_type=plot_type,
                          orientation=orientation, title=element)
            except KeyError:
                histogram(data_frames_dict[element], path=output, plot_type='Actual Data', orientation=orientation,
                          title=element)
    elif plot_type == 'actual':
        if not data_frames_dict:
            print('Cannot graph the data as no data has been given')
        else:
            for element in data_frames_dict:
                histogram(data_frames_dict[element], path=output, plot_type='Actual Data', orientation=orientation,
                          title=element)
    else:
        print(f'Unrecognized plot type: {plot_type}')
    # if there is a PdfPages output active, closes it and prints the location
    if output:
        output.close()
        print(f'Saved plot to {path}')


def box_plot_helper(pandas_data_frame_dict, plot_type: str = '', path=None, title=''):
    """
    Input:
        A pandas data frame dictionary, an optional plot type, an optional path, an optional title
    Expected behavior:
        Helper function for the box_plot function.
    Output:
        None
    """
    # initializes the output to None
    output = None
    # checks to see if a path was given
    if path:
        # checks to see if the path is a valid path
        if check_output_pdf(path):
            output = PdfPages(path)
    # creates a reference to the data frames dictionary and forecast dictionary
    data_frames_dict = pandas_data_frame_dict['data_frames_dict']
    forecast_dict = pandas_data_frame_dict['forecast']

    # if the plot type is forecast, does appropriate error checks
    if plot_type == 'forecast':
        # if there is a forecast dictionary with data in it
        if bool(forecast_dict):
            # goes through the elements in the dictionary, drops Nan values, and passes them to box_plot function
            for element in forecast_dict:
                box_plot(forecast_dict[element].dropna(), plot_type=plot_type, path=output, title=element)
        else:
            print('Cannot plot forecast data, as there is no forecast data available')
    # checks if the plot type is actual and forecast data, or if it has not been set
    elif plot_type == 'actualandforecast' or not plot_type:
        # checks  for a data frames dictionary
        if not data_frames_dict:
            # if there is no data frame with actual data, graphs only the forecst data
            for element in forecast_dict:
                box_plot(forecast_dict[element].dropna(), plot_type='forecast', path=output, title=element)
            print('Did not graph any actual data, as only forecast data was given')
        # goes through the elements in the data frames dictionary
        for element in data_frames_dict:
            # tries to pass both the actual and forecast data to the box plot function, if failure, then passes
            # only actual data
            try:
                box_plot(data_frames_dict[element].dropna(), forecast_dict[element].dropna(), path=output,
                         plot_type=plot_type, title=element)
            except KeyError:
                box_plot(data_frames_dict[element].dropna(), path=output, plot_type='Actual', title=element)
                print(f'Plotted only actual data for element {element}')
    # checks if the plot type is 'actual'
    elif plot_type == 'actual':
        # if there is no actual data given, prints an error message
        if not data_frames_dict:
            print('Cannot plot data, no data given')
        # if actual data is given, passes it to the box and whiskers function
        else:
            for element in data_frames_dict:
                box_plot(data_frames_dict[element].dropna(), path=output, plot_type=plot_type, title=element)
    # no invalid plot type was given
    else:
        print(f'Unrecognized plot type: {plot_type}')
    # if there is a PdfPages output active, closes it and prints the location
    if output:
        output.close()
        print(f'Saved plot to {path}')


def histogram(data_frame, data_frame_forecast=None, plot_type=None, path=None, orientation=None, title=''):
    """
    Input:
        A pandas data frame, an optional forecast data frame, an optional plot type, an optional path,
        and a title for the graph
    Expected behavior:
        Plots graph of a data frame(s). Saves to a specified PDF if given a path.
    Output:
        None
    """
    # checks to see if an orientation value was give. if not, sets to vertical
    if not orientation:
        orientation = 'vertical'
    # if a title was given, adds the title to the graph
    if title:
        plt.title(f'Histogram of Data for TS {title}')
    else:
        plt.title('Histogram of Data')

    # checks to see if there is a second data frame given
    if data_frame_forecast is not None:
        # plots the second data frame with appropriate labels
        plt.hist([data_frame['Magnitude'], data_frame_forecast['Magnitude']],
                 label=['Magnitude Values', 'Forecast Values'], stacked=True, orientation=orientation)
    else:
        # plots the single data frame
        plt.hist(data_frame['Magnitude'], label=plot_type.capitalize() + ' ' + 'Values', orientation=orientation)
    # labels the y axis with 'Magnitude'
    plt.ylabel('Magnitude')
    # shows the legend on the graph
    plt.legend()
    # if a path is specified, saves the plot to that path
    if path:
        path.savefig()
    plt.show()


def plot(data_frame, data_frame_forecast=None, plot_type='', path=None, title: str = ''):
    """
    Input:
        A pandas data frame, an optional forecast data frame, a plot type, a path, and a title for the graph
    Expected behavior:
        Plots graph of a data frame(s). Saves to a specified PDF if given a path.
    Output:
        None
    """
    # creates subplots
    fig, ax = plt.subplots()

    # if there is a title, adds it to the graph
    if title:
        plt.title(f'Plotted Data for TS {title}')
    else:
        plt.title('Plotted Data')
    # labels the y axis with 'Magnitude'
    plt.ylabel('Magnitude')
    # checks to see if there are TimeStamps in the data frame. If so, adds them to X label
    if 'TimeStamp' in data_frame:
        time_series_list = data_frame['TimeStamp']
        plt.tight_layout()
        plt.xlabel('Time')

        # checks to see if there is a second data frame provided. if so, plots it with appropriate labels
        if data_frame_forecast is not None:
            ax.plot_date(time_series_list, data_frame['Magnitude'], 'b-', label='Actual values')
            ax.plot_date(data_frame_forecast['TimeStamp'], data_frame_forecast['Magnitude'], 'b-',
                         label='Forecast Values', color='r')
        else:
            # plots just the first data frame with appropriate labels
            ax.plot_date(time_series_list, data_frame['Magnitude'], 'b-', label=plot_type.capitalize() + ' ' + 'Values')
        # rotates dates on X axis for easier readability
        fig.autofmt_xdate()
    # plots the data without timestamps
    else:
        # creates a reference to the data frame 'Magnitude' list
        actual_value_list = data_frame['Magnitude']
        # creates an empty list for labels for the data
        actual_data_label = []
        # checks to see if there is a forecast data frame as well
        if data_frame_forecast is not None:
            # creates a reference to the forecast 'Magnitude' list
            forecast_list = data_frame_forecast['Magnitude']
            # creates an empty list for labels for the data
            forecast_data_label = []
            # initializes variables for the length of the two Magnitude lists
            act_length = len(actual_value_list)
            fore_length = len(forecast_list)
            # goes through the length of the two lists and adds the index to the appropriate list based on index
            for i in range(act_length + fore_length):
                if i >= act_length:
                    forecast_data_label.append(i + 1)
                else:
                    actual_data_label.append(i + 1)
            # plots the data frame data with the labels from the two label lists
            ax.plot(actual_data_label, actual_value_list, 'b-', label='Actual values')
            ax.plot(forecast_data_label, forecast_list, 'b-', label='Forecast Values', color='r')
        # if there is only 1 data frame, plots it with appropriate labels
        else:
            ax.plot(data_frame['Magnitude'], 'b-', label=plot_type.capitalize() + ' ' + 'Values')

    # Creates a legend for the plot
    plt.legend()

    # If a path was specified, saves the plot to the path
    if path:
        path.savefig()
    # shows the plot
    plt.show()


def five_num_summary(data_frame):
    """
    Input:
        A pandas data frame
    Output:
        Returns the 5 number summary as a concatenated string
    """
    # Calculates mean, minimum, maximum, and quantile values
    mean = data_frame['Magnitude'].mean()
    minimum = data_frame['Magnitude'].min()
    df_max = data_frame['Magnitude'].max()
    quartiles = data_frame['Magnitude'].quantile([0.25, 0.5, 0.75])

    # Joins these numbers as a single string
    five_num_sum = '\n'.join((
        r'Minimum =%.1f ' % (minimum,),
        r'Q1 =%.1f ' % (quartiles[0.25],),
        r'Median =%.1f ' % (quartiles[0.5],),
        r'Q3 =%.1f ' % (quartiles[0.75],),
        r'Maximum =%.1f ' % (df_max,),
    ))
    return five_num_sum


def box_plot(data_frame, data_frame_forecast=None, plot_type='', path=None, title: str = ''):
    """
    Input:
        A pandas data frame, an optional forecast data frame, a plot type, a path, and a title for the graph
    Expected behavior:
        Plots the box and whiskers graph of a data frame. Saves to a specified PDF if given a path.
    Output:
        None
    """

    fig, ax = plt.subplots()

    # If a title is specified add it to the plot
    if title:
        plt.title(f'Box and Whiskers of Data for TS {title}')
    else:
        plt.title('Box and Whiskers of Data')

    # Labels the Y-Axis as 'Magnitude'
    plt.ylabel('Magnitude')

    # Sets five_num_sum to the value of the five number summary of the data frame
    five_num_sum = five_num_summary(data_frame)

    # Creates a box used a sa bounding box for the 5 number summary
    box = dict(boxstyle='round', alpha=0.2, color='darkgrey')

    # Adjusts the plot so the five number summary can fit at the bottom
    fig.subplots_adjust(bottom=0.3)

    # Checks to see if there is a second data frame provided
    if data_frame_forecast is not None:
        # Calculates the five number summary for this data frame
        forecast_five_num_sum = five_num_summary(data_frame_forecast)

        # Plots the Magnitude values of both data frames and adds appropriate labels and 5 number summaries
        ax.boxplot([data_frame['Magnitude'], data_frame_forecast['Magnitude']], vert=True)
        plt.figtext(0.2, 0.05, five_num_sum, bbox=box)
        plt.figtext(0.6, 0.05, forecast_five_num_sum, bbox=box)
        plt.xticks([1, 2], ['Magnitude Values', 'Forecast values'])
    else:
        # If there is no second data frame, plots only the first one
        ax.boxplot(data_frame['Magnitude'], vert=True)

        # Adds appropriate labels and 5 number summaries
        plt.figtext(0.4, 0.05, five_num_sum, bbox=box)
        plt.xticks([1], [plot_type.capitalize() + ' ' + 'Magnitude Values'])
    # If a path was provided, saves the plot to the path
    if path:
        path.savefig()
    # show the plot
    plt.show()


def normality_helper(pandas_data_frame_dict, plot_type: str = '', path=None):
    """
    Input:
        A pandas data frame dictionary, a plot type, and a path
    Expected behavior:
        Helper function for normality test. Checks to see which type of plot is being made, does appropriate error
        checks before passing the data frame to the normality function.
    Output:
        Saves graphs of normality tests to a pdf
    """

    # Initializes output to None
    output = None

    # Checks to see if a path has been specified
    if path:
        # Checks to see if this is a valid output path. if so, created a Pdfpages output
        if check_output_pdf(path):
            output = PdfPages(path)

    # Makes a reference to the data frames dictionary and forecast dictionary
    data_frames_dict = pandas_data_frame_dict['data_frames_dict']
    forecast_dict = pandas_data_frame_dict['forecast']

    # If the plot type is forecast, makes sure that there are valid values
    if plot_type == 'forecast':
        # Checks to ensure there are forecast values
        if bool(forecast_dict):
            # Goes through data frame in the forecast dictionary
            for element in forecast_dict:
                # Checks to see if the number of values is greater than 20 for accurate normality test results
                if forecast_dict[element]['Magnitude'].dropna().size < 20:
                    print('Error: Normality tests only valid for data sets with >= 20 valid values')
                else:
                    # Calculates the normality values
                    value = normality_test(forecast_dict[element], plot_type=plot_type, path=output, title=element)
                    # adds the value to the data frame
                    add_stat_results(pandas_data_frame_dict, 'normality', value, element, plot_type='Forecast')
        else:
            print('Cannot find normality of forecast data, as no forecast data has been given')
    # if the plot type is actual, or if a plot type was not given
    elif plot_type == 'actual' or not plot_type:
        # if there is not a data frames dictionary
        if not data_frames_dict:
            print('Cannot find normality as no data has been given')
        else:
            for element in data_frames_dict:
                # checks to see if there are more than 20 values when invalid values were taken out
                if data_frames_dict[element]['Magnitude'].dropna().size < 20:
                    print('Error: Normality tests only valid for data sets with >= 20 valid values')
                else:
                    #
                    value = normality_test(data_frames_dict[element],
                                           path=output,
                                           plot_type='Actual Data',
                                           title=element)
                    add_stat_results(pandas_data_frame_dict,
                                     'normality',
                                     value,
                                     element,
                                     plot_type='Actual')
    # if it was not a recognized plot type, prints error message
    else:
        print(f'Error: Unrecognized plot type: {plot_type}')
    # if there was an output path, closes this output then prints a message
    if output:
        output.close()
        print(f'Saved plot to {path}')


def normality_test(data_frame, plot_type='', path=None, title=''):
    """
    Input:
        A pandas data frame, a plot type, a path, and a title
    Expected behavior:
        Calculates the normality value using the D'Agostino and Pearson's test. Plots the results
    Output:
        Returns the normality value. Plots the quarter quantile plot. Saves this to the path specified
        by 'path'
    """

    # Calculates the normality value
    value = stats.normaltest(data_frame['Magnitude'])

    # Plots the probability plot
    scipy.stats.probplot(data_frame['Magnitude'], plot=plt)

    # Adds the title to the graph
    if title:
        plt.title(f'Probability plot for {plot_type} of TS {title}')
    else:
        plt.title('Probability Plot')
    # if a path was provided, save the figure there
    if path:
        path.savefig()
    # show the plot
    plt.show()
    return value


def check_errors(pandas_data_frame_dict):
    """
    Input:
        A pandas data frame dictionary
    Expected behavior:
        Checks to see if parameters needed for calculating errors are in the dictionary
    Output:
        True if required keys are in dictionary, else: False
    """

    # Checks to see if 'split_dfs' and forecast key is not in dictionary
    if 'split_dfs' not in pandas_data_frame_dict and 'forecast' not in pandas_data_frame_dict:
        print('Error: Must have forecast values and test values in order to calculate error percentages')
        return False

    # Checks to see if 'split_dfs' key is not in dictionary
    elif 'split_dfs' not in pandas_data_frame_dict:
        print("Error: Must have test values in order to calculate error percentages."
              "Use 'split data' to split the time series. ")
        return False

    # Checks to see if 'forecast' key is not in dictionary
    elif 'forecast' not in pandas_data_frame_dict:
        print('Error: Must have forecast values in order to calculate error percentages')
        return False

    return True


def error_helper(visualize_function: str, data_frame_test, data_frame_forecast, df_num):
    """
    Input:
        A visualize function name, a data frame of test values, a data frame of forecast values,
        and a data frame number
    Expected behavior:
        Takes two data frames and appends the values at identical timestamps to lists. Then passes these
        lists into functions that calculate either the MSE, MAPE, SMAPE, or normality depending on the
        visualize function value
    Output:
        Returns the value of the type of error specified by 'visualize function'
    """
    # Initializes two empty lists
    y_test_list = []
    y_forecast_list = []

    # Checks to see if there are timestamps available to accurately calculate errors based on correct times
    if 'TimeStamp' not in data_frame_test or 'TimeStamp' not in data_frame_forecast:
        print(f'Calculating {visualize_function.upper()}'
              f' for DF {df_num} without TimeStamps due to invalid or no TimeStamp input')

        # Makes the two lists the same size, getting rid of the data at the end
        num_values = min(len(data_frame_test['Magnitude']), len(data_frame_forecast['Magnitude']))
        y_test_list = data_frame_test['Magnitude'][:num_values].dropna()
        y_forecast_list = data_frame_forecast['Magnitude'][:num_values].dropna()

    # If there are no timestamps, creates arrays to be passed to functions without them
    else:
        # Drops the nan values from the data and forecast arrays and resets the indices
        data_frame_test = data_frame_test.dropna()
        data_frame_test = data_frame_test.reset_index(drop=True)
        data_frame_forecast = data_frame_forecast.dropna()
        data_frame_forecast = data_frame_forecast.reset_index(drop=True)

        # Loops through the test timestamp array
        test_size = data_frame_test['TimeStamp'].size
        forecast_size = data_frame_forecast['TimeStamp'].size
        for i in range(test_size):
            # Loops through the forecast timestamp array
            for j in range(forecast_size):
                if data_frame_test['TimeStamp'][i] == data_frame_forecast['TimeStamp'][j]:
                    y_test_list.append(data_frame_test['Magnitude'][i])
                    y_forecast_list.append(data_frame_forecast['Magnitude'][j])

    if (len(y_test_list) == 0) or (len(y_forecast_list) == 0):
        print("Error: No comparable dates found between test data and forecast data")
        return None
    # Checks to see which kind of visualize function is being passed in
    if visualize_function == 'mse':
        return mse(y_test_list, y_forecast_list)
    elif visualize_function == 'mape':
        return mape(y_test_list, y_forecast_list)
    elif visualize_function == 'smape':
        return smape(y_test_list, y_forecast_list)
    else:
        print('Error: Did not recognize the function that was used as input')
        return None


def mse(y_test, y_forecast):
    """
    Input:
        An array of test values and an array of forecast values
    Expected behavior:
        Calculates the MSE error between the two arrays
    Output:
        Returns the MSE value, either a float or int on success. Returns None on Failure
    """
    # Checks if the lengths of the two arrays are equal
    if len(y_test) == len(y_forecast):
        total = 0

        # For each index in the arrays, calculates the MSE error between elements
        for i in range(len(y_test)):
            total += ((y_test[i] - y_forecast[i]) ** 2)

        # Finds the average MSE error by dividing total by the number of elements
        value = total / (len(y_test))

    else:
        print('Test and forecast must be the same size to find the MAPE value ')
        value = None

    return value


def mape(y_test, y_forecast):
    """
    Input:
        An array of test values and an array of forecast values
    Expected behavior:
        Calculates the MAPE error between the two arrays
    Output:
        Returns the MAPE value, either a float or int on success. Returns None on Failure
    """

    # Checks if the lengths of the two arrays are equal
    if len(y_test) == len(y_forecast):
        total = 0

        # For each index in the arrays, calculates the MAPE error between elements
        for i in range(len(y_test)):
            # sets the divisor value to the y test value
            divisor = y_test[i]
            # checks to make sure the divisor is not 0
            if divisor != 0:
                difference = abs((y_test[i] - y_forecast[i]) / divisor)
            # if the divisor is 0, does not divide the different
            else:
                difference = abs(y_test[i] - y_forecast[i])
            total += difference

        # Finds the average MAPE error by dividing total by the number of elements
        total = total / len(y_test)
    else:
        print('Test and forecast must be the same size to find the MAPE value ')
        total = None

    return total


def smape(y_test, y_forecast):
    """
    Input:
        An array of test values and an array of forecast values
    Expected behavior:
        Calculates the SMAPE error between the two arrays
    Output:
        Returns the SMAPE value, either a float or int on success. Returns None on Failure
    """
    # Checks if the lengths of the two arrays are equal
    if len(y_test) == len(y_forecast):
        total = 0
        # For each index in the arrays, calculates the SMAPE error between elements
        for i in range(len(y_test)):
            # finds the absolute values of the difference
            difference = abs((y_test[i] - y_forecast[i]))
            # divides this by the average of the two
            divisor = (abs(y_test[i]) + abs(y_forecast[i]))
            # checks to see if the divisor is not 0. if it is, does not divide by anything
            if divisor != 0:
                difference = difference / divisor
            total += difference
        # Finds the average SMAPE error by dividing total by the number of elements
        total = total / len(y_test)
        # multiply this number by 100 to find the percent
        total = total * 100
    # If the lengths are not equal, returns None
    else:
        print('Test and forecast must be the same size to find the MAPE value ')
        total = None

    return total


def check_forecast(pandas_data_frame_dict):
    """
    Input:
        A Pandas Data Frame Dictionary
    Expected behavior:
        Checks to see if there is a 'forecast' key in the dictionary
    Output:
        Adds forecast key if it is not present
    """
    if 'forecast' not in pandas_data_frame_dict:
        pandas_data_frame_dict['forecast'] = {}


def add_stat_results(pandas_data_frame_dict, visualize_function: str, val, df_num, plot_type=None):
    """
    Input:
        A Pandas Data Frame Object, a visualize function string, a value to add to the stat results,
        a data frame number, and the plot type.
    Expected behavior:
        Takes the Pandas Data Frame dictionary and checks to see if there is a 'stat_results' key.
        If there isn't, adds this key to the dictionary. Also adds the value to the corresponding
        key and function.
    Output:
        Modifies the pandas Data Frame
    """

    # Checks to see if 'stat_results' is a key in the data frame dictionary, if not, adds it
    if 'stat_results' not in pandas_data_frame_dict:
        pandas_data_frame_dict['stat_results'] = {}

    # Checks to see if the data frame number is a key in the data frame dictionary, if not, adds it
    if df_num not in pandas_data_frame_dict['stat_results']:
        pandas_data_frame_dict['stat_results'][df_num] = {}

    # Checks to see if there is a plot type passed in. If so, adds a key for values of that plot type
    if not plot_type:
        pandas_data_frame_dict['stat_results'][df_num][visualize_function] = val
    # plot type found, adds to either 'actual' or 'forecast' key
    else:
        val_key = visualize_function.capitalize() + ' ' + plot_type
        pandas_data_frame_dict['stat_results'][df_num][val_key] = val


def main():
    print("Visualization & Statistics")


if __name__ == "__main__":
    main()
