"""
* Title:            node.py
* Project:          Transformation Tree Library
* Description:      Prototype implementations of the OperationNode class and its child classes.
*
* Team:             The Hackson Five
*
* Last Modified by: Aaron Van Cleave and Kuan-Yun Chao
* Date Modified:    07 Feb 2021
"""

# Using copy allows a node to be deep copied
#       It's useful to make a totally separate recursive copy of a node and its children
import copy
# These four imports connect the node classes to the time series operation modules
import input as inp
import preprocess as prep
import model_forecast as mod
import visualization as vis
# This enables each node type to:
#       Execute the correct function for each module
#       Access correct execution modes for each module, respectively

# Global names for node types
#       Facilitates compatibility checking between nodes
default_type = "Default"
input_type = "Input"
prep_type = "Process"
mod_type = "Model"
vis_type = "Visual"


class OperationNode:
    """
    An OperationNode is the class that any node that will form part of a Transformation Tree inherits from
    It is an abstract class - not useful by itself and exhibiting only default behavior.
    """

    # Default attributes -
    node_type = default_type
    default_params = []
    default_mode = None
    default_child_types = []

    def __init__(self, node_mode=None, node_params=None):
        """
        Input:
            A string for node type
            A list of strings of compatible child node types
            A string (or None) for the node mode
            A list for node parameters
        Expected behavior:
            Instantiates a default node with the given default members, an empty list for children, and a None parent
            Invalid inputs are converted to legal inputs
        """

        # print("Creating node with " + str(node_mode) + ", " + str(node_params))

        # Parameters that aren't user-defined

        # The node type for compatibility checking
        self.type = self.node_type
        # This node's parent
        self.parent = None
        # The types of nodes that this node can parent
        self.child_types = self.default_child_types
        # This node's children
        self.children = []

        # Validate mode input
        if node_mode is None or not self.valid_mode(node_mode):
            node_mode = self.default_mode

        # Validate parameter input
        if node_params is not None:
            node_params = self.valid_params(node_params)
            if node_params == 0:
                node_params = self.default_params
        else:
            node_params = self.default_params

        # Set all the user-specified instance members
        # Mode is which type of function the node will execute
        self.mode = node_mode
        # Parameters are a list - they are user-defined, persistent, and affect node execution
        self.parameters = node_params

        print("Created node of type " + self.type +
              " with " + str(self.mode) +
              ", " + str(self.parameters) +
              ", " + str(self.child_types))

    def execute(self, node_input):
        """
        Input:
            Type varies among subclasses
        Expected behavior:
            No operation is performed in this default method
        Output:
            The input, unchanged
        """

        print("Executing " + self.type + " node")

        # By default, input is returned without modification
        return node_input

    def view_settings(self):
        """
        Expected behavior:
            Prints whether the mode and parameters are specified (that is, not None)
            If specified, prints their values
        """

        print("Settings of " + str(self) + ":")

        # Display mode if it is specified
        set_str = ("Mode: " + self.mode if self.mode is not None else "No mode") + ", "

        # Display parameters if they are specified
        if self.parameters:
            set_str = set_str + "Parameters:"

            # Print the current settings for the user
            for parameter in self.parameters:
                set_str = set_str + " " + str(parameter)
        else:
            set_str = set_str + "No parameters"

        print(set_str)

    def set_mode(self, new_mode: str or None):
        """
        Input:
            A string specifying a new mode for the node to take
        Expected behavior:
            Checks if the mode and type are compatible
            If so, sets the new mode
        Should fail when:
            The new mode isn't one of the node's legal modes
        Output:
            1 for success, 0 for failure
        """

        print("Applying mode " + str(new_mode) + " to " + self.type + " node")

        # Use mode validation function to check - will be overridden in child classes
        if not self.valid_mode(new_mode):
            print("Failed applying mode")
            return 0

        self.mode = new_mode
        print("Succeeded applying mode")
        return 1

    def set_params(self, new_params: list):
        """
        Input:
            An object representing new parameters
        Expected behavior:
            Checks if the parameters are valid
            If they are, sets them as the new node parameters
        Should fail when:
            Parameters are invalid
        Output:
            1 for success, 0 for failure
        """

        print("Applying parameters " + str(new_params) + " to " + self.type + " node")

        # Deep copy the parameters to avoid these situations:
        #       The node's parameter list being modified via an external reference
        #       The external list being modified from inside the node class
        new_params = copy.deepcopy(new_params)

        # Use the validation method to obtain validated parameters - will be overridden in child classes
        validated_params = self.valid_params(new_params)

        # Check if validation succeeded or not
        if validated_params == 0:
            print("Failed applying parameters")
            return 0

        self.parameters = validated_params
        print("Succeeded applying parameters " + str(self.parameters))
        return 1

    def is_leaf(self):
        """
        Output:
            1 if the node is a leaf node, 0 otherwise
        """

        # Leaf nodes have no children, so their lists are empty
        return not self.children

    def can_parent(self, target):
        """
        Input:
            A child to test for compatibility with self
        Should fail when:
            Child is None
            Child isn't a Node
            Child is the parent
            Child type is incompatible
            Child's parent is not None, so it already has a parent
        Output:
            1 if the node can parent the given child, 0 otherwise
        """

        # print("Checking if " + str(self) + " can parent " + str(target))

        # Cannot be None
        if target is None:
            print("Error: Target is None")
            return 0

        # This actually covers the previous case, but I think it's helpful
        #       to be more specific in the case of None
        if not isinstance(target, OperationNode):
            print("Error: Target is not an OperationNode")
            return 0

        # Cannot parent itself - prevent basic cycles
        if target is self:
            print("Error: A node cannot parent itself")
            return 0

        # Cannot parent an incompatible type
        if target.type not in self.child_types:
            print("Error: A " + self.type + " cannot parent " + target.type)
            return 0

        # A node cannot have two parents. The only nodes that have None as their parent are:
        #       Newly-instantiated nodes
        #       Nodes that have been removed from their previous parent
        if target.parent is not None:
            print("Error: Target is already a child")
            return 0

        return 1

    def valid_mode(self, try_mode: str or None):
        """
        Input:
            A string or None representing a mode to validate for the given node
        Should fail when:
            The passed object isn't the right type
            The mode isn't a mode defined for the given node type (implemented in subclasses' overridden methods)
        Output:
            1 if the node can be set to the mode, 0 otherwise
        """

        # This method is overridden in child classes
        print("Error: " + self.type + " node cannot be set to any mode")
        return 0

    def valid_params(self, try_params: list):
        """
        Input:
            A list of parameters to validate for the given node
        Should fail when:
            The passed object isn't the right type
            The parameters aren't valid for the given node type (implemented in subclasses' overridden methods)
        Output:
            Validated parameters, 0 if they are invalid
        """

        # This method is overridden in child classes
        print("Error: " + self.type + " node cannot accept any parameters")
        return 0

    def copy(self, tree_flag=None):
        """
        Input:
            The flag "tree" or nothing
        Expected behavior:
            Creates a semi-deep copy of the node
            Objects representing parameters are deep copied
            If tree flag is not set or is invalid: (default behavior)
                The children list is not copied - a new, empty list is assigned
            If tree flag is set:
                The children list is semi-deep copied - the output is effectively a copy of the subtree rooted at self
        Output:
            A shallow or subtree copy of the node
        """

        # print("Copying " + str(self) + " at " + str(id(self)))

        correct_flag = "tree"

        # Recursive function for deep copying children without duplicating static members such as:
        #       The class-level list of compatible node types
        def recursive_children_copy(current_parent, new_parent):

            # Start with an empty set of children
            copied_children = []

            for child in current_parent.children:

                # Make a shallow copy of the child
                new_child = copy.copy(child)

                # Deep copy the parameters and mode
                new_child.parameters = copy.deepcopy(child.parameters)
                new_child.mode = copy.deepcopy(child.mode)

                # Assign the parent as the external node that the children list will ultimately be assigned to
                new_child.parent = new_parent

                # Get the child's children recursively
                new_child.children = recursive_children_copy(child, new_child)

                # Add the new child to the list
                copied_children.append(new_child)

            return copied_children

        # Perform an initial shallow copy to avoid an expensive function call for large subtrees if possible
        node_copy = copy.copy(self)

        # Check the flag
        if tree_flag == correct_flag:
            print("- Subtree copy mode")
            node_copy.children = recursive_children_copy(self, node_copy)
        else:
            # Generate a warning if they entered something incorrect for the tree flag
            if tree_flag is not None:
                print("Warning: Subtree copy flag is \"" + correct_flag +
                      "\". Shallow copied by default.")
            # Replace the reference to the original children with an empty list
            node_copy.children = []

        # Replace the reference to the original parent with None
        node_copy.parent = None

        # Replace other references with deep copies
        node_copy.parameters = copy.deepcopy(self.parameters)
        node_copy.mode = copy.deepcopy(self.mode)

        # The list child_types is static to each class and may remain shallow-copied as a reference

        # print("Copied to " + str(node_copy) + " at " + str(id(node_copy)))

        return node_copy

    def __str__(self):
        """
        Output:
            A string summarizing the node's type, number of children, and mode
        """

        # Get the number of children and generate the return string
        child_num_str = str(len(self.children))
        return "(T: " + self.type + ", C: " + child_num_str + ", M: " + str(self.mode) + ")"

# ------------------------------------/ CHILD CLASSES /------------------------------------


class InputNode (OperationNode):
    """
    An InputNode processes input files and returns structured data that other nodes can use.
    """

    # Default attributes
    # The node type; immutable
    node_type = input_type
    # By default the node accepts files in the project directory with the following format:
    #       No header rows
    #       First column is timestamps
    #       Second column is magnitudes
    default_params = ["", 0, 0, 1]
    # However, the default mode is None, so an empty DFD object is returned
    default_mode = None
    # The input node can parent any type except itself and ModNode; immutable
    default_child_types = [prep_type, vis_type]

    def execute(self, node_input):
        """
        Input:
            A list of filename strings
        Expected behavior:
            Acts as a wrapper function for the Input module function
        Output:
            Returns the output of the called function
        """

        print("Executing " + self.type + " node in " + ("null data" if self.mode is None else self.mode) + " mode")

        # Break the parameters out of the list
        p0 = self.parameters[0]
        p1 = self.parameters[1]
        p2 = self.parameters[2]
        p3 = self.parameters[3]

        # Node_input is a list of filenames passed from the Tree's execute function
        # Return the data - even if it's an error.
        # Call the input function with the node input, mode, and parameters
        return inp.input_func(node_input, self.mode, p0, p1, p2, p3)

    def valid_mode(self, try_mode: str or None):
        """
        Input:
            A string or None
        Should fail when:
            The passed object isn't the right type
            The mode isn't in the Input module's list of predefined modes
        Output:
            1 if the mode is valid, 0 otherwise
        """

        # None mode is acceptable, otherwise it must be a string
        if try_mode is None:
            return 1
        elif not isinstance(try_mode, str):
            print("Error: Mode must be a str or None")
            return 0

        # Check if the passed string is one of the defined input modes
        if try_mode in inp.modes:
            return 1

        # If not, display legal modes
        print("Error: Invalid mode. Here are the " + self.type + " modes:")
        for mode in inp.modes:
            print(mode)
        return 0

    def valid_params(self, try_params: list):
        """
        Input:
            A list of parameters to validate
        Should fail when:
            The passed object isn't a list
            The passed list isn't at least length 4
            The single list elements, in order, aren't a string and three non-negative ints
        Output:
            Validated parameters or 0 if parameters are invalid
        """

        # Must be a list
        if not isinstance(try_params, list):
            print("Error: Parameter must be a list")
            return 0

        # Get list lengths to compare
        try_len = len(try_params)
        correct_len = len(InputNode.default_params)

        # Too few parameters
        if try_len < correct_len:
            print("Error: Too few parameters in list")
            return 0

        # Generate a warning; truncate extra parameters
        if try_len > correct_len:
            print("Warning: These extra parameters will be truncated: " + str(try_params[correct_len:]))
            try_params = try_params[:correct_len]

        # Type checking for the parameters with their respective required types and ranges
        if not isinstance(try_params[0], str):
            print("Error: Parameter 0 must be a string")
            return 0
        if not isinstance(try_params[1], int) or try_params[1] < 0:
            print("Error: Parameter 1 must be a non-negative integer")
            return 0
        if not isinstance(try_params[2], int) or try_params[2] < 0:
            print("Error: Parameter 2 must be an non-negative integer")
            return 0
        if not isinstance(try_params[3], int) or try_params[3] < 0:
            print("Error: Parameter 3 must be a non-negative integer")
            return 0

        # Another caveat is that the last two integers cannot equal each other
        if try_params[2] == try_params[3]:
            print("Error: Argument 4 and 5 must be distinct")
            return 0

        return try_params


class PrepNode(OperationNode):
    """
    A PrepNode processes time series data in various ways.
    """

    # Default attributes
    # Node type; immutable
    node_type = prep_type
    # The default parameters are all None
    # This may trigger validation warnings from the Preprocess module depending which function is chosen.
    #       Some don't require these arguments or set their own default values, executing without issue.
    #       In any event, an attempt to execute with these parameters should be gracefully handled in the
    #       Preprocess module.
    default_params = [None, None, None, None, None, None, None]
    # The default mode is None and returns the passed input unchanged
    default_mode = None
    # The PrepNode can parent any type except InputNode; immutable
    default_child_types = [prep_type, mod_type, vis_type]

    def execute(self, node_input):
        """
        Input:
            A dataframes-dict-formatted dictionary
        Expected behavior:
            Acts as a wrapper function for the Prep module prime function
        Output:
            Returns the output of the called function
        """

        print("Executing " + self.type + " node in " + ("no-op" if self.mode is None else str(self.mode)) + " mode")

        # If no function is specified, take no action
        if self.mode is None:
            return node_input

        # Breaking the parameters out of the parameter list
        # Due to parameter validation, we are guaranteed the necessary number in self.parameters
        p0 = self.parameters[0]
        p1 = self.parameters[1]
        p2 = self.parameters[2]
        p3 = self.parameters[3]
        p4 = self.parameters[4]
        p5 = self.parameters[5]
        p6 = self.parameters[6]

        # Make the call to the preprocess function with the input, mode, and parameters
        return prep.preprocess(node_input, self.mode, p0, p1, p2, p3, p4, p5, p6)

    def valid_mode(self, try_mode: str or None):
        """
        Input:
            A string or None
        Should fail when:
            The passed object isn't the right type
            The mode isn't in the Preprocess module's list of predefined modes
        Output:
            1 if the mode is valid, 0 otherwise
        """

        # None mode is acceptable, otherwise it must be a string
        if try_mode is None:
            return 1
        elif not isinstance(try_mode, str):
            print("Error: Mode must be a str or None")
            return 0

        # Check if the mode is in the list of legal Preprocess modes
        if try_mode in prep.modes:
            return 1

        print("Error: Invalid mode. Here are the " + self.type + " modes:")
        for mode in prep.modes:
            print(mode)
        return 0

    def valid_params(self, try_params: list):
        """
        Input:
            A list of parameters to validate
        Should fail when:
            The passed object isn't a list
            The passed list isn't length 1
            The single list element isn't a string
        Output:
            Validated parameters or 0 if parameters are invalid
        """

        # Must be a list
        if not isinstance(try_params, list):
            print("Error: Parameter must be a list")
            return 0

        # Get list lengths to compare
        try_len = len(try_params)
        correct_len = len(PrepNode.default_params)

        # Validate the parameters after correcting the list length, if necessary
        # If there are too few, Nones can be appended
        if try_len < correct_len:
            for i in range(0, correct_len - try_len):
                try_params.append(None)
        # If too many, generate a warning and truncate
        if try_len > correct_len:
            print("Warning: These extra parameters will be truncated: " + str(try_params[correct_len:]))
            try_params = try_params[:correct_len]

        return try_params


class ModNode(OperationNode):
    """
    A ModNode builds and trains a model from, and produces forecasts for, time series data.
    """

    # Default attributes
    # Node type; immutable
    node_type = mod_type
    # The default parameters aren't likely to generate a good model
    # They simply conform to the correctness requirements:
    #       First, second, and fourth parameters are positive integers
    #       Third parameter is a nonempty list of positive integers
    default_params = [1, 1, [1], 1]
    # ModNode takes only a None mode or an "active" mode that determines whether it will process data or not
    default_mode = None
    active_mode = "active"
    # The ModNode can only parent VisNodes; immutable
    default_child_types = [vis_type]

    def __init__(self, node_mode=None, node_params=None):
        """
        Expected behavior:
            Instantiates a ModNode
        """

        # Call default constructor:
        OperationNode.__init__(self, node_mode, node_params)

    def execute(self, node_input):
        """
        Input:
            A dataframes-dict-formatted dictionary
        Expected behavior:
            Acts as a wrapper function for the Mod module prime function
        Output:
            Returns the output of the called function
        """

        print("Executing " + self.type + " node in " + ("no-op" if self.mode is None else self.mode) + " mode")

        # If mode is None, return the input
        if self.mode is None:
            return node_input

        # Breaking the parameters out of the parameter list
        # Due to set_params and __init__ we are guaranteed the correct parameter format
        int_p0 = self.parameters[0]
        int_p1 = self.parameters[1]
        list_p = self.parameters[2]
        int_p2 = self.parameters[3]

        return mod.mod_fore(node_input, int_p0, int_p1, list_p, int_p2)

    def valid_mode(self, try_mode: str or None):
        """
        Input:
            A string or None
        Should fail when:
            The passed object isn't the right type
        Output:
            1 if the mode is valid, 0 otherwise
        """

        # Only two modes are permissible; check if it is either of those
        if try_mode is None or try_mode == ModNode.active_mode:
            return 1

        print("Error: Mode must be " + ModNode.active_mode + " or None")
        return 0

    def valid_params(self, try_params: list):
        """
        Input:
            A list of parameters to validate
        Should fail when:
            The passed object isn't a list
            The passed list isn't the proper length
            The list elements aren't, in order: two ints, a nonempty list of only ints, and another int
        Output:
            Validated parameters or 0 if parameters are invalid
        """

        # Must be a list
        if not isinstance(try_params, list):
            print("Error: Parameter must be a list")
            return 0

        # Get the list lengths for comparison
        try_len = len(try_params)
        correct_len = len(ModNode.default_params)

        # Validate the parameters after correcting the list length, if necessary
        if try_len < correct_len:
            print("Error: Too few parameters in list")
            return 0
        # Generate a warning and truncate
        if try_len > correct_len:
            print("Warning: These extra parameters will be truncated: " + str(try_params[correct_len:]))
            try_params = try_params[:correct_len]

        # Type checking for the parameters and their respective required types
        if not isinstance(try_params[0], int) or try_params[0] < 1:
            print("Error: Parameter 0 must be a positive int")
            return 0
        if not isinstance(try_params[1], int) or try_params[1] < 1:
            print("Error: Parameter 1 must be a positive int")
            return 0
        if not isinstance(try_params[2], list):
            print("Error: Parameter 2 must be a list")
            return 0

        # Checking if the list is nonempty
        if not try_params[2]:
            print("Error: Parameter 2 must be nonempty")
            return 0
        # Checking if the list contains only positive integers
        for item in try_params[2]:
            if not isinstance(item, int) or item < 1:
                print("Error: Parameter 2 must contain positive ints only")
                return 0

        # Type-checking the final parameter
        if not isinstance(try_params[3], int) or try_params[3] < 1:
            print("Error: Parameter 3 must be a positive int")
            return 0

        return try_params


class VisNode(OperationNode):
    """
    A VisNode visualizes and produces statistics for time series data.
    """

    # Default attributes
    # Node type; immutable
    node_type = vis_type
    # Default parameters will be handled gracefully in the visualization module
    default_params = ["", "", ""]
    # Default mode returns the input without taking action
    default_mode = None
    # The VisNode can only parent other VisNodes; immutable
    default_child_types = [vis_type]

    def execute(self, node_input):
        """
        Input:
            A dataframes-dict-formatted dictionary
        Expected behavior:
            Acts as a wrapper function for the Vis module prime function
        Output:
            Returns the output of the called function
        """

        print("Executing " + self.type + " node in " + ("no-op" if self.mode is None else self.mode) + " mode")

        # Take no action if mode is None
        if self.mode is None:
            return node_input

        # Breaking the parameters out of the parameter list
        # Due to set_params and __init__ we are guaranteed the necessary number in self.parameters
        p0 = self.parameters[0]
        p1 = self.parameters[1]
        p2 = self.parameters[2]

        return vis.visualize(node_input, self.mode, p0, p1, p2)

    def valid_mode(self, try_mode: str or None):
        """
        Input:
            A string or None
        Should fail when:
            The passed object isn't the right type
            The mode isn't in the Visualization module's list of predefined modes
        Output:
            1 if the mode is valid, 0 otherwise
        """

        # None mode is ok, otherwise is must be a string
        if try_mode is None:
            return 1
        elif not isinstance(try_mode, str):
            print("Error: Mode must be a str or None")
            return 0

        # Check if the given string is in the Visualization module's list of legal modes
        if try_mode in vis.modes:
            return 1

        print("Error: Invalid mode. Here are the " + self.type + " modes:")
        for mode in vis.modes:
            print(mode)
        return 0

    def valid_params(self, try_params: list):
        """
        Input:
            A list of parameters to validate
        Should fail when:
            The passed object isn't a list
            The passed list isn't the proper length
            The list elements aren't all strings
        Output:
            Validated parameters or 0 if parameters are invalid
        """

        # Must be a list
        if not isinstance(try_params, list):
            print("Error: Parameter must be a list")
            return 0

        # Get list lengths for comparison
        try_len = len(try_params)
        correct_len = len(VisNode.default_params)

        # Validate the parameters after correcting the list length, if necessary
        if try_len < correct_len:
            print("Error: Too few parameters in list")
            return 0

        # Generate a warning and truncate
        if try_len > correct_len:
            print("Warning: These extra parameters will be truncated: " + str(try_params[correct_len:]))
            try_params = try_params[:correct_len]

        # Make sure every item in the list is a string
        for item in try_params:
            if not isinstance(item, str):
                print("Error: All parameters must be strings")
                return 0

        return try_params


# Light testing for the node classes focusing on validation of mode and parameters
def main():

    bad_inputs = [-1, 0, 1, 5, True, False, "Cheesecake", "", [1, 2, 3], [], None]

    op1 = OperationNode()
    inp1 = InputNode()
    prep1 = PrepNode()
    mod1 = ModNode()
    vis1 = VisNode()

    nodes = [op1, inp1, prep1, mod1, vis1]

    # Testing valid mode sets
    print("\nTESTING INPUT MODES\n")
    for mode in inp.modes:
        inp1.set_mode(mode)
        print("")

    print("\nTESTING PREPROCESS MODES\n")
    for mode in prep.modes:
        prep1.set_mode(mode)
        print("")

    print("\nTESTING MODELING-FORECASTING MODES\n")
    mod1.set_mode(None)
    print("\n")
    mod1.set_mode("active")
    print("")

    print("\nTESTING VISUALIZATION MODES\n")
    for mode in vis.modes:
        vis1.set_mode(mode)
        print("")

    # Testing input params
    print("\nTESTING INPUT PARAMETERS\n")
    inp1.set_params(["race car"])
    print("")
    inp1.set_params(["race car", "second argument", 3, "frogs"])
    print("")
    inp1.set_params(["test", 7, 9, 10])
    print("")
    inp1.set_params([])
    print("")
    inp1.set_params(["test", 7, -9, 10])
    print("")
    inp1.set_params([5, "valid parameter"])
    print("")
    inp1.set_params(["test", 7, 9, "bagel", 56])
    print("")
    inp1.set_params([5])
    print("")
    inp1.view_settings()
    print("")

    # Testing prep params
    print("\nTESTING PREPROCESS PARAMETERS\n")
    prep1.set_params([1, 2, 3, 4, 5, 6, 7])
    print("")
    prep1.set_params([1, 2])
    print("")
    prep1.set_params([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
    print("")
    prep1.set_params([])
    print("")
    prep1.view_settings()
    print("")

    # Testing mod params
    print("\nTESTING MODELING-FORECASTING PARAMETERS\n")
    mod1.set_params([1, 2, [1, 2, 3, 4, 5, 6, 7, 8, 9, 9], 10])
    print("")
    mod1.set_params([1, 2, [1, 2, 3, 4, 5, 6, 7, 8.5, 9, 9], 10])
    print("")
    mod1.set_params(["", 5, 6, ""])
    print("")
    mod1.set_params([1, 2, [1, 2, 3, 4, 5, 6, 7, 8, 9, 9], "a"])
    print("")
    mod1.view_settings()
    print("")

    # Testing vis params
    print("\nTESTING VISUALIZATION-STATISTICS PARAMETERS\n")
    vis1.set_params([1, 2, 3, 4, 5, 6, 7])
    print("")
    vis1.set_params(["", ""])
    print("")
    vis1.set_params(["", "", "", 1])
    print("")
    vis1.set_params(["", 5, 6, ""])
    print("")
    vis1.set_params(["", "", "hello", 5])
    print("")
    vis1.view_settings()
    print("")

    print("\nTESTING ALL BAD MODES AND PARAMETERS\n")
    for node in nodes:
        for item in bad_inputs:
            node.set_mode(item)
            print("")
        for item in bad_inputs:
            node.set_params(item)
            print("")
        node.view_settings()
        print("")

    vis1.can_parent(None)

    vis1.can_parent(vis1)

    vis_copy = vis1.copy("fake_tree_flag")

    print(vis_copy)


if __name__ == "__main__":
    main()
