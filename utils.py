"""
* Title:            utils.py
* Project:          Transformation Tree Library
* Description:      Global variables, names, and values used throughout the project.
*
* Team:             The Hackson Five
*
* Last Modified by: Aaron Van Cleave and Sarah Hartley
* Date Modified:    09 Feb 2021
"""

# Pandas and math are used in the default data frame dictionary and helper methods below
import pandas as pd
import math
import input

# --------------------------------/ DATA FRAMES DICTIONARY (DFD) OBJECT STANDARD /--------------------------------

"""
For time series storage, we have a time series (TS) object of this form:

{
data_frames_dict: {0: DF0, ..., n: DFn},
min_max: {0: (min0, max0), ..., n: (minn, maxn)},
forecasts: {0: fDF0, ..., n: fDFn}
split_data: {0: {training: DF0_Train, valid: DF0_Valid, test: DF0_Test},
             ...
             n: {training: DFn_Train, valid: DFn_Valid, test: DFn_Test}}
complete: True/False,
stat_results: {0: {mse: mse_of_DF0, mape: mape_of_DF0, smape: smape_of_DF0},
               ...
               n: {mse: mse_of_DFn, mape: mape_of_DFn, smape: smape_of_DFn}}
}

Where:
    DF0, ..., DFn are Pandas Dataframes
    (min0, max0), ..., (minn, maxn) are tuples of the min and max of the previous time series
    fDF0, ..., fDF0 are Pandas Dataframes of forecasted time series
    DF0_Train, DF0_Valid, DF0_Test ... DFn_Train, DFn_Valid, DFn_Test are Dataframes split from the original time series

min_max and forecasts are optional, and their presence (or lack thereof) in the dict serves as a flag for modules.
Examples of checking properties of a DF dict:
    If min_max is not present, the DFs in data_frames_dict are not normalized.
    If forecasts is not present, the DFs in data_frames_dict have not been forecasted.
    If complete is False, one or more DFs in data_frames_dict contain NaN elements or timestamps.

The elements in data_frames_dict, min_max, forecasts, split_data, and stat_results are
intended to correspond with one another.
A dataframe, min_max tuple, forecast, split data, and stat results for a single time
series should be indexed with the same integer so that when iterating through a TS object,
they can all be accessed with a single index.
This facilitates a few useful and important features:
    The original values of a time series can be calculated from its normalized dataframe and the min_max tuple.
    A forecast can easily be stored in the TS object that its original time series data came from.
    A forecast can easily be plotted next to its original time series.
"""

# The key of the dictionary of dataframes in the TS object
data_frames_dict = "data_frames_dict"

# The key for the min and max values
min_max = "min_max"

# The key for the forecast dict
forecasts = "forecast"

# The key for the split data dict
split_dfs = "split_dfs"

# The key of the completeness bool in the TS object
complete = "complete"

# The key for the stat results dict
stat_results = "stat_results"

"""
In the same way as a DF dict, the presence or lack of these elements
in the DF will be used to determine whether certain functions can process it.
"""
# For individual dataframes, these keys are optional
magnitude = "Magnitude"
timestamp = "TimeStamp"

# For split data:
training = "training"
valid = "valid"
test = "test"

# A default empty dataframe dictionary
default_empty_DFD = {data_frames_dict: {"0": pd.DataFrame({magnitude: [], timestamp: []})}, complete: False}

# Some more examples of DFD format
"""
bad_magnitude = [1, 2, None, 4, 5, 253.22, 7, 8, None, 10, 5, 4, None, -3, 15, 100000, 17, 18, 19, 5]
temp_magnitude = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
small_magnitude = [-0.11034, -5, -5, -1, -0.25, -1.3, -7, -0.00005, 0.0001, -4]

temp_date1 = pandas.to_datetime(["2010-06-01 18:30:00", "2010-06-01 19:30:00", "2010-06-01 20:30:00",
                                 "2010-06-01 21:30:00", "2010-06-01 22:30:00", "2010-06-01 23:30:00",
                                 "2010-06-02 00:30:00", "2010-06-02 01:30:00", "2010-06-02 02:30:00",
                                 "2010-06-02 03:30:00", "2010-06-02 04:30:00", "2010-06-02 05:30:00",
                                 "2010-06-02 06:30:00", "2010-06-02 07:30:00", "2010-06-02 08:30:00",
                                 "2010-06-02 09:30:00", "2010-06-02 10:30:00", "2010-06-02 11:30:00",
                                 "2010-06-02 12:30:00", "2010-06-02 13:30:00"])

temp_date2 = pandas.to_datetime(["2010-06-01 18:30:00", "2010-06-01 19:30:00", "2010-06-01 20:30:00",
                                 "2010-06-01 21:30:00", "2010-06-01 22:30:00", "2010-06-01 23:30:00",
                                 "2010-06-02 00:30:00", "2010-06-02 01:30:00", "2010-06-02 02:30:00",
                                 "2010-06-02 03:30:00", "2010-06-02 04:30:00", "2010-06-02 05:30:00",
                                 "2010-06-02 06:30:00", "2010-06-02 07:30:00", "2010-06-02 08:30:00",
                                 "2010-06-02 09:30:00", "2010-06-02 10:30:00", "2010-06-02 11:30:00",
                                 "2010-06-02 12:30:00", "2010-06-02 13:30:00"])

temp_date3 = pandas.to_datetime(["2013-06-01 18:30:00", "2013-06-01 19:30:00", "2013-06-01 20:30:00",
                                 "2013-06-01 21:30:00", "2013-06-01 22:30:00", "2013-06-01 23:30:00",
                                 "2013-06-02 00:30:00", "2013-06-02 01:30:00", "2013-06-02 02:30:00",
                                 "2013-06-02 03:30:00"])


# As specified in utils.py, but repeated here - the output type needs to be:

# {data_frame_dict: {# Data frames in here}, complete: False}

# Small data frames to work with
df_0 = pandas.DataFrame({utils.magnitude: bad_magnitude, utils.timestamp: temp_date1})
df_1 = pandas.DataFrame({utils.magnitude: temp_magnitude, utils.timestamp: temp_date2})
df_2 = pandas.DataFrame({utils.magnitude: small_magnitude, utils.timestamp: temp_date3})

# We'll pretend these are forecast dataframes
fdf_0 = pandas.DataFrame({utils.magnitude: [], utils.timestamp: []})
fdf_1 = pandas.DataFrame({utils.magnitude: [], utils.timestamp: []})
fdf_2 = pandas.DataFrame({utils.magnitude: [], utils.timestamp: []})

simple_example_TS = {utils.data_frames_dict: {"0": df_0, "1": df_1, "2": df_2}, utils.complete: False}

complex_example_TS = {utils.data_frames_dict: {"0": df_0, "1": df_1, "2": df_2},
                      utils.min_max: {"0": (0, 5), "1": (-1, 43), "2": (-550, -17)},
                      utils.forecasts: {"0": fdf_0, "1": fdf_1, "2": fdf_2},
                      utils.complete: True}
"""

# ------------------------------------/ HELPER METHODS /------------------------------------


def split_data(pandas_data_frame_dict, perc_training=60, perc_valid=20, perc_test=20):
    """
    Adds the 'split_data' element to the data_frames_dict:
    each data frame has the same key, IE '0', '1' '2' etc, and each of these
    has a dictionary containing 'training', 'valid', and 'test' as the key for each element.
    -Default values: training 60% , validation 20%, testing 20%
    -The percentages must be ints or floats, and the sum must be <= 100.
    -The data frame must have at least length 10 or it will return an empty dataframe.
    """

    # Type checking: All percent values must be ints or floats
    if not ((isinstance(perc_training, int) or isinstance(perc_training, float))
            and (isinstance(perc_valid, int) or isinstance(perc_valid, float))
            and (isinstance(perc_test, int) or isinstance(perc_test, float))):
        print("Error: Percent parameters given to split_data() must be integers or floats")
        return 0
    if ((perc_training < 0) or (perc_valid < 0) or (perc_test < 0)):
        print("Error: Percent parameters given to split_data must non-negative")
        return 0
    # Cannot have more than 100% of anything
    if (perc_training + perc_valid + perc_test) > 100:
        print("Error: Sum of percentages given to split_data() must not exceed 100")
        return 0

    # Add the split_data dictionary to the universal object being passed around
    pandas_data_frame_dict[split_dfs] = dict()

    # create a reference to the data frames dictionary
    df_dict = pandas_data_frame_dict[data_frames_dict]

    # iterate through each data frame in the data frame dictionary
    for element in df_dict:
        # call the helper function each data frame and save the result
        results = split_data_helper(df_dict[element], perc_training, perc_valid, perc_test)

        # save the result as a dictionary
        results_dict = {training: results[0], valid: results[1], test: results[2]}

        # add the result to the universal dictionary object being passed around
        pandas_data_frame_dict[split_dfs][element] = results_dict

    # return the universal dictionary object
    return pandas_data_frame_dict


def split_data_helper(data_frame, perc_training, perc_valid, perc_test):
    """
    Called by spit_data.  Accepts a single pandas dataframe, and
    three floats or ints.  Returns a list of pandas data frames.
    Returns an empty data frame if there are less than 10 items.
    """

    # checks whether the values sum to 100  (used later)
    flag_100_percent = (sum([perc_training, perc_valid, perc_test]) == 100)

    # getting number of rows in the data frame
    len_data_frame = len(data_frame)

    # I arbitrarily required there to be 10 items.
    if len_data_frame < 10:
        if timestamp in data_frame:
            # Don't want to add columns to the dataframe so I check whether TimeStamp is an element
            data = {'TimeStamp': [], 'Magnitude': []}
        else:
            # I know that magnitude is always an element.
            data = {'Magnitude': []}

        pandas_data_frame = pd.DataFrame(data)  # Create DataFrame object

        # return a list of 3 empty data frames
        return [pandas_data_frame, pandas_data_frame, pandas_data_frame]

    # get percentages as a decimal between 0 and 1
    perc_training /= 100

    # calculate the length  of the train series and slice
    train_end = math.floor(len_data_frame * perc_training)
    train_data_frame = data_frame[0:train_end]

    # reset the indices of the matrix
    train_data_frame.reset_index(inplace=True, drop=True)

    # get percentages as a decimal between 0 and 1
    perc_valid /= 100

    # calculate the length of the validation series and slices
    # from the end of the train series to the end of the validation series
    valid_end = math.floor(len_data_frame * perc_valid)
    valid_data_frame = data_frame[train_end:train_end + valid_end]

    # reset the indices of the matrix
    valid_data_frame.reset_index(inplace=True, drop=True)

    # Get percentage as a decimal between 0 and 1
    perc_test /= 100

    # Calculate the length of the end series
    test_end = math.floor(len_data_frame * perc_test)

    # depending on whether the flag from the beginning of the function is true or not,
    # either slice to the end of the series or stop where the end series finishes.
    if flag_100_percent:
        test_data_frame = data_frame[train_end + valid_end:]
    else:
        test_data_frame = data_frame[train_end + valid_end:train_end + valid_end + test_end]

    # reset the indices of the matrix
    test_data_frame.reset_index(inplace=True, drop=True)

    # Return the data frames in a list
    return [train_data_frame, valid_data_frame, test_data_frame]


def is_complete_dictionary(pandas_data_frame_dict):
    """
    Checks an entire dataframe dictionary for null values.
        Returns 0 if data_frames_dict missing from passed object.
        Returns True if ALL 'Magnitude' and 'TimeStamp' series in all dataframes do not contain null values.
        Returns True even if 'Magnitude and 'TimeStamp' series are absent, because they do not contain nulls
    """

    # Type checking the argument is a dictionary
    if not isinstance(pandas_data_frame_dict, dict):
        print("Error: is_complete_dictionary() takes a dictionary of dataframes as its argument."
              + " You provided an object of type <" + str(type(pandas_data_frame_dict)) + ">")
        return 0

    # making sure the dictionary contains the required elements
    if data_frames_dict not in pandas_data_frame_dict:
        print(data_frames_dict + " is required element of dictionary passed to is_complete()")
        return 0

    # create reference to the data frames dictionary
    df_dict = pandas_data_frame_dict[data_frames_dict]

    # default to saying the dictionary is complete
    complete_flag = True

    # iterate through every data frame in the dictionary
    for element in df_dict:
        # call helper function on the dataframe (returns bool)
        comp = is_complete_dataframe(df_dict[element])

        # if an incomplete (contains null) dataframe is encountered, completeness is false.
        if not comp:
            complete_flag = False
            break

    # Add the complete_flag element to the universal object
    pandas_data_frame_dict[complete] = complete_flag

    # return boolean
    return complete_flag


def is_complete_dataframe(data_frame):
    """
    Checks if a single dataframe contains null values.
            (Helper function for is_complete but works fine on its own)
        Takes a single data frame as argument and returns bool value:
        True if no null values are found, else False
    """

    # create a dummy pandas dataframe for type checking
    dataframe_type_object = pd.DataFrame({})
    if type(data_frame) != type(dataframe_type_object):
        print("Error: is_complete_dataframe() takes a single Pandas Dataframe object as its argument."
              + " You provided an object of type <" + str(type(data_frame)) + ">")
        return 0

    # default to completeness is true
    is_complete = True

    # if 'TimeStamp' is a column in the dataframe, check it for nulls.
    if timestamp in data_frame:
        # get the length of the series
        length_series = len(data_frame[timestamp])
        # convert it to a list
        time_series_list = list(data_frame[timestamp])
        for i in range(length_series):
            # if any values are NaT or None, completeness is false & break
            if time_series_list[i] is None or pd.isnull(time_series_list[i]):
                is_complete = False
                break
    # if 'Magnitude' is a column and the dataframe is complete so far
    if magnitude in data_frame and is_complete:
        # get the length of the series
        length_series = len(data_frame[magnitude])
        # convert it to a list
        magnitude_series_list = list(data_frame[magnitude])
        for i in range(length_series):
            # if any values are None or NaN, completeness is false & break
            if magnitude_series_list[i] is None or \
                    math.isnan(magnitude_series_list[i]):
                is_complete = False
                break
    # return completeness (bool)
    return is_complete


def check_output(output_path: str):
    """
    Input:
        A string that is a path to a CSV file
    Expected behavior:
        Checks a string to see if string has a "csv" extension
    Output:
        True on success, False on failure
    """
    # checks the output string for a csv extension. if found, returns True
    if output_path.endswith('.csv'):
        return True
    # if there is no csv extension, returns False
    else:
        print('Error:Output path must be a csv ')
        return False


def check_matrix_input(input_index, output_index):
    """
    Input:
        A list of input indices and a list of output indices
    Expected behavior:
        Checks the input of the function to make sure it is a valid input for
        design matrix
    Output:
        True on success, False on failure
    """
    # checks to see is the input indices and output indices are in a list
    if not isinstance(input_index, list) or not isinstance(output_index, list):
        print('Error: Design Matrix Input Index and Output Index must be a list ')
        return False
    # checks to see if all input indices are less than or equal to 0
    for index in input_index:
        if index > 0:
            print('Error: input index cannot contain positive values')
            return False
    # checks to see if all output indices are are greater than 0
    for index in output_index:
        # if the index is less than or equal to 0
        if index <= 0:
            print('Error: output_index cannot contain negative values')
            return False
    # returns True if invalid arguments were not found
    return True


def design_matrix(ts, input_index, output_index, a=None, b=None):
    """
    Input:
        A time series data frame, an input list, an output list
        OR
        A time series data frame, and an mi, ti, mo, and to integer
    Expected behavior:
        Checks for optional parameters a and b. If found, checks to ensure input fits mi, ti, mo, to format.
        Calls the helper function to change the format to input index/output index format.
        Then takes input index/output index format and creates a design matrix
    Output:
        Returns a dataframe in the form of a design matrix
    """
    # checks for optional parameters a and b, if found passes to helper function
    if a and b:
        df = design_matrix_helper(ts, input_index, output_index, a, b)
        return df
    # checks for presence of exclusively a or b
    elif (a and not b) or (b and not a):
        print('Error: Invalid number of arguments to design matrix')
        return 0
    # else, has valid parameters for input index and output index
    else:
        if not check_matrix_input(input_index, output_index):
            return 0
        # makes a reference to the time series magnitude list
        mag_list = ts['Magnitude']
        matrix = []
        # calculates the lowest value in the input index
        lowest = (min(input_index))
        # calculates the highest value in the output index
        highest = (max(output_index))
        # sets the t value to the absolute value of the lowest value
        t = abs(lowest)
        # concatenates the two index lists together
        index_list = input_index + output_index
        # starts the current t value at t and goes through the list, stopping when the end is reached
        for curr_t in range(t, (len(mag_list) - highest)):
            curr_mag_list = []
            # goes through index list, finds appropriate index
            for index in index_list:
                curr_mag_list.append(mag_list[curr_t + index])
            matrix.append(curr_mag_list)
        # sorts the index list for better readability
        index_list.sort()
        # creates a data frame from the list of calues and index list
        df = pd.DataFrame(matrix, columns=index_list)
        return df


def design_matrix_helper(ts, mi, ti, mo, to):
    """
    Input:
        A time series data frame, and integers mi, ti, mo, and to that specify the spacing and occurances
        of data
    Expected behavior:
        Turns the input into a format that can then be passed back to the design matrix function
    Output:
        Passes the newly formatted data back to design matrix, returns a design matrix
    """
    # checks to make sure that the given arguments are integers. If so, returns an error message
    if not (isinstance(mi, int) and isinstance(ti, int) and isinstance(mo, int) and isinstance(to, int)):
        print('Error: mi, ti, mo, and to must be integers')
        return 0
    # initializes empty lists for the input and output indices
    input_index = []
    output_index = []
    # turns mi values into input index values and appends them to the empty input list
    for i in range(1, mi + 1):
        input_index.append((i * ti) * -1)
    # turns mo values into output index values and appends them to the empty output list
    for i in range(1, mo + 1):
        output_index.append(i * to)
    # takes the new input and output list and calls design matrix
    return design_matrix(ts, input_index, output_index)


def ts2db(input_filename, target_folder, perc_training, perc_valid, perc_test, input_index, output_index,
          output_file_name):
    """
   Input:
       A string that is a path to file name, a target folder that the file is in, a percentage of valid data,
       a percentage of training data, a percentage of test day, an list of input indices, a list of output indices,
       and an output file name that is the path to a csv file
   Expected behavior:
       Takes these parameters and calls the input, split data, and design matrix formulas creating a database.
       Saves this database to a csv file.
   Output:
       Saves a database to the file specified by 'output_file_name'
   """
    # checks to see if the output file name has a csv extension
    if not check_output(output_file_name):
        return 0
    # take in input_filename, return data_frame_dict
    df = input.input_func([input_filename], "interactive", target_folder, 0, 0, 1)
    # takes data_frame_dict, call split_data
    x = split_data(df, perc_training, perc_valid, perc_test)
    # passes the split data frame to the design matrix function
    new_df = design_matrix(x['split_dfs']['0']['training'], input_index, output_index)
    # writes the data frame to a csv file
    new_df.to_csv(output_file_name)
