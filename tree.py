"""
* Title:            tree.py
* Project:          Transformation Tree Library
* Description:      Implementation of the TFormTree class.
*
* Team:             The Hackson Five
*
* Last Modified by: Aaron Van Cleave and Kuan-Yun chao (Execution test and minor tests left)
* Date Modified:    07 Feb 2021
"""

# This facilitates the copy_path method
import copy
# Trees are composed of nodes; this is essential
import node as node
# The tree class has a method for generating pipeline objects
import pipeline as pipe


class TFormTree:
    """
    A TFormTree is manipulated by the library functions to facilitate
    data processing, modeling, forecasting, and visualization on time series.
    """

    def __init__(self, input_mode=None, input_params=None):
        """
        Expected behavior:
            Instantiates a tree object
        """

        print("Creating TFormTree")

        # Root of tree, always an input node
        self.root = node.InputNode(input_mode, input_params)
        # List of all the nodes in the tree to prevent cycles
        self.nodes = [self.root]

        print("Created TFormTree\n")

# ------------------------------------/ MODIFICATION METHODS /------------------------------------

    def add(self, mount_node: node.OperationNode or int, addition):
        """
        Input:
            Two nodes, one to become the parent of the other.
            May take an int index to the node already in the tree.
        Expected behavior:
            Validates the attempted subtree
            Adds a single node or a subtree of nodes to the given mount point
            If add succeeds, adds node references to the tree list
        Should fail when:
            __in_tree detects a redundant node
            the add operation fails due to incompatibility
            the given mount node is invalid
        Output:
            1 on success, 0 on failure
        """

        print("Adding " + str(addition) + " to tree at " + str(mount_node))

        # Validate the mount point, if valid get the specified node
        if isinstance(mount_node, int):
            mount_node = self.at_index(mount_node)
            if mount_node == 0:
                print("Add failed\n")
        elif not self.in_tree(mount_node):
            print("Error: " + str(mount_node) + " not in tree")
            print("Add failed\n")
            return 0

        # Make sure the new subtree / single node isn't already in the tree
        if self.in_tree(addition):
            print("Add failed\n")
            return 0

        # Make sure the mount and addition are compatible
        if not mount_node.can_parent(addition):
            print("Error: " + str(mount_node) + " cannot parent " + str(addition))
            print("Add failed\n")
            return 0

        # Validation passed: add the node to mount and to the tree list
        TFormTree.__node_add(mount_node, addition)
        TFormTree.__recursive_list_add(self, addition)
        print("Add succeeded\n")
        return 1

    def insert(self, parent: node.OperationNode or int, child: node.OperationNode or int, new_node):
        """
        Input:
            A node outside the tree to place between two nodes inside the tree
        Expected behavior:
            Adds the new node as a child of the parent
            Adds the old child as a child of the new node
        Should fail when:
            Either old node isn't in the tree
            The old nodes don't have a parent-child relation
            New node is in the tree
            The new node is incompatible with either old node
        Output:
            1 for success, 0 for failure
        """

        print("Inserting " + str(new_node) + " between " + str(parent) + " and " + str(child))

        # Validate the first insertion point, if valid get the specified node
        if isinstance(parent, int):
            parent = self.at_index(parent)
            if parent == 0:
                print("Insert failed\n")
        elif parent not in self.nodes:
            print("Error: " + str(parent) + " not in tree")
            print("Insert failed\n")
            return 0

        # Validate the second insertion point, if valid get the specified node
        if isinstance(child, int):
            child = self.at_index(child)
            if child == 0:
                print("Insert failed\n")
        elif child not in self.nodes:
            print("Error: " + str(child) + " not in tree")
            print("Insert failed\n")
            return 0

        # Validate the new node
        if self.in_tree(new_node):
            print("Insert failed\n")
            return 0

        # Validate the parent-child relationship
        # Is the first node the parent of the second?
        if parent is not child.parent:
            # Is the second node the parent of the first?
            # If neither is the case, we cannot insert
            if child is not parent.parent:
                print("Error: " + str(parent) + " and " + str(child) + " are not parent and child")
                print("Insert failed\n")
                return 0

            # If it's the other way around, switch the references for easier handling in the remainder of the method
            parent, child = child, parent

        # Validate this compatibility
        if not parent.can_parent(new_node):
            print("Error: " + str(parent) + " cannot parent " + str(new_node))
            print("Insert failed\n")
            return 0

        # Temporarily remove the child's parent to check the other compatibility
        child.parent = None
        if not new_node.can_parent(child):
            # If failed, set the parent back again so the tree is unchanged
            child.parent = parent
            print("Error: " + str(new_node) + " cannot parent " + str(child))
            print("Insert failed\n")
            return 0

        # All validation passed - do all the switches for children and parents

        # Add it and its children to the tree
        TFormTree.__recursive_list_add(self, new_node)

        # Unlink the parent and child,
        TFormTree.__node_remove(parent, child)

        # Relate the new node to each as appropriate
        TFormTree.__node_add(parent, new_node)
        TFormTree.__node_add(new_node, child)

        print("Insert succeeded\n")
        return 1

    def replace(self, old_node: node.OperationNode or int, new_node: node.OperationNode):
        """
        Input:
            A node in the tree to replace with another node outside the tree
        Expected behavior:
            Replaces the old node with the new one
            The old node's parent becomes the new node's parent
            The old node's children become the new node's children
        Should fail when:
            Old node isn't in the tree
            Old node is the root
            New node is in the tree
            The new node is incompatible with the old node's parent
            The new node is incompatible with any of the old node's children
        Output:
            1 for success, 0 for failure
        """

        print("Replacing " + str(old_node) + " with " + str(new_node))

        # Validate the replacement point, if valid get the specified node
        if isinstance(old_node, int):
            old_node = self.at_index(old_node)
            if old_node == 0:
                print("Replace failed\n")
        elif old_node not in self.nodes:
            print("Error: " + str(old_node) + " not in tree")
            print("Replace failed\n")
            return 0

        # Replacing the root is not permitted
        if old_node == self.root:
            print("Error: Cannot replace root")
            print("Replace failed\n")
            return 0

        # Can't replace with a node already in the tree
        if self.in_tree(new_node):
            print("Replace failed\n")
            return 0

        # Compatibility validation
        if not old_node.parent.can_parent(new_node):
            print("Error: " + str(old_node.parent) + " cannot parent " + str(new_node))
            print("Replace failed\n")
            return 0

        # Set children's parent to None to pretend they have no parent
        for child in old_node.children:
            child.parent = None
        # Is the new node compatible with them?
        for child in old_node.children:
            if not new_node.can_parent(child):
                # Set these back so the tree is unchanged
                for retained_child in old_node.children:
                    retained_child.parent = None
                print("Error: " + str(new_node) + " cannot parent " + str(child))
                print("Replace failed\n")
                return 0

        # All validation passed - do all the switches for children and parents
        switch_par = old_node.parent

        # Add the new node and remove the old one
        TFormTree.__node_add(switch_par, new_node)
        TFormTree.__node_remove(switch_par, old_node)

        # Update the tree list
        # Remove ONLY the old node, not its children
        self.nodes.remove(old_node)
        # Add the new node and all its children
        TFormTree.__recursive_list_add(self, new_node)

        # Add all the old node's children to the new node
        for child in old_node.children:
            TFormTree.__node_add(new_node, child)
        # Remove them all from the old node
        old_node.children = []

        print("Replace succeeded\n")
        return 1

    def remove(self, removal_node: node.OperationNode or int, tree_flag=None):
        """
        Input:
            A node to remove, the tree flag or nothing
        Expected behavior:
                If tree flag is invalid:
                    No action is taken
                If tree flag is not set:
                    Removal node is spliced out of the tree and its children are appended to its parent if possible
                If tree flag is set:
                    Removes the subtree rooted at the given node
                    If remove succeeds, removes references to the subtree nodes from the tree's list
        Should fail when:
            Removal node isn't found in the tree list
            Removal node is the root
            Tree flag is not set and removal node's children are incompatible with its parent
        Output:
            1 on success, 0 otherwise
        """

        correct_flag = "tree"

        print("Removing " + str(removal_node))

        # Validate the removal point, if valid get the specified node
        if isinstance(removal_node, int):
            removal_node = self.at_index(removal_node)
            if removal_node == 0:
                print("Removal failed\n")
        elif removal_node not in self.nodes:
            print("Error: " + str(removal_node) + " not in tree")
            print("Remove failed\n")
            return 0

        # Can't remove the root
        if removal_node == self.root:
            print("Error: Cannot remove root")
            print("Remove failed\n")
            return 0

        rem_par = removal_node.parent

        # Check the flag
        # Subtree removal is quick and easy, just get rid of the subtree's root and update the list
        if tree_flag == correct_flag:
            print("- Subtree removal mode")
            TFormTree.__node_remove(rem_par, removal_node)
            TFormTree.__recursive_list_remove(self, removal_node)
            print("Remove succeeded\n")
            return 1

        # Check if the user specified an incorrect flag or other incorrect input
        elif tree_flag is not None:
            # Since removal is difficult to reverse, don't perform it unless the user provides clear input
            print("Error: Subtree removal flag is \"" + correct_flag + "\"")
            print("Remove failed\n")
            return 0

        # Remove just the specified node, attaching its children to its parent if possible
        # Set all children's parent to None temporarily
        for removal_child in removal_node.children:
            removal_child.parent = None
        # Can the new node parent all the old node's children?
        for removal_child in removal_node.children:
            if not rem_par.can_parent(removal_child):
                # Set these back so the tree is unchanged
                for retained_child in removal_node.children:
                    retained_child.parent = removal_node
                print("Error: " + str(rem_par) + " cannot parent " + str(removal_child))
                print("Remove failed\n")
                return 0

        # Now that this removal is validated, swap the children
        for removal_child in removal_node.children:
            TFormTree.__node_add(rem_par, removal_child)
        # Removed node ends up with no children
        removal_node.children = []

        # Remove the node from its parent and the tree list
        TFormTree.__node_remove(rem_par, removal_node)
        self.nodes.remove(removal_node)

        print("Remove succeeded\n")
        return 1

# ------------------------------------/ EXECUTE METHODS /------------------------------------

    def execute_path(self, path_node: node.OperationNode or int, *args):
        """
        Input:
            A node in the tree, an arbitrary number of filenames
        Expected behavior:
            Executes nodes in order from the root to the given node, with each node feeding its output to the next
        Should fail when:
            No filenames are given
            No path can be generated from the given node
            The executed path fails
        Output:
            The output of the given node after its predecessors have executed, or 0 on failure
        """

        print("Executing path from root to " + str(path_node))

        # copy_path accepts ints or nodes, so no int-to-node conversion is necessary in the execute_path function
        path = self.copy_path(path_node)

        # Validate the returned path
        if not isinstance(path, node.InputNode):
            print("Error: Could not generate path")
            print("Path execution failed\n")
            return 0

        # This function takes a variable number of arguments, convert those to a list
        node_input = []
        for filename in args:
            node_input.append(filename)

        # Make sure there's at least one filename
        if not node_input:
            print("Error: No filename(s) given")
            print("Path execution failed\n")
            return 0

        traversal_node = path

        # Tree structure should guarantee that this loop terminates
        while True:

            # Execute the current node
            node_input = traversal_node.execute(node_input)

            # Check the output and stop if invalid
            if not node_input:
                print("Error: Path execution interrupted at " + str(traversal_node))
                break

            # After executing all the path nodes, break out
            if traversal_node.is_leaf():
                break
            else:
                # The path structure guarantees that each node has only one child
                traversal_node = traversal_node.children[0]

        print("Path execution finished\n")
        return node_input

    def execute(self, *args):
        """
        Input:
            An arbitrary number of filenames
        Expected behavior:
            Executes recursively from the input root down to each leaf node
        Should fail when:
            No filenames are specified
        Output:
            A list of the outputs of every executable path
        """

        print("Executing tree")

        # This function takes a variable number of arguments; convert those to a list
        node_input = []
        for filename in args:
            node_input.append(filename)

        # Make sure there's at least one filename
        if not node_input:
            print("Error: No filename(s) given")
            print("Tree execution failed\n")
            return 0

        # Storage for return values
        returns = []
        # A string to help visually represent the recursion
        recursion_str = "   "

        # Recursive function for traversing the tree down to the leaves and executing their paths
        def recursive_execute(current_node, current_input, depth_str):

            # Print the depth string and execute the current node
            print(depth_str, end="")
            exec_output = current_node.execute(current_input)

            # If the output is valid, feed it to each node child
            if exec_output:
                for child in current_node.children:
                    # Since the arguments are DFDs, deep copy them to avoid conflicts
                    recursive_execute(child, copy.deepcopy(exec_output), depth_str + recursion_str)
                # If a path ending is reached, indicate a successful execution and append the output
                if current_node.is_leaf():
                    print(depth_str + "Path execution complete at " + str(current_node))
                    returns.append(exec_output)
            # Indicate a failed execution and append its result to the list
            else:
                print(depth_str + "Error: Path execution interrupted at " + str(current_node))
                returns.append(exec_output)

        # Call the recursive function on the tree root with the original input and an empty string
        recursive_execute(self.root, node_input, "")

        # Return the list of results
        print("Tree execution finished\n")
        return returns

# ------------------------------------/ EXPORT METHODS /------------------------------------

    def to_pipeline(self, path_node: node.OperationNode or int):
        """
        Input:
            A node in the tree
        Expected behavior:
            Instantiates and returns a pipeline generated from the path defined by the given node
        Output:
            A Pipeline instance that may be null if the passed node is invalid
        """

        # The pipeline constructor uses the copy_path method, so it accept ints or nodes
        print("Creating pipeline of path from root to " + str(path_node) + "\n")

        return pipe.Pipeline(self, path_node)

    def save(self, filename):
        """
        Input:
            A filename for the destination file
        Expected behavior:
            Traverses the tree, encoding each node's list index, type, mode, parameters, and parent as a tuple on a line
            The encoding is as follows for the first line:
                (None, 0, input, mode, [params])
            The encoding is as follows for every other line:
                (parent index, index, type, mode, [params])
            This encoding is stored as a string and written to a file
            It's essentially a sequential instruction for building the tree from the root down
        Should fail when:
            The filename is invalid
            A file writing error occurs
        Output:
            The function returns 1 on success and 0 on failure.
        """

        print("Saving tree to " + str(filename))

        # Validate the filename
        if not isinstance(filename, str):
            print("Error: Filename must be a string\n")
            return 0

        # Recursive function for encoding the tree nodes
        def tree_encode(current_node, current_str):
            # Encode node members and their parent / self indices as tuples and write the tuples to the filetext
            for child in current_node.children:
                child_tuple = (self.nodes.index(current_node),
                               self.nodes.index(child),
                               child.type,
                               child.mode,
                               child.parameters)
                # Add the tuple line
                current_str = current_str + str(child_tuple) + "\n"
                # Recurse on each child, that is, encode each child's children
                current_str = tree_encode(child, current_str)

            return current_str

        # A starter tuple representing the root, and its string
        rt_tuple = (self.root.parent, self.nodes.index(self.root), self.root.type, self.root.mode, self.root.parameters)
        root_str = str(rt_tuple) + "\n"

        # Calling the recursive function with the root information
        file_text = tree_encode(self.root, root_str)

        # Open the file, write, and close
        o_file = open(filename, "w")
        o_file.write(file_text)
        o_file.close()

        print("Tree save succeeded\n")
        return 1

    @staticmethod
    def load(filename):
        """
        Input:
            A filename for the source file
        Expected behavior:
            Reads the file, parses the encoding (specified above), and builds the tree
            Building the tree serves as a form of validation, as other tree methods help enforce tree correctness
        Should fail when:
            The filename is invalid
            A file read error is encountered
            Syntax in the file is invalid
            The tree building procedure encounters an invalid state
        Output:
            Returns a tree object for success and 0 for failure
        """

        print("Loading TFormTree from " + str(filename))

        # Validate the filename
        if not isinstance(filename, str):
            print("Error: Filename must be a string")
            print("TFormTree load failed\n")
            return 0

        # Attempt to open the file
        try:
            i_file = open(filename, "r")
        except FileNotFoundError:
            print("Error: Failed to open file")
            print("TFormTree load failed\n")
            return 0

        # Get the contents as separate lines and close the file
        file_lines = i_file.read().splitlines()
        i_file.close()

        # Make sure the file was nonempty
        if not file_lines:
            print("Error: File is empty")
            print("TFormTree load failed\n")
            return 0

        # Get the number of lines read; which we expect is equal to the number of nodes in the tree
        num_nodes = len(file_lines)
        # print("Number of lines read: " + str(num_nodes))

        # A list of specified length with all members initialized to None
        # From: https://stackoverflow.com/questions/10712002/create-an-empty-list-in-python-with-certain-size
        # Serves as storage for tuples of valid nodes and their self indices
        # A node with self index n will be placed in index n
        tree_nodes = [None] * num_nodes

        # For each line in the file
        for line in file_lines:

            # A validity flag to only set true if all validations succeed
            valid = False
            # Storage for pre-validated tuples and nodes that also serve as flags
            temp_tuple = None
            temp_node = None

            # Attempt to convert the raw string to a tuple
            try:
                # Evaluate the file line string as a python expression, expecting a tuple
                temp_tuple = eval(line)

                # Validate tuple expression and tuple structure
                if isinstance(temp_tuple, tuple) and len(temp_tuple) == 5:

                    # Extract the tuple elements
                    prnt_i = temp_tuple[0]
                    self_i = temp_tuple[1]
                    n_type = temp_tuple[2]

                    # Validate tuple contents
                    # The parent index
                    if (isinstance(prnt_i, int) and 0 <= prnt_i < num_nodes - 1) or prnt_i is None:
                        # The node index
                        if isinstance(self_i, int) and 0 <= self_i < num_nodes:
                            # The node type
                            # If valid, instantiate the appropriate type
                            if n_type == node.input_type:
                                temp_node = node.InputNode()
                            elif n_type == node.prep_type:
                                temp_node = node.PrepNode()
                            elif n_type == node.mod_type:
                                temp_node = node.ModNode()
                            elif n_type == node.vis_type:
                                temp_node = node.VisNode()
                            else:
                                print("Error: Invalid node type: " + str(n_type))
                        else:
                            print("Error: Invalid node index: " + str(self_i))
                    else:
                        print("Error: Invalid parent index: " + str(prnt_i))
                else:
                    print("Error: Invalid tuple: " + line)

            # The file was improperly formatted somehow
            except SyntaxError:
                print("Error: File contains invalid Python syntax:")
                print(line)
            except TypeError:
                print("Error: File contains invalid Python type usage:")
                print(line)

            # If we were able to extract a valid node, it's time to validate the mode and parameters
            if temp_node is not None:

                # Retrieve these from the validated tuple
                n_mode = temp_tuple[3]
                params = temp_tuple[4]

                # The node class will validate the mode and parameters
                if temp_node.set_mode(n_mode):
                    if temp_node.set_params(params):
                        # This line-to-node conversion is valid if and only if every validation up to now succeeded
                        valid = True
                    else:
                        print("Error: Invalid parameters: " + str(params))
                else:
                    print("Error: Invalid mode: " + str(n_mode))

            # If we have a valid node
            if valid:
                # If the node list is unoccupied at the node's index
                if tree_nodes[temp_tuple[1]] is None:
                    # Save a tuple of the node and its parent index to the list
                    tree_nodes[temp_tuple[1]] = (temp_tuple[0], temp_node)
                # There's a conflict; two nodes shouldn't have the same index
                else:
                    print("Error: Two nodes correspond to same index: " + str(temp_tuple[1]))
                    print("TFormTree load failed\n")
                    return 0
            # One unsuccessful line validation is sufficient to fail the load process
            else:
                print("TFormTree load failed\n")
                return 0

        # All the nodes were valid and their self-indices didn't conflict

        # Make sure the first node obtained was the input root
        if not isinstance(tree_nodes[0][1], node.InputNode):
            print("Error: First file line is not an encoding of an InputNode: " + str(tree_nodes[0][1]))
            print("TFormTree load failed\n")
            return 0

        # This is the tree that will be returned if all goes well
        t_tree = TFormTree()

        # Overwrite the reference to the original tree root with the read input node
        t_tree.root = tree_nodes[0][1]
        t_tree.nodes[0] = t_tree.root

        # A proper file should guarantee that nodes will be added before nodes with indices that depend on them
        # Just in case, the tree.add function will fail if there is an unsatisfied dependency
        #       (as well as validating compatibility, etc)
        for item in tree_nodes[1:]:
            # Get the parent via the parent index
            mount = tree_nodes[item[0]][1]

            new_node = item[1]

            if not t_tree.add(mount, new_node):
                print("Error: Invalid node configuration encountered")
                print("TFormTree load failed\n")
                return 0

        # One last check - make sure every node read from the file ended up in the tree
        for item in tree_nodes:
            if item[1] not in t_tree.nodes:
                print("Error: Node not added to tree:" + str(item[1]))
                print("TFormTree load failed\n")
                return 0

        print("TFormTree load succeeded\n")
        return t_tree

# ------------------------------------/ HELPER METHODS /------------------------------------

    def at_index(self, index):
        """
        Input:
            A non-negative integer
        Expected behavior:
            Fetches a reference to the tree node with the corresponding integer index in the tree list, if it exists
        Should fail when:
            index is outside the list bounds
        Output:
            The node reference on success, 0 on failure
        """

        # Check if the input is a valid int and if that int falls in the valid positive or negative range
        if not isinstance(index, int) or index < 0 - len(self.nodes) or index >= len(self.nodes):
            print("Error: " + str(index) + " is an invalid index\n")
            return 0

        return self.nodes[index]

    def in_tree(self, node_subtree):
        """
        Input:
            A node subtree to check
        Expected behavior:
            Helper function to prevent cycles in the tree
            Recursively checks if the node or any of its children are in the tree list
        Should fail when:
            A node in the subtree is contained in the tree's list
            The passed item is not a node
        Output:
            1 on detection, 0 on none detected or error
        """

        # A recursive function to make sure no node in tested node's subtree is in the tree
        def recursive_in_tree(current_node):

            # Check if the current node or any of its children are already in the tree
            if current_node in self.nodes:
                print(str(current_node) + " found in this tree")
                return 1
            for child in current_node.children:
                if recursive_in_tree(child):
                    return 1

            return 0

        # This function serves a secondary role as a type validator
        if not isinstance(node_subtree, node.OperationNode):
            print("Error: Passed object is not an OperationNode")
            return 0

        # Run the recursive checker
        return recursive_in_tree(node_subtree)

    def copy_path(self, end_node: node.OperationNode or int):
        """
        Input:
            A node to serve as the end of the path
        Expected behavior:
            Traverses back up the tree to reconstruct the path from root downward
            Copies each node and adds it to the path
        Should fail when:
            End node isn't in the tree list
        Output:
            A path of nodes on success, 0 otherwise
        """

        print("Copying path from root to " + str(end_node))

        # Validate the copy point, if valid get the specified node
        if isinstance(end_node, int):
            end_node = self.at_index(end_node)
            if end_node == 0:
                print("Path copy failed\n")
                return 0
        elif end_node not in self.nodes:
            print("Error: " + str(end_node) + " is not in the tree")
            print("Path copy failed\n")
            return 0

        # Traverse up to the root using these node references
        path = end_node.copy()
        traversal_node = end_node.parent

        # Tree structure guarantees that this loop terminates
        while traversal_node is not None:

            # Add a shallow copy of the current node to the previous node
            self.__node_add(traversal_node.copy(), path)

            # Increment both nodes
            path = path.parent
            traversal_node = traversal_node.parent

        print("Path copy succeeded\n")
        return path

    def copy(self):
        """
        Expected behavior:
            Creates a semi-deep copy of the tree
        Output:
            A copy of the tree
        Restrictions:
            The indexing of nodes in the nodes list of the original tree is not replicated exactly in the copied tree
        """

        print("Copying tree")

        new_tree = TFormTree()

        # Replicate the mode and parameters
        new_tree.root.set_mode(self.root.mode)
        new_tree.root.set_params(self.root.parameters)

        # Use the node class copy method to copy the subtrees rooted at the root's children
        for subtree in self.root.children:
            new_tree.add(new_tree.root, subtree.copy("tree"))

        print("Tree copy succeeded\n")
        return new_tree

    def __str__(self):
        """
        Expected behavior:
            Recursively generates a string representation of the tree.
        """

        depth_str = "   "

        # Recursive function for indicating node depth with indentation
        def recursive_str(current_node, current_str, level=0):

            # Indent the string to the tree depth
            for x in range(0, level):
                current_str = current_str + depth_str

            # Append a one-line string representation of the node
            current_str = current_str + str(self.nodes.index(current_node)) + ": " +\
                str(current_node.type) + ", " + str(current_node.mode) + "\n"

            # Do the same for any children
            for child in current_node.children:
                current_str = recursive_str(child, current_str, level + 1)

            return current_str

        # Call the recursive function on the tree root with a blank starting string
        return recursive_str(self.root, "")

    # ------------------------------------/ NON-USER METHODS /------------------------------------

    @staticmethod
    def __recursive_list_remove(tree, current_node):
        """
        Input:
            A node
        Expected behavior:
            Recursively adds the node and its children to the tree's list
        Restrictions:
            Is only called when tree and current_node have been type-validated
            It is NOT intended to be called by users
        """

        # Recursively remove the node and its children
        tree.nodes.remove(current_node)

        for child in current_node.children:
            TFormTree.__recursive_list_remove(tree, child)

    @staticmethod
    def __recursive_list_add(tree, current_node):
        """
        Input:
            A node
        Expected behavior:
            Recursively adds the node and its children to the tree's list
        Restrictions:
            Is only called when tree and current_node have been type-validated
            Must only be called after __in_tree returns false with the respect to tree and current_node
            It is NOT intended to be called by users
        """

        tree.nodes.append(current_node)

        for child in current_node.children:
            TFormTree.__recursive_list_add(tree, child)

    @staticmethod
    def __node_add(parent, child):
        """
        Input:
            Two OperationNodes
        Expected behavior:
            Adds second node as a child of the first
        Restrictions:
            This method is only called with nodes that have been determined compatible by node.can_parent
            It is NOT intended to be called by users
        """

        child.parent = parent
        parent.children.append(child)

    @staticmethod
    def __node_remove(parent, child):
        """
        Input:
            Two OperationNodes
        Expected behavior:
            Removes second node as a child of the first
        Restrictions:
            This method is only called with confirmed node instances
            It is NOT intended to be called by users.
        """

        child.parent = None
        parent.children.remove(child)


# Heavy testing, but not rigorous unit testing
def main():

    # Setting up a tree
    test_tree = TFormTree()

    # Declaring all nodes
    prep1 = node.PrepNode("cubic root")
    prep2 = node.PrepNode("difference")
    prep3 = node.PrepNode("scaling")

    mod1 = node.ModNode()

    vis1 = node.VisNode("histogram")
    vis2 = node.VisNode("boxandwhiskers")

    # Making paths of the nodes
    test_tree.add(test_tree.root, prep1)
    test_tree.add(prep1, prep2)
    test_tree.add(prep2, prep3)
    test_tree.add(prep3, mod1)
    test_tree.add(mod1, vis1)
    test_tree.add(vis1, vis2)

    # Copying some subtrees in the tree
    alt_path1 = prep3.copy("tree")
    alt_path2 = prep2.copy("tree")

    # Adding all the paths to the tree
    test_tree.add(test_tree.root, alt_path1)
    test_tree.add(test_tree.root, alt_path2)
    print(test_tree)

    # Make a new path from copies of tree's nodes
    prep4 = test_tree.root.children[2].copy()
    alt_path3 = test_tree.at_index(14).copy("tree")

    alt_path3.set_mode("plot")
    alt_path3.children[0].set_mode("mse")
    prep4.set_mode("logarithm")

    # Mount it somewhere other than the root
    test_tree.add(prep3, prep4)
    test_tree.add(prep4, alt_path3)

    # New tree structure:
    print(test_tree)

    # Insert test

    prep5 = prep4.copy()
    prep5.set_mode("denoise")

    test_tree.insert(prep1, prep2, prep5)

    print(test_tree)

    # Removal / Replacement tests

    # Splice one of the high-level nodes out
    test_tree.remove(prep2)
    print(test_tree)

    # Remove an entire subtree
    test_tree.remove(prep4, "tree")
    print(test_tree)

    test_tree.replace(prep1, prep4)
    print(test_tree)

    # A sanity check for the qualities of the tree nodes
    for item in test_tree.nodes:
        print(item)

    # Tree copy test

    test_tree2 = test_tree.copy()

    test_tree2.save("test_files/tree.txt")

    print(test_tree2.copy_path(10))
    print(test_tree2.copy_path(-177))
    print(test_tree2.copy_path(10000))
    print(test_tree2.copy_path(test_tree2.at_index(10)))

    test_tree3 = TFormTree.load("test_files/tree.txt")
    test_tree4 = TFormTree.load("test_files/badtree1.txt")
    print("")
    test_tree5 = TFormTree.load("test_files/badtree2.txt")
    print("")
    test_tree6 = TFormTree.load("test_files/badtree3.txt")

    print(id(test_tree2))
    print(test_tree2)
    print("")
    print(id(test_tree3))
    print(test_tree3)

    # Validate the tree and turn it into a no-op tree
    for item in test_tree2.nodes:
        if test_tree.in_tree(item):
            print("Copy error detected: There is a reference to a node in the old tree in the new tree")
            return 0
        else:
            item.set_mode(None)
    print("\n")

    output1 = test_tree2.execute_path(test_tree2.at_index(12), "testdata1.csv")

    print(output1)

    output2 = test_tree2.execute("nonexistent args")

    for item in output2:
        print("DF with at " + str(id(item)))


if __name__ == "__main__":
    main()
