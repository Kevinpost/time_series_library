# Title:            preprocess.py
# Project:          Transformation Tree Library
# Description:      Implementations of the preprocess function and its associated functions.

# Team:             The Hackson Five

# Last Modified by: Kevin Post and Kuan-Yun Chao
# Date Modified:    08 Feb 2021

import pandas as pd
import math
import statistics  # unused except in currently commented out print statements inside standardize()

from itertools import groupby
from operator import itemgetter
import datetime
import numpy as np
from sklearn.neighbors import LocalOutlierFactor


# This is accessed in node.py to bind the user options to the different modes here
modes = ["scaling", "cubic_root", "logarithm", "denoise", "assign_time", "difference", "standardize",
         "clip", "impute_missing_data", "longest_continuous_run", "impute_outliers"]


def preprocess(pandas_data_frame_dict, process_function: str, a=None, b=None,
               c=None, d=None, e=None, f=None, g=None):
    """Accepts a pandas_data_frame_dict object, a process function (string)
        and optional arguments.
        Each data frame object MUST have a 'Magnitude' series.
        'TimeStamp' series is optional.
        Returns: pandas_data_frame_dict with each data frame processed
                OR
                the integer 0, if an error was encountered
    """
    # Make sure that the data frames dictionary is an element
    if 'data_frames_dict' not in pandas_data_frame_dict:
        print("'data_frames_dict' is required element of dictionary passed to preprocess()")
        return 0
    # make a reference to the data frames dict
    data_frames_dict = pandas_data_frame_dict['data_frames_dict']
    # make sure each data frame has the 'magnitude' series
    for element in data_frames_dict:
        if 'Magnitude' not in data_frames_dict[element]:
            print("'Magnitude' is a required element of DataFrame dictionaries for preprocess() function")
            return 0
        magnitude_list = list(data_frames_dict[element]['Magnitude'])
        for i in range(len(magnitude_list)):
            item = magnitude_list[i]
            if not (isinstance(item, int) or (isinstance(item, float)) or (item is None)):
                print("Every item in magnitude series must be int, float, None, or NaN.  Found <" + str(item) +
                      "> in DataFrame + <" + str(element) + ">\n")
                return 0
    # Call the appropriate function depending on what is called
    if process_function == 'scaling':
        pandas_data_frame_dict['min_max'] = dict()
        for element in data_frames_dict:
            # this one is different in that the preprocess function also
            # returns a tuple called min_max that is the min and max vals of the original magnitude series
            (df, min_max) = scaling(data_frames_dict[element])
            # update the dataframe with the modified series
            data_frames_dict[element] = df
            # add the min_max value for this dataframe
            pandas_data_frame_dict['min_max'][element] = min_max
        # make the return value the new modified dictionary
        return_value = pandas_data_frame_dict
    elif process_function == 'cubic_root':
        for element in data_frames_dict:
            data_frames_dict[element] = cubic_root(data_frames_dict[element])
        return_value = pandas_data_frame_dict
    elif process_function == 'logarithm':
        for element in data_frames_dict:
            data_frames_dict[element] = logarithm(data_frames_dict[element], a)
        return_value = pandas_data_frame_dict
    elif process_function == 'denoise':
        for element in data_frames_dict:
            data_frames_dict[element] = denoise(data_frames_dict[element], a)
        return_value = pandas_data_frame_dict
    elif process_function == 'assign_time':
        for element in data_frames_dict:
            data_frames_dict[element] = assign_time(data_frames_dict[element], a, b)
        return_value = pandas_data_frame_dict
    elif process_function == 'difference':
        for element in data_frames_dict:
            data_frames_dict[element] = difference(data_frames_dict[element])
        return_value = pandas_data_frame_dict
    elif process_function == 'standardize':
        for element in data_frames_dict:
            data_frames_dict[element] = standardize(data_frames_dict[element])
        return_value = pandas_data_frame_dict
    elif process_function == 'clip':
        for element in data_frames_dict:
            data_frames_dict[element] = clip(data_frames_dict[element], a, b)
        return_value = pandas_data_frame_dict
    elif process_function == 'impute_missing_data':
        for element in data_frames_dict:
            data_frames_dict[element] = impute_missing_data(data_frames_dict[element])
        # Impute missing data removes all nulls.  The data frames are by definition complete now
        pandas_data_frame_dict['complete_flag'] = True
        return_value = pandas_data_frame_dict
    elif process_function == 'longest_continuous_run':
        for element in data_frames_dict:
            data_frames_dict[element] = longest_continuous_run(data_frames_dict[element])
        return_value = pandas_data_frame_dict
    elif process_function == 'impute_outliers':
        for element in data_frames_dict:
            data_frames_dict[element] = impute_outliers(data_frames_dict[element], a)
        return_value = pandas_data_frame_dict
    else:
        # this shouldn't happen unless the module given an incorrect function name argument
        print("Invalid preprocess function name given to preprocess(): " + process_function)
        return 0
    for element in pandas_data_frame_dict['data_frames_dict']:
        # if any of the preprocess functions returned 0 there was an error
        if isinstance(pandas_data_frame_dict['data_frames_dict'][element], int):
            print(
                "Error while preprocessing data_frame '" + element + "' with preprocess function '"
                + process_function + "'")
            return_value = 0
    # return either 0 (error) or the modified universal object
    return return_value


def impute_outliers(data_frame, num_neighbors):
    """ Imputes outliers using a 1-dimensional analysis along the x-axis.
        Requires 'Magnitude' series.  TimeStamp series is optional.
         num_neighbors defaults to 8 or length of time series, whichever is lesser, if none provided.
         num_neighbors defaults to (length of series-1) if the provided number is larger than the length of the series
         Note:
             Removes rows with missing magnitude data.  (required for sklearn library)
     """
    # default for num neighbors is 8 values (number of adjacent values to look at to determine outlier status)
    num_neighbors = 8 if num_neighbors is None else num_neighbors
    # num  neighbors must be an integer
    if not isinstance(num_neighbors, int):
        print("Error in impute_outliers preprocess function: num_neighbors argument must be an integer")
        return 0
    # drop all rows with null values in the magnitude series
    data_frame.dropna(inplace=True, subset=['Magnitude'])
    # convert magnitude series to a list
    magnitude_list = list(data_frame['Magnitude'])
    # length of series must be at least 2 for this to function properly
    if len(magnitude_list) < 2:
        print("Impute_outliers function given series with less than 2 non-null values.  Returning empty series")
        # return an empty data frame if length is less than 2
        if 'TimeStamp' in data_frame:
            return pd.DataFrame({'TimeStamp': [], 'Magnitude': []})
        else:
            return pd.DataFrame({'Magnitude': []})
    # makes num_neighbors at most one less than the length of the series
    if num_neighbors > len(magnitude_list):
        num_neighbors = len(magnitude_list) - 1
    # get the length of the series
    len_mag_list = len(magnitude_list)
    # make an empty list
    mag_list_list = []
    for i in range(len_mag_list):
        # append each item in magnitude with an increasing index value
        element = [i, magnitude_list[i]]
        mag_list_list.append(element)
    # now that I have a 2D array I can use the sklearn library to calculate which values are outliers
    olf = list(LocalOutlierFactor(n_neighbors=num_neighbors, metric='l2').fit_predict(mag_list_list))
    # the output of this function is a series of '1' and '-1' where '-1' indicates outlier.
    # so then I iterate through the original series and replace outliers with None values
    for i in range(len_mag_list):
        if olf[i] == -1:
            magnitude_list[i] = None
    data_frame['Magnitude'] = magnitude_list
    # Impute missing data to fill in the Nones created by the above loop
    data_frame = impute_missing_data(data_frame)
    return pd.DataFrame(data_frame)


def longest_continuous_run(data_frame):
    """ Returns the longest consecutive run where no values in a row have missing data.
        If there are no rows without missing data, returns empty series
        Requires 'Magnitude' series.  TimeStamp series is optional
    """
    # create ann empty list
    null_bool_list = []
    # length of the series
    magnitude_list_len = len(data_frame['Magnitude'])
    # get reference to the magnitude series, as a list
    magnitude_series_list = list(data_frame['Magnitude'])
    if 'TimeStamp' in data_frame:
        # get reference to the timestamp series
        time_series_list = data_frame['TimeStamp']
    if 'TimeStamp' in data_frame:
        for i in range(magnitude_list_len):
            # if either value is None, NaT, or NaN, append True
            if (magnitude_series_list[i] is None or math.isnan(magnitude_series_list[i])) or \
                    (time_series_list[i] is None or pd.isnull(time_series_list[i])):
                null_bool_list.append(True)
            else:
                # else, append False
                null_bool_list.append(False)
    else:
        for i in range(magnitude_list_len):
            # if any of the values in the magnitude series are None or NaN, append True
            if magnitude_series_list[i] is None or math.isnan(magnitude_series_list[i]):
                null_bool_list.append(True)
            else:
                # else, append false
                null_bool_list.append(False)
    # Get the longest series of False values (not null or None)
    # Credit for below groups/keys logic to : https://stackoverflow.com/questions/58920673/
    # return-the-index-range-of-the-longest-run-of-a-boolean-list
    groups = [[i for i, _ in group] for key, group in groupby(enumerate(null_bool_list), key=itemgetter(1)) if not key]
    group = max(groups, key=len, default=[-1, -1])
    start, end = group[0], group[-1]
    #  Credit ends

    if start == -1 and end == -1:
        # this means that the length of the longest series is 0.
        # Return an empty data frame
        if 'TimeStamp' in data_frame:
            return pd.DataFrame({'TimeStamp': [], 'Magnitude': []})
        else:
            return pd.DataFrame({'Magnitude': []})
    else:
        # add 1 to end because of the way that python slicing works
        end += 1
        # Return the data frame sliced appropriately based on the start & end of the longest continuous run
        if 'TimeStamp' in data_frame:
            return pd.DataFrame(
                {'TimeStamp': time_series_list[start:end], 'Magnitude': magnitude_series_list[start:end]})
        else:
            return pd.DataFrame({'Magnitude': magnitude_series_list[start:end]})


def impute_missing_data(data_frame):
    """ Takes a single pandas data frame, with or without a time series.
        Removes null values from the beginning and end, and imputes
        missing data using a 'linear' algorithm with the ____ASSUMPTION____
        that the missing magnitude values and the missing time stamps are
        equally distant from each other.
        For example:
        (magnitude)
        IN:  [NaN, 1, NaN, NaN, 4, NaN]
        OUT: [1, 2, 3, 4]
        (Time stamp)
        IN: [NaT, "2021-01-01 00:10:00", NaT, NaT, 2021-01-01 00:40:00, NaT]
        OUT: ["2021-01-01 00:10:00", "2021-01-01 00:20:00", "2021-01-01 00:30:00", "2021-01-01 00:40:00"]
    """
    # create reference to the magnitude series as a list
    magnitude_series_list = list(data_frame['Magnitude'])
    if 'TimeStamp' in data_frame:
        # create reference to the TimeStamp series as a list
        time_series = list(data_frame['TimeStamp'])
    # get the length of the series so it can be iterated through
    len_magnitude_series = len(magnitude_series_list)

    # trim beginning based on magnitude series
    for i in range(len_magnitude_series):
        # make sure the list is greater than 0 size
        if len(magnitude_series_list) > 0:
            # if the first value is None or NaN
            if magnitude_series_list[0] is None or math.isnan(magnitude_series_list[0]):
                # then delete the first value in each series
                if 'TimeStamp' in data_frame:
                    del time_series[0]
                del magnitude_series_list[0]
            else:
                # break when the first non-null value is encountered
                break
    # trim end based on magnitude series
    len_magnitude_series = len(magnitude_series_list)
    for i in range(len_magnitude_series):
        # make sure the list is still greater than zero length
        if len(magnitude_series_list) > 0:
            # if the last value is None or Nan
            if magnitude_series_list[-1] is None or math.isnan(magnitude_series_list[-1]):
                # then delete the last value in each series
                if 'TimeStamp' in data_frame:
                    del time_series[-1]
                del magnitude_series_list[-1]
            else:
                # break when the first non-null value is encountered
                break

    # re-create the data frame using the new trimmed magnitude and time stamp series
    if 'TimeStamp' in data_frame:
        data = {'TimeStamp': time_series, 'Magnitude': magnitude_series_list}
        data_frame = pd.DataFrame(data)
    else:
        data = {'Magnitude': magnitude_series_list}
        data_frame = pd.DataFrame(data)

    # Impute missing magnitude data using the pandas interpolate function with the 'linear' algorithm
    data_frame['Magnitude'].interpolate(method='linear', inplace=True)

    # Create reference to the magnitude series
    magnitude_series_list = data_frame['Magnitude']

    # trim the beginning based on time series
    if 'TimeStamp' in data_frame:
        # create reference to the time stamps series as a list
        time_series = list(data_frame['TimeStamp'])
        # get the length of the time series so i can iterate through it
        len_time_series = len(time_series)
        for i in range(len_time_series):
            # make sure the length of the series is still more than 0
            if len(time_series) > 0:
                # if the first value in the time series is None or Nan
                if time_series[0] is None or pd.isnull(time_series[0]):
                    # delete the first value in both series
                    del time_series[0]
                    del magnitude_series_list[0]
                else:
                    # break when the first non-null value is encountered
                    break
    # trim the end based on time series
    if 'TimeStamp' in data_frame:
        # get the length of the time series
        len_time_series = len(time_series)
        for i in range(len_time_series):
            # make sure the length of the series is still more than 0
            if len(time_series) > 0:
                # If the last value is None or NaT
                if time_series[-1] is None or pd.isnull(time_series[-1]):
                    # delete the last value from both series
                    del time_series[-1]
                    del magnitude_series_list[-1]
                else:
                    # break when the first non-null value is encountered
                    break

    # re-create the data frame from the new data
    if 'TimeStamp' in data_frame:
        data = {'TimeStamp': time_series, 'Magnitude': magnitude_series_list}
        data_frame = pd.DataFrame(data)
    else:
        data = {'Magnitude': magnitude_series_list}
        data_frame = pd.DataFrame(data)

    # impute missing TimeStamp data
    if 'TimeStamp' in data_frame:
        # create a reference to the time stamp series as a list
        time_series_list = list(data_frame['TimeStamp'])
        # iterate through the whole series
        for i in range(len(time_series_list)):
            # if any value is NaT
            if pd.isnull(time_series_list[i]):
                # replace it with some random and VERY precise value
                # this is a known (very slight) weakness.  if any time series contains a time stamp at
                # EXACTLY 123456789.123456789 seconds after the epoch, it will erroneously treat it as missing data
                time_series_list[i] = pd.to_datetime(123456789.123456789, unit='s')
        # replace the TimeStamp series with the time series list.
        data_frame['TimeStamp'] = time_series_list
        # Convert each time stamp value to seconds since the epoch, as a list
        time_series_in_seconds = list(data_frame['TimeStamp'].astype('int64') / 1e9)
        # Create a pandas time series object using that list
        pandas_time_series = pd.Series(time_series_in_seconds)
        # replace all of the values with my arbitrarily chosen but VERY SPECIFIC time stamp with None
        for i in range(len(pandas_time_series)):
            if pandas_time_series[i] == 123456789.123456789:
                pandas_time_series[i] = None
        # Use the built-in interpolate function with the 'linear' argument to impute the Missing data
        pandas_time_series.interpolate(method='linear', inplace=True)
        # create an empty list
        time_series = []
        for element in pandas_time_series:
            # append each object, converted from seconds since the epoch to a conventional date/time stamp
            time_series.append(pd.Timestamp(element, unit='s'))
        # replace the TimeStamp series in the dataframe with the new, imputed series
        data_frame['TimeStamp'] = time_series
    # return the data frame, now absent any null values
    return pd.DataFrame(data_frame)


def clip(data_frame, starting_date, ending_date):
    """ Required Arguments:
            A single Pandas DataFrame with a 'TimeStamp' series
            Starting Date 'YYYY-MM-DD HH:MM:SS' or 'YYYY-MM-DD'
            Ending Date 'YYYY-MM-DD HH:MM:SS' or 'YYYY-MM-DD'
        Returns a data frame whose Time Stamp values are between the starting and end date
        Notes:
            Removes values where TimeStamp is missing.  (requirement for pandas library)
    """
    # these are the two formats my function accepts for date and time
    format1 = "%Y-%m-%d"
    format2 = "%Y-%m-%d %H:%M:%S"
    # starting date and ending date must be strings
    if not (isinstance(starting_date, str) and isinstance(ending_date, str)):
        print("Error in clip() function: arguments 'starting date' and 'ending date' must be strings")
        return 0
    # create a reference to the magnitude series as a list
    magnitude_list = list(data_frame['Magnitude'])
    # make sure the data frame contains a timestamp series
    if 'TimeStamp' not in data_frame:
        print("Time Series is a required element for 'clip' preprocess function")
        return 0
    # Make sure that a starting date and ending date were provided.
    if (starting_date is None) or (ending_date is None):
        print("clip function requires both a starting and end date: 'YYYY-MM-DD HH:MM:SS' or 'YYYY-MM-DD")
        return 0
    # drop all rows without a time stamp value
    data_frame.dropna(inplace=True, subset=['TimeStamp'])
    # reference to the time series as a list of seconds since the epoch
    time_series_in_seconds = list(data_frame['TimeStamp'].astype('int64') / 1e9)
    # Test whether the start date is formatted using format1
    try:
        datetime.datetime.strptime(starting_date, format1)
    except ValueError:
        try:
            # if it didn't work, try it using format2
            datetime.datetime.strptime(starting_date, format2)
        except ValueError:
            # If this is reached, then the starting date was not properly formatted
            print("Starting Date <'" + str(starting_date) + "'> provided to Clip function is improperly formatted. "
                  + "Must be a string of the form 'YYYY-MM-DD HH:MM:SS' or 'YYYY-MM-DD")
            return 0
    # repeat the above, but with the ending date
    try:
        datetime.datetime.strptime(ending_date, format1)
    except ValueError:
        try:
            datetime.datetime.strptime(ending_date, format2)
        except ValueError:
            print("Ending Date <'" + str(ending_date) + "'> provided to Clip function is improperly formatted. "
                  + "Must be a string of the form 'YYYY-MM-DD HH:MM:SS' or 'YYYY-MM-DD")
            return 0
    # convert the start and end date to seconds-since-the-epoch
    start_time_in_seconds = pd.to_datetime(starting_date).timestamp()
    end_time_in_seconds = pd.to_datetime(ending_date).timestamp()
    # make sure that the end value is after or the same as the start value
    if end_time_in_seconds < start_time_in_seconds:
        print("Error in clip preprocess function: User-supplied start date <'" +
              starting_date + "'> is AFTER end date <'" + ending_date + "'>")
        return 0
    # get the length of the time series so it can be iterated through
    len_time_series = len(time_series_in_seconds)
    for i in range(len_time_series):
        # if the value at the beginning of the list is before the start time
        if time_series_in_seconds[0] < start_time_in_seconds:
            # delete that item from both series
            del time_series_in_seconds[0]
            del magnitude_list[0]
    # get the length of the time series again so it can be iterated through
    len_time_series = len(time_series_in_seconds)
    for i in range(len_time_series):
        # if the value at the end of the list is after end time
        if time_series_in_seconds[-1] > end_time_in_seconds:
            # delete that item from both series
            del time_series_in_seconds[-1]
            del magnitude_list[-1]
    # create an empty list
    time_series = []
    for element in time_series_in_seconds:
        # create a list of time stamp objects from the modified list
        time_series.append(pd.Timestamp(element, unit='s'))

    data = {'TimeStamp': time_series, 'Magnitude': magnitude_list}
    # create a data frame using the modified series
    data_frame = pd.DataFrame(data)
    # and return it
    return data_frame


def standardize(data_frame):
    """ Argument: A single Pandas DataFrame with or without TimeStamp series, but requires Magnitude series.
        Produces a time series whose mean is 0 and variance is 1.
        Notes:
            Removes values where Magnitude is missing.  (requirement for numpy library)
    """
    # first, delete rows with missing magnitude values
    data_frame.dropna(inplace=True, subset=['Magnitude'])
    # create a reference to the magnitude series as a list
    magnitude_list = list(data_frame['Magnitude'])
    """ Alternative method commented out below.  (Same end result though)
    """
    # X_train = np.array([magnitude_list])
    # X_train = np.transpose(X_train)
    # scaler = preprocessing.StandardScaler().fit(X_train)
    # X_scaled = scaler.transform(X_train)
    # X_scaled = np.transpose(X_scaled)
    # x = X_scaled[0]
    # x = list(x)
    # length must be greater than 2
    if len(magnitude_list) < 2:
        # if it isn't return an empty data frame with the appropriate elements
        print("Standardize function given series with less than 2 non-null values.  Returning empty series")
        if 'TimeStamp' in data_frame:
            return pd.DataFrame({'TimeStamp': [], 'Magnitude': []})
        else:
            return pd.DataFrame({'Magnitude': []})
    # get the average of the magnitude list values
    avg = sum(magnitude_list) / len(magnitude_list)
    # then iterate through the list
    for i in range(len(magnitude_list)):
        # subtract the average from each value to make the average zero
        magnitude_list[i] = magnitude_list[i] - avg
    # create a numpy array from the list
    numpy_list = np.asarray(magnitude_list, dtype=np.float64)
    # divide each value by the standard deviation, to give it stdev ~1
    numpy_list /= np.std(magnitude_list)
    # convert it to a list
    standardized_magnitude = list(numpy_list)
    # replace the magnitude series with the modified series
    data_frame['Magnitude'] = standardized_magnitude
    # print(statistics.variance(standardized_magnitude))
    # print(statistics.mean(standardized_magnitude))
    # return it
    return pd.DataFrame(data_frame)


def difference(data_frame):
    """ Argument: A single Pandas DataFrame with or without TimeStamp series, but requires Magnitude series.
        Computes each point as the delta between each point and the next.
        Notes:
            May introduce new NaN values around existing NaN values
            Resulting dataframe will be 1 element smaller than the input data frame
    """
    # create a reference to the magnitude series as a list
    magnitude_list = list(data_frame['Magnitude'])
    # make sure the magnitude list has at least length 2
    if len(magnitude_list) < 2:
        # if it doesn't, then return an empty data frame with the right contents.
        print("Difference function given series with less than 2 values.  Returning empty series")
        if 'TimeStamp' in data_frame:
            return pd.DataFrame({'TimeStamp': [], 'Magnitude': []})
        else:
            return pd.DataFrame({'Magnitude': []})
    # create an empty list
    magnitude_differences = []
    # get the length of the magnitude series so it can be iterated through.
    len_magnitude_list = len(magnitude_list)
    for i in range(len_magnitude_list - 1):
        # each value is now equal to the difference from one step to the next
        magnitude_differences.append(magnitude_list[i + 1] - magnitude_list[i])
    if 'TimeStamp' in data_frame:
        # if the time stamp element is in the dataframe, slice off the last element
        timestamp_list = list(data_frame['TimeStamp'])
        timestamp_list_trimmed = timestamp_list[:-1]
        # create a data frame
        data_frame_trimmed = pd.DataFrame({'TimeStamp': timestamp_list_trimmed, 'Magnitude': magnitude_differences})
    else:
        # else, create a data frame
        data_frame['Magnitude'] = magnitude_differences
        data_frame_trimmed = data_frame
    # return the data frame
    return pd.DataFrame(data_frame_trimmed)


def assign_time(data_frame, start, increment):
    """ Argument: A single Pandas DataFrame with or without TimeStamp series, but requires Magnitude series.
        Computes a time series from starting point, separated by 'increment' distance
        If there is already a time series, it replaces it with a new one
        Optional argument 'start' : Defaults to '2021-01-01' if none provided
        Optional argument 'increment' : Defaults to '1MIN' if none provided
    """
    # start and increment must be strings
    if not (isinstance(start, str) and isinstance(increment, str)):
        print("Error in assign_time() preprocess function: argument 'starting date' and 'increment' must be a strings")
        return 0
    # these are the two formats accepted for date and time
    format1 = "%Y-%m-%d"
    format2 = "%Y-%m-%d %H:%M:%S"
    try:
        # check whether the start time is formatted correctly with format1
        datetime.datetime.strptime(start, format1)
    except ValueError:
        try:
            # if that didn't work, then check whether it is formatted correctly with format2
            datetime.datetime.strptime(start, format2)
        except ValueError:
            # this means that the start date was formatted incorrectly
            print("Starting Date <'" + str(start) + "'> provided to assign_time preprocess" +
                  " function is improperly formatted. " +
                  "Must be a string of the form 'YYYY-MM-DD HH:MM:SS' or 'YYYY-MM-DD")
            return 0
    # default values for start date and increment
    start = '2021-01-01' if start is None else start
    increment = '1MIN' if increment is None else increment
    # get the list of the magnitude list
    num_periods = len(list(data_frame['Magnitude']))
    try:
        # attempt to create a time series using the user data
        time_series = pd.date_range(start, periods=num_periods, freq=increment)
        # make a data frame using the newly created time series
        data_frame = pd.DataFrame({'TimeStamp': time_series, 'Magnitude': data_frame['Magnitude']})
    except ValueError:
        # this means that the supplied frequency is not formatted correctly
        print("Error in assign_time(): Supplied frequency is invalid. Good examples: \n" +
              "5D = 5 days, 5MIN = 5 mins, 5S = 5 secs \n" +
              "VISIT FOR MORE INFORMATION: https://pandas.pydata.org/pandas-docs/stable/" +
              "user_guide/timeseries.html#offset-aliases")
        return 0
    # return the data frame
    return pd.DataFrame(data_frame)


def scaling(data_frame):
    """ Argument: A single Pandas DataFrame with or without TimeStamp series, but requires Magnitude series.
        Returns a series whose magnitudes are scaled between [0,1]
        Notes:
            Removes entries with missing Magnitude ( Required per project description.  NaN is not between 0 and 1)
    """
    # drop all rows with null values
    data_frame.dropna(inplace=True, subset=['Magnitude'])
    # create reference to the magnitude series
    magnitude_list = list(data_frame['Magnitude'])
    # Get the list of the magnitude series
    length_magnitude_list = len(magnitude_list)
    if length_magnitude_list == 0:
        # if the length is zero, then return the empty data frame and None for the min_max
        return pd.DataFrame(data_frame), None
    # get the max and min
    max_val = max(magnitude_list)
    min_val = min(magnitude_list)
    # create a tuple of the min and max (preserved for return value)
    min_max = (min_val, max_val)

    if min_val < 0:
        # if the minimum value is negative
        # then shift every value in the positive direction to make the lowest value = 0
        zero_diff = 0 - min_val  # difference between the min value and zero
        for i in range(length_magnitude_list):
            magnitude_list[i] = magnitude_list[i] + zero_diff
    else:
        # otherwise, shift every value down until the lowest value = 0
        for i in range(length_magnitude_list):
            magnitude_list[i] = magnitude_list[i] - min_val
    # get the new max of the series
    new_max = max(magnitude_list)
    if new_max == 0:
        # this means every value is zero. since the min and max are both zero
        for i in range(length_magnitude_list):
            magnitude_list[i] = 0
    else:
        # else, divide each value by the max to get a series scaled between 0 and 1
        for i in range(length_magnitude_list):
            magnitude_list[i] = magnitude_list[i] / new_max
    # make the magnitude series equal to the scaled list
    data_frame['Magnitude'] = magnitude_list
    # return the dataframe as well as the min_max tuple
    return pd.DataFrame(data_frame), min_max


def cubic_root(data_frame):
    """ Argument: A single Pandas DataFrame with or without TimeStamp series, but requires Magnitude series.
        Computes the cubic root of each value, in place.
    """
    # create reference to the magnitude series as a list
    magnitude_list = list(data_frame['Magnitude'])
    # get the length of the list so it can be iterated through
    len_magnitude_list = len(magnitude_list)
    for i in range(len_magnitude_list):
        # take the cubic root of each value and replace it in-place
        if magnitude_list[i] is None:
            continue
        if magnitude_list[i] < 0:
            print("Error in cubic_root() : values in Magnitude series must be >= 0.  Your series contained <"
                  + str(magnitude_list[i]) + ">\n")
            return 0
        magnitude_list[i] = math.pow(magnitude_list[i], 1 / 3.)
    # replace the magnitude series with the new modified list
    data_frame['Magnitude'] = magnitude_list
    return pd.DataFrame(data_frame)


def logarithm(data_frame, base):
    """ Argument: A single Pandas DataFrame with or without TimeStamp series, but requires Magnitude series.
        Computes the log of each value, in place.
        Optional argument 'base' : Defaults to natural log if none provided
    """
    # make sure the base is an int, a float, or None
    if not (isinstance(base, int) or isinstance(base, float) or (base is None)):
        print("Invalid 'base' argument: <'" + str(base) + "'> type: <'" + str(type(base)) +
              "'> to logarithm() preprocess function. Must be an int or a float.")
        return 0
    # make sure the base is greater than zero
    if (isinstance(base, int) or isinstance(base, float)) and base <= 0:
        print("Invalid 'base' argument: <" + str(base) + "> to logarithm() preprocess function. "
                                                         "Must be greater than zero.")
        return 0
    # create reference to magnitude series as a list
    magnitude_list = list(data_frame['Magnitude'])
    # get the length of the magnitude series so it can be iterated through
    length_magnitude_list = len(magnitude_list)
    for i in range(length_magnitude_list):
        # Cannot take log of num less than zero. Error
        if magnitude_list[i] is None:
            continue
        if magnitude_list[i] <= 0:
            print("Error in logarithm() preprocess function.  Values in dataframe given to logarithm function must be"
                  + " greater than zero.  Value encountered : <" + str(magnitude_list[i]) + "> at index " + str(i))
            return 0
        else:
            # else, take the log of the number using the supplied base.  If no base is provided then standard log.
            magnitude_list[i] = math.log(magnitude_list[i]) if (base is None) else math.log(magnitude_list[i], base)
    # replace the magnitude series with the new modified list of values
    data_frame['Magnitude'] = magnitude_list
    # return the modified data frame
    return pd.DataFrame(data_frame)


def denoise(data_frame, window_size):
    """ Argument: A single Pandas DataFrame with or without TimeStamp series, but requires Magnitude series.
        Computes each value using the average of surrounding (window_size) values
        Optional Argument window_size (Integer) defaults to 5
        Notes:
            Trims values from the start of the series equal to 1/2 the window size
            Introduces additional NaN values around existing NaN values
                (as a result of taking the average of NaN values)
    """
    # make sure that window size is an integer or none
    if not (isinstance(window_size, int) or (window_size is None)):
        print("Invalid 'window_size' argument: <'" + str(window_size) +
              "'> type: <'" + str(type(window_size)) + "'> to denoise() preprocess function. Must be an int.")
        return 0
    # if window size is none then default to 5
    window_size = 5 if window_size is None else window_size
    # if the supplied window size is not a positive integer, error
    if window_size <= 0:
        print("Invalid 'window_size' argument: <" + str(window_size) +
              "> to denoise() preprocess function. Must be greater than zero")
        return 0

    # credit for below to https://www.kite.com/python/answers/how-to-find-the-moving-average-of-a-list-in-python
    # create a rolling window
    windows = data_frame['Magnitude'].rolling(window_size)
    # get the moving averages as a series
    moving_averages = windows.mean()
    # get the moving averages as a list
    moving_averages_list = moving_averages.tolist()
    # _____credit ends_____
    # trim off the first few values, which are now NaN
    without_nans = moving_averages_list[window_size - 1:]
    if 'TimeStamp' in data_frame:
        # if timestamp in the series, convert to a list, minus the trimmed beginning
        timestamp_list_without_nans = list(data_frame['TimeStamp'])[window_size - 1:]
        # convert it to a pandas date time series
        time_series_no_nans = pd.to_datetime(timestamp_list_without_nans)
        # create a pandas dataframe with the denoised values
        data = {'TimeStamp': time_series_no_nans, 'Magnitude': without_nans}
        pandas_data_frame = pd.DataFrame(data)
    else:
        # modify the magnitude series in the data frame
        data = {'Magnitude': without_nans}
        # now it includes the denoised values
        pandas_data_frame = pd.DataFrame(data)
    # return the denoised data frame
    return pandas_data_frame


def main():
    """Creates two time series with time stamps and magnitudes.
    this will not run on execution of the program, it is only for testing the preprocess module."""

    import test_function
    default_magnitude = [-110, 150]
    number_of_datapoints = 100
    control_list = [True, True, True, True]
    outliers = [10, [-130, 170]]  # [number of outliers, magnitude of outliers from low to high]
    nones = 10  # number of nones for the none list
    flats = [10, -2]  # [length of a flat sequence, magnitude of the flat sequence]

    temp_list = test_function.generate_random_time_series(default_magnitude, number_of_datapoints,
                                                          control_list, outliers, nones, flats)
    magnitude_series = pd.Series(temp_list)  # create series of magnitude values from list

    temp_list_2 = []
    for item in temp_list:
        if item is None:
            temp_list_2.append(None)
        else:
            temp_list_2.append(item + 1)
    magnitude_series_2 = pd.Series(temp_list_2)
    time_series = pd.date_range("2021-01-01 00:00:00", periods=len(temp_list), freq="10MIN")
    data = {'TimeStamp': time_series, 'Magnitude': magnitude_series}  # dictionary of series
    pandas_data_frame = pd.DataFrame(data)  # Create DataFrame object
    time_series_2 = pd.date_range("2021-03-13 00:00:00.000", periods=len(temp_list), freq="25MIN")
    data_2 = {'TimeStamp': time_series_2, 'Magnitude': magnitude_series_2}
    pandas_data_frame_2 = pd.DataFrame(data_2)
    pandas_data_frame_dict = \
        {'data_frames_dict': {'0': pandas_data_frame, '1': pandas_data_frame_2}, 'complete_flag': False}

    x = preprocess(pandas_data_frame_dict, "impute_outliers")
    # t1 = pandas_data_frame_dict['data_frames_dict']
    # t2 = t1['Magnitude'].to_list()
    y = pandas_data_frame['Magnitude'].to_list()

    print(x)
    print(temp_list)
    print(y)

    return None


if __name__ == "__main__":
    main()
